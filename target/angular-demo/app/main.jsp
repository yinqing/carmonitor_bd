<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.alibaba.fastjson.JSON" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	Object sess = request.getSession().getAttribute("loginUser");
	String loginSession = JSON.toJSONString(sess);
%>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-control" content="no-cache">
	<meta http-equiv="Cache" content="no-cache">
	<link rel="shortcut icon" href="/app/img/logo/logo.ico" />
	<link rel="stylesheet" type="text/css" href="/app/lib/grid/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="/app/lib/grid/bootstrap-table.min.css"/>
	<link rel="stylesheet" type="text/css" href="/app/lib/font/css/font-awesome.css" />
	<link rel="stylesheet" type="text/css" href="/app/lib/layui/css/layui.css"/>
	<link rel="stylesheet" type="text/css" href="/app/lib/grid/jquery.contextMenu.css"/>
	<link rel="stylesheet" href="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.css" />
	<link rel="stylesheet" type="text/css" href="/app/lib/css/animate.min.css"/>
	<link rel="stylesheet" type="text/css" href="/app/font/iconfont.css"/>
	<link rel="stylesheet" href="/app/css/main.css">
	<title>变电站电力作业安全智能预警管控系统</title>
</head>
<body>
<div id="app">
	<!--顶部导航栏-->
	<div class="nav">
		<div class="main-logo">
			<%--<span>电力作业安全管控系统</span>--%>
		</div>
		<div class="nav-items">
			<div class="nav-top">
				<span class="welcom-info"></span>
				<!--分割线-->
				<span class="line-separator"></span>
				<span class="update-pwd">修改密码</span>
				<!--分割线-->
				<span class="line-separator"></span>
				<span class="login-out">退出</span>
			</div>
			<div class="nav-item">
				<ul class="home-nav">
					<li class="active" data-index="0">实时监控</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="home-body">
		<div id="moveBarBox" class="home-menu-outer">
			<div class="show-arrow"></div>
			<!--左侧菜单栏-->
			<div class="home-menu-grid home-location">
				<div class="location-params">
					<span class="location-item location-tip location-checked" data-index="1">实时位置</span>
					<span class="location-item history-tip" data-index="2">历史查询</span>
					<!--<span class="location-item tower-tip" data-index="3">塔杆信息</span>-->
				</div>
				<!--实时位置查询 start -->
				<div class="location-box">
					<div class="search-location">
						<p class="main-self-item">
							<label class="page-label">员工姓名：</label>
							<input type="text" class="main-self-input curr-search-name page-input" name="name"  value="" placeholder="请输入姓名"/>
						</p>
						<p class="main-self-item">
							<label class="page-label">手机号码：</label>
							<input type="text" class="main-self-input curr-search-mobile page-input" name="number" value="" placeholder="请输入手机号码"/>
						</p>
						<p class="main-self-item">
							<label class="page-label">变电站名称：</label>
							<input type="text" class="main-self-input curr-search-power page-input" name="number" value="" placeholder="请输入变电站名称"/>
						</p>
						<p class="main-self-item">
							<label class="page-label">作业名称：</label>
							<select name="routeId" class="curr-location-route home-select-cls page-input">
								<option value="">请选择作业</option>
							</select>
						</p>
						<p class="main-btn-item">
							<button class="main-self-btn location-search">查询</button>
							<button class="main-self-btn location-reset">重置</button>
						</p>
					</div>
					<table id="main-location-table" class="table table-striped main-self-table" border="0" cellspacing="" cellpadding="0">
					</table>
				</div>
				<!--实时位置查询  end-->
				<!--历史查询 start -->
				<div class="history-box">
					<div class="history-line">
						<p class="main-self-item">
							<label class="his-label">员工姓名：</label>
							<input type="text" class="main-self-input page-input" name="name"value="" placeholder="请输入姓名"/>
						</p>
						<p class="main-self-item">
							<label class="his-label" >手机号码：</label>
							<input type="text" class="main-self-input page-input" name="number" value="" placeholder="请输入手机号码"/>
						</p>
						<p class="main-self-item">
							<label class="his-label">作业名称：</label>
							<input type="taskName" class="main-self-input page-input" name="name"value="" placeholder="请输入作业名称"/>
						</p>
						<p class="main-self-item">
							<label class="his-label">日期：</label>
							<input type="text" class="main-self-input page-input" id="his-date" name="name"value="" placeholder="请选择日期"/>
						</p>
						<p class="main-self-item">
							<label class="his-label">变电站名称：</label>
							<select name="hisRouteId" class="his-location-route home-select-cls page-input">
								<option value="">请选择变电站</option>
							</select>
						</p>
						<p class="main-btn-item">
							<button class="main-self-btn history-search">查询</button>
							<button class="main-self-btn history-reset">重置</button>
						</p>
					</div>
					<table id="main-history-table" class="table table-striped main-self-table" border="0" cellspacing="" cellpadding="0">
					</table>
				</div>
				<!--历史查询  end-->
				<!--塔杆信息 start -->
				<div class="tower-box">
					<div class="tower-info">
						<p class="main-self-item">
							<label >塔杆编号：</label>
							<input type="text" class="main-self-input" name="name" value="" placeholder="请输入塔杆编号"/>
						</p>
						<p class="main-self-item">
							<label >责任人：</label>
							<input type="text" class="main-self-input" name="number"  value="" placeholder="请输入责任人"/>
						</p>
						<p class="main-btn-item">
							<button class="main-self-btn tower-search">查询</button>
							<button class="main-self-btn tower-reset">重置</button>
						</p>
					</div>
					<table id="main-tower-table" class="table table-striped main-self-table" border="0" cellspacing="" cellpadding="0">
					</table>
				</div>
				<!--塔杆信息  end-->
			</div>
			<div class="home-menu home-other">
				<ul class="home-menu-item"></ul>
			</div>
		</div>
		<div id="moveBox"></div>
		<!--正文列表-->
		<div class="home-content">
			<div class="show-bian-dian">
				<span class="main-map-back">返回</span>
				<div class="person-photo"></div>
			</div>
			<div class="home-map">
				<div id="allmap" class="home-location"></div>
				<div class="map-tips">
					<p class="map-tips-title">
						<span class="map-tips-info">系统警报信息提醒</span>
						<span><input type="checkbox" class="open-warning"/>开启报警声音</span>
						<span class="fast-handle">一键处理</span>
						<span class="export-warn">导出Excel</span>
						<span class="close-map-tips" title="收起警示框">X</span>
					</p>
					<table class="table warnint-table">
					</table>
				</div>
				<div class="show-waring-icon" title="显示报警信息"></div>
			</div>
			<audio controls="controls" preload id="warn-voice" hidden>
				<source src="/app/voice/song.mp3" type="audio/mp3" />
			</audio>
			<div class="home-border">
				<div class="show-grid home-other"></div>
			</div>
		</div>
	</div>
	<div class="home-bottom">
		<span>copyright@2017 北京彦拓科技有限公司，保留所有权利。技术支持：TEL:010-51645202</span>
	</div>
</div>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<!--TABLE LIB end-->
<script>
	LOGININFO = JSON.parse('<%=loginSession%>');
	L_RNAME = LOGININFO.realName;
	L_NAME = LOGININFO.userName;
	L_TYPE = LOGININFO.userType;
	L_ID = LOGININFO.userId;
	$(".welcom-info").text(L_RNAME?"欢迎您："+L_RNAME:"欢迎您："+L_NAME);

	var bodyWidth = document.documentElement.clientWidth,
			bodyHeight = document.documentElement.clientHeight;
	$(".home-menu").height(bodyHeight-104);
	$(".home-menu-grid").height(bodyHeight-104);
	$(".home-content").height(bodyHeight-104);
	$(".home-content").css("max-height","bodyHeight"-104);
	window.onresize = function(){
		var bodyWidth = document.documentElement.clientWidth,
				bodyHeight = document.documentElement.clientHeight;
		$(".home-menu").height(bodyHeight-104);
		$(".home-menu-grid").height(bodyHeight-104);
		$(".home-content").height(bodyHeight-104);
		$(".home-content").css("max-height","bodyHeight"-104);
	}
</script>
<script src="/app/common/menu.js" type="text/javascript" charset="utf-8"></script>
<!--BMap LIB start-->
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=Om5N5uYSAWfedXNL4ItqdxVqz0vHHqvk"></script>
<script type="text/javascript" src="/app/js/GPSToBaidu.js"></script>
<script type="text/javascript" src="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.js"></script>
<script type="text/javascript" src="http://api.map.baidu.com/library/GeoUtils/1.2/src/GeoUtils_min.js"></script>
<script type="text/javascript" src="http://api.map.baidu.com/library/TextIconOverlay/1.2/src/TextIconOverlay_min.js"></script>
<script type="text/javascript" src="http://api.map.baidu.com/library/MarkerClusterer/1.2/src/MarkerClusterer_min.js"></script>
<!--BMap LIB end-->
<!--TABLE LIB start-->
<script src="/app/lib/grid/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
<script src="/app/lib/grid/bootstrap-table.min.js" type="text/javascript" charset="utf-8"></script>
<!--<script src="lib/grid/bootstrap-table.js" type="text/javascript" charset="utf-8"></script>-->
<script src="/app/lib/grid/locale/bootstrap-table-zh-CN.min.js" type="text/javascript" charset="utf-8"></script>
<script src="/app/lib/layui/layui.all.js" type="text/javascript" charset="utf-8"></script>
<script src="/app/lib/convertPinyin.js" type="text/javascript" charset="utf-8"></script>
<script src="/app/lib/grid/BootstrapMenu.min.js" type="text/javascript" charset="utf-8"></script>

<script src="/app/lib/grid/jquery.contextMenu.js" type="text/javascript" charset="utf-8"></script>
<script src="/app/lib/infoBox/InfoBox_min.js" type="text/javascript" charset="utf-8"></script>
<script src="/app/common/common.js" type="text/javascript" charset="utf-8"></script>
<script src="/app/js/map.js" type="text/javascript" charset="utf-8"></script>
<script src="/app/js/main.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>
