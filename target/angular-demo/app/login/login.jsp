<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String baseUrl = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<title>变电站电力作业安全智能预警管控系统</title>
		<link rel="shortcut icon" href="/app/img/logo/logo.ico" />
		<link rel="stylesheet" href="./css/login.css">
		<link rel="stylesheet" type="text/css" href="font/iconfont.css"/>
		<script>
			var baseUrl = "<%=baseUrl%>";
		</script>
	</head>
	<body>
		<div class="login-page">
			<div class="top">
				<span class="power-logo">
					<img src="./img/logo.png" alt="">
				</span>
				<span class="country-logo"></span>
				<%--如果要打开的话，要去login.css中47行.country-logo把margin-top值去掉--%>
				<%--<span class="login-company">国网邯郸电网公司</span>--%>
				<span class="sys-title">变电站电力作业安全智能预警管控系统</span>
			</div>
			<div class="main">
				<div class="sub-main">
					<div class="login-img">
						<%--<div class="login-tower-img"></div>--%>
					</div>
					<div class="login">
						<form class="login-from" onsubmit="return false">
							<p class="error-tip"></p>
							<span class="login-title">登录</span>
							<p class="login-item">
								<input type="text" class="username" placeholder="请输入用户名" tabindex="1"/>
								<a href="javascript:void(0)" class="login-icon">
									<i class="iconfont icon-username"></i>
								</a>
							</p>
							<p class="login-item">
								<input type="password" class="password" placeholder="请输入密码" tabindex="2"/>
								<a href="javascript:void(0)" class="login-icon">
									<i class="iconfont icon-password"></i>
								</a>
							</p>
							<p class="login-item">
								<a href="javascript:void(0)" class="remember-btn">
									<span class="login-rempwd"></span>
									<span>记住密码</span>
								</a>
							</p>
							<p class="login-item">
								<button class="login-btn">登录</button>
							</p>
						</form>
					</div>
				</div>
			</div>
			<div class="bottom">
				<div class="bottom-info">
					<span>服务支持</span>
					<span class="line-sp"></span>
					<span>关于我们</span>
					<p>
						copyright@2017 北京彦拓科技有限公司，保留所有权利。技术支持：TEL:010-51645202
					</p>
				</div>
			</div>
		</div>
		<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
		<script src="<%=baseUrl%>/app/common/common.js"></script>
		<script type="text/javascript">
			$(function(){
				var co = getCookie("loginInfo");
				if(co){
					var loginInfo = co.split("@");
					if(loginInfo[2]==="yes"){//记住密码了
						var u_name = loginInfo[0],
								u_pwd = loginInfo[1];
						$(".username").val(u_name);
						$(".password").val(u_pwd);
						$(".remember-btn").find(".login-rempwd").addClass("checked");
					}
				}
				//点击记住密码
				$(".remember-btn").click(function(){
					$(this).find(".login-rempwd").toggleClass("checked");
				});
				//登录按钮点击事件
				$(".login-btn").click(function(){
					var username = $(".username").val().trim(),
							pwd = $(".password").val().trim();

					if(!username){
						showErrorTip("用户名不能为空！");
						$(".username").addClass("login-text-error");
						return false;
					}
					if(!pwd){
						showErrorTip("密码不能为空！");
						$(".password").addClass("login-text-error");
						return false;
					}
					$(".login-btn").text("登录中...");
					//调用登录接口
					$.ajax({
						type:"POST",
						url:baseUrl+"login.do",
						data: {userName:username,password:pwd},
						dataType: "json",
						success:function(resp){
							if(resp && resp.success!=="false"){
								//检查是否记住密码
								var isRememberPwd = $(".remember-btn").find(".checked");
								var _cookie = username+"@"+pwd;
								_cookie += isRememberPwd.length>0?"@yes":"@no";
								setCookie("loginInfo",_cookie,1);
								window.location.href = baseUrl+"/index.do";
							}else if(resp && resp.success==="false"){
								showErrorTip(resp.message);
							}else{
								showErrorTip("用户名或密码不正确！");
							}
							return false;
						},
						error:function(){
							showErrorTip("系统繁忙，请稍后再试！");
							return false;
						}
					})

				});
				//绑定Enter按键
				$("body").on("keyup",function(e){
					e = e || window.event;
					//Enter键
					if(e.keyCode===13){
						$(".login-btn").trigger("click");
					}
				})
				function showErrorTip(msg){
					$(".error-tip").text(msg);
					$(".error-tip").show();
					setTimeout(function(){
						$(".error-tip").fadeOut().text("");
					},2500);
					$(".login-btn").text("登录");
				}
				$(".login-text").focus(function(){
					$(this).removeClass("login-text-error");
				});

				//底部高度
				var h = $(window).height()-$(".top").height()-$(".main").height();
				$(".bottom").height(h);
			})
		</script>
	</body>
</html>
