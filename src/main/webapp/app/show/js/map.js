var h = $(".containt-center").height();
var w = $(".containt-center").width();
$(".map-box").height(h);
$(".map-box").width(w);
var chart,		//全国地图
	myChartProe,//省级地图
	myChartCity;//市级地图
var initProv,	//初始化省
	initCity;	//初始化市

var LoadMap = function(options){
	this.chart;	//全国地图
	this.myChartProe;//省级地图
	this.myChartCity;//市级地图
	
	var opts = {
		initProv : "河北",
		initCity : "邯郸市",
		initType:"china"	//默认加载全国地图
	}
	this.config = $.extend(opts, options||{});
	this.init();
}
LoadMap.prototype = {
	init : function(){
		var me = this;
		var config =  me.config;
		var flag =config.initType;
		switch(flag){
			case "prov":
				var opt = {
					name:config.initProv
				};
				me.loadProvMap(opt);
				break;
			case "city":
				var opt = {
					name:config.initCity
				};
				me.loadCityMap(opt,config.initProv);
				break;
			default:
				me.loadChinaMap();
		}
	},
	//加载全国地图
	loadChinaMap : function(){
		var me = this;
		$.get('json/china.json', function (mapJson) {
			echarts.registerMap('china', mapJson);
		    me.chart = echarts.init(document.getElementById('china_map'));//在id为mainMap的dom元素中显示地图
		  	me.chart.setOption({
		        tooltip: {
		            trigger: 'item',
//		            formatter: function (result){//回调函数，参数params具体格式参加官方API
//		               return result.name+'<br />数据:'+result.value;
//		            } 
		        },
		        dataRange:{  
		           	min:0,  
		           	max:50,  
		           	splitNumber:0,
		          	text:['高','低'],  
		            realtime:false,
		           	calculable:false,
		           	selectedMode:false,
		           	realtime:false,
		           	show:false,
		           	itemWidth:10,
		           	itemHeight:60,
		           	color:['lightskyblue','#f2f2f2']
		        }, 
				title:{
			        text:'全国数据总览',
			        textStyle: {  
		                fontWeight: 'normal',              //标题颜色  
		                color: '#FFFFFF'  
		            },  
			        //subtext:'',
			        x:'center',
			        y:'top',
			        textAlign:'left'
			    },
		        series: [{
		            type: 'map',
		            map: 'china',//要和echarts.registerMap（）中第一个参数一致
		            scaleLimit: { min: 0.8, max: 1.9 },//缩放
		            mapLocation:{  
		                y:60  
		            }, 
		            zoom:1.1,
	            	nameMap :{
	            		'鍗楁捣璇稿矝':'南海诸岛',
	            		'南海诸岛':'南海诸岛',
	            	},
		            itemSytle:{  
		                emphasis:{label:{show:false}}  
		            }, 
		            label: {
		                normal: {
		                    show: true
		                },
		                emphasis: {
		                    show: true
		                }
		            },
		            data : [
		            	{name:'湖南',value:'5'},  
		                {name:'北京',value:'20'},  
		                {name:'内蒙古',value:'10'},  
		                {name:'贵州',value:'30'},  
		                {name:'福建',value:'50'},  
		                {name:'甘肃',value:'15'},
		                {name:'云南',value:'33'},
		                {name: '南海诸岛',value: 0, itemStyle:{ normal:{opacity:0,label:{show:false}}} }
		            ]//dataParam//人口数据：例如[{name:'济南',value:'100万'},{name:'菏泽'，value:'100万'}......]
		        }]
		    });
		    //点击任意一个省
		    me.chart.on('click', function (result) {
		    	me.loadProvMap(result);
		    });
	    });
	},
	//加载省级地图
	loadProvMap : function(result){
		var me = this;
		setTimeout(function () {
				$('#china_map').css('display','none');
				$('#city_map').css('display','none');
				$('#proe_map').css('display','block');
		}, 500);
	       	 //选择省的单击事件
		var selectName = result.name;//汉字
       	var selectProe = ConvertPinyin(selectName);//拼音
		//由于山西和陕西拼音一样，所有陕西的拼音为陕西3  代表的是第三声
		if(selectName =="陕西"){
			selectProe = ConvertPinyin("陕西3");
		}
		$.get('json/'+selectProe+'/'+selectProe+'.json', function (Citymap) {
	  		echarts.registerMap(selectProe, Citymap);
	  		me.myChartProe = echarts.init(document.getElementById('proe_map'));
	  		me.myChartProe.setOption({
		        tooltip: {
		            trigger: 'item'
		            //formatter: function loadData(result){//回调函数，参数params具体格式参加官方API
		            //    return selectName+'<br />数据:';
		            //}
		        },
		        dataRange:{  
	                min:0,  
	                max:50,  
	                text:['高','低'],  
	                realtime:false,
	                calculable:false,
	                splitNumber:0,
	               	itemWidth:10,
	               	show:false,
	               	itemHeight:60,
	               	color:['lightskyblue','#f2f2f2']
	            }, 
				title:{
			        text:selectName+'数据总览',
			        textStyle: {  
		                fontWeight: 'normal',              //标题颜色  
		                color: '#FFFFFF'  
		            },  
			        x:'center',
			        y:'top',
			        textAlign:'left'
			    },
		        series: [{
		            type: 'map',
		            map: selectProe ,//要和echarts.registerMap（）中第一个参数一致
		            scaleLimit: { min: 0.8, max: 1.9 },//缩放
		            mapLocation:{  
                        y:60  
                    },  
                    itemSytle:{  
                        emphasis:{label:{show:false}}  
                    }, 
		            label: {
		                normal: {
		                    show: true
		                },
		                emphasis: {
		                    show: true
		                }
		            },
		            data : [
		            	{name:'大方县',value:'5'},  
                        {name:'金沙县',value:'0'},  
                        {name:'织金县',value:'10'},  
                        {name:'七星关区',value:'30'},  
                        {name:'纳雍县',value:'50'},  
                        {name:'赫章县',value:'15'}
		            ]
		        }]
		    });
		    //点击省份的某个市级地图
		    me.myChartProe.on('click',function(rel){
		    	me.loadCityMap(rel,selectProe);
		    })
	  	});
	},
	loadCityMap : function(rel,selectProe){
		var me = this;
		setTimeout(function () { 
			function contains(arr, obj) {  
	    var i = arr.length;  
	    while (i--) {  
	        if (arr[i] === obj) {  
	            return true;  
	        }  
	    }  
	    return false;  
	}
	var arr = new Array('北京','上海','天津','台湾','重庆');
		if(contains(arr,selectProe)==false){
			$('#china_map').css('display','none');
			$('#proe_map').css('display','none');
			$('#city_map').css('display','block');
		}
		else{
			$('#china_map').css('display','none');
			$('#proe_map').css('display','block');
			$('#city_map').css('display','none');
		}
			
	}, 500);
	//选择市的单击事件
	var selectName = rel.name;//汉字
   	var selectCity = ConvertPinyin(selectName);//拼音
	//    调取后台数据
	$.get('json/'+ConvertPinyin(selectProe)+'/'+selectCity+'.json', function (Citymap) {
	  		echarts.registerMap(selectCity, Citymap);
	  		me.myChartCity = echarts.init(document.getElementById('city_map'));
	  		me.myChartCity.setOption({
		        tooltip: {
		            trigger: 'item'
//		            formatter: function loadData(result){//回调函数，参数params具体格式参加官方API
//		                return result.name+'<br />数据:'+result.value;
//		            } 
		        },
		        dataRange:{  
	                min:0,  
	                max:50,  
	                text:['高','低'],  
	                realtime:false,
	                show:false,
	                calculable:false,
	                splitNumber:0,
	               	itemWidth:10,
	               	itemHeight:60,
	               	color:['lightskyblue','#f2f2f2']
	            }, 
				title:{
			        text:selectName+'数据总览',
			        textStyle: {  
		                fontWeight: 'normal',              //标题颜色  
		                color: '#FFFFFF'  
		            },  
			        //subtext:'',  
			        x:'center',
			        y:'top',
			        textAlign:'left'
			    },
		        series: [{
		            type: 'map',
		            map: selectCity ,//要和echarts.registerMap（）中第一个参数一致
		            scaleLimit: { min: 0.8, max: 1.9 },//缩放
		             mapLocation:{  
                        y:60  
                    },  
                    itemSytle:{  
                        emphasis:{label:{show:false}}  
                    }, 
		            label: {
		                normal: {
		                    show: true
		                },
		                emphasis: {
		                    show: true
		                }
		            },
		            data : [
		            	{name:'大方县',value:'5'},  
                        {name:'金沙县',value:'0'},  
                        {name:'织金县',value:'10'},  
                        {name:'七星关区',value:'30'},  
                        {name:'纳雍县',value:'50'},  
                        {name:'赫章县',value:'15'}
		            ]//dataParam//人口数据：例如[{name:'济南',value:'100万'},{name:'菏泽'，value:'100万'}......]
		        }]
		    })
	  		me.myChartCity.on('click',function(rel){
	  			setTimeout(function () { 
					$('#cont_pro_map').css('display','block');
   					$('#cont_city_map').css('display','none');
				}, 500);
	  			
	  		})
	  	});
	},
	setBoxStyle : function(id){
		var _h = $(".containt-center").height();
		var _w = $(".containt-center").width();
		$("#"+id).height(_h);
		$("#"+id).width(_w);
	}
}
var initParams = {
	initProv : "河北",
	initCity : "邯郸市",
	initType:"city"
}
var showMap = new LoadMap(initParams);


window.onresize = function(){
	var _h = $(".containt-center").height();
	var _w = $(".containt-center").width();
	$(".map-box").height(_h);
	$(".map-box").width(_w);
	if(showMap.chart){
		showMap.chart.resize();
	}
	if(showMap.myChartProe){
		showMap.myChartProe.resize();
	}
	if(showMap.myChartCity){
		showMap.myChartCity.resize();
	}
}
function goBackChinaMap(){
	$('#proe_map').remove();
	$('#city_map').remove();
	if(!showMap.chart){
		showMap.loadChinaMap();
	}
	$('#china_map').after('<div class="map-box" id="city_map"></div>')
					.after('<div class="map-box" id="proe_map"></div>');
	showMap.setBoxStyle("proe_map");
	showMap.setBoxStyle("city_map");
	$('#china_map').css('display','block');
	$('#city_map').css('display','none');
	$('#proe_map').css('display','none');
}
function goBackProeMap(){
	if(!showMap.myChartProe){
		var opt = {
			name:initParams.initProv
		}
		showMap.loadProvMap(opt);
	}
	$('#china_map').css('display','none');
	$('#city_map').css('display','none');
	$('#proe_map').css('display','block');
}