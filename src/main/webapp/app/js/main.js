;!function(){
	layer = layui.layer
			,laydate = layui.laydate
			,form = layui.form;
}();
var hasWarnData = false;
/**
 * 实时位置点击事件
 * **/
$(function(){
	initLocationGrid();
	loadRouteInfo();
	var co = getCookie("openWarn");
	//开启了报警声音
	if(co && co == "yes"){
		$(".open-warning").attr("checked",true);
	}
	$(".location-item").click(function(){
		//改变按钮样式
		$(".location-params").find(".location-item").removeClass("location-checked");
		$(this).addClass("location-checked");
		//获取点击的按钮序号，1为实时位置，2为历史查询
		var index = $(this).data("index");
		getHeight();
		switch (index){
			case 1:
				$(".history-box").hide();
				$(".tower-box").hide();
				$(".location-box").show();
				initLocationGrid();
				break;
			case 2:
				$(".location-box").hide();
				$(".tower-box").hide();
				$(".history-box").show();
				initHistoryLine();
				break;
			default:
				$(".location-box").hide();
				$(".history-box").hide();
				$(".tower-box").show();
				initTowerInfo();
		}

	});
})
//加载实时位置信息
function initLocationGrid(){
	//加载列表
	$("#main-location-table").bootstrapTable({
		height: getHeight()-30,
		method:"post",
		url:basePath+"/selectCurrPosition.do",
		dataType:"json",
		pagination: true, //分页
		sidePagination: "client",//服务器端分页server  如果是客户端分页 是 client
		pageSize: 1000,
		pageList: [10, 25, 50, 100],
		responseHandler: function(resp) {
			checkLogin(resp);
			var data =[]
			if(resp.success){
				data = resp.data;
			}
			return data;
		},
		onLoadSuccess: function(data) { //加载成功时执行
			_contextMenu_(data);
		},
		onLoadError: function() { //加载失败时执行
			layer.msg("系统繁忙，请稍后再试", { time: 1500, icon: 2 });
		},
		queryParams: function(params) { //查询的参数
			//return {
			//	pageNumber: params.offset + 1,
			//	pageSize: params.limit,
			//};
		},
		onClickRow:function(row){
			checkLocation(row)
		},
		columns: [
			{
				field:"id",
				visible:false
			},
			{
				field: 'peopleName',
				title: '姓名',
				sortable:true,
				width:'70',
				align: 'center'
			}, {
				field: 'mobile',
				title: '手机号',
                width:'88',
				align: 'center'
			}, {
				field: 'taskName',
				title: '作业名称',
				align: 'center'
			}, {
				field: 'peopleId',
				visible:false
			}, {
				field: 'peopleName',
				visible:false
			}, {
				field: 'routeName',
				visible:false
			}, {
				field: 'mobile',
				visible:false
			}, {
				field: 'taskId',
				visible:false
			}, {
				field: 'routeId',
				visible:false
			}, {
				field: 'SIMH',
				visible:false
			}
		]
	})
}
function getHeight(){
	// return $(window).height()-307;
	return $(window).height()-257;
}
$(window).resize(function () {
	$('.main-self-table').bootstrapTable('resetView', {
		height: getHeight()
	});
	var hasMenu = $(".home-menu").css("display");
	if(hasMenu =="block"){
        var li = $(".home-menu-item").find(".first-level");
        var menuH = $(".home-menu").height();//获取左侧菜单总高度
        var menuLength = li.length;
        var thisH =menuH-(menuLength*35) ;
        $(".home-menu").find(".open-title").find(".sub-menu").height(thisH);

	}
});
//加载历史信息
function initHistoryLine(){
    var laydate = layui.laydate;
    //日期
    laydate.render({
        elem: '#his-date'
    });
	//加载列表
	$("#main-history-table").bootstrapTable({
		height: $(window).height()-357,
		pagination: true, //分页
		sidePagination: "client",//服务器端分页server  如果是客户端分页 是 client
		pageSize: 3,
		pageList: [10, 25, 50, 100],
		responseHandler: function(resp) {
			var data = resp.result;
			return data;
		},
		onLoadSuccess: function(data) { //加载成功时执行
			_contextMenu_(data);
			//							alert("加载成功" + data);
			//							$("th.bs-checkbox .th-inner").append("<div class='my-grid-checkbox'></div>");
		},
		onLoadError: function() { //加载失败时执行
			//							layer.msg("加载数据失败", { time: 1500, icon: 2 });
		},
		queryParams: function(params) { //查询的参数
			//var subcompany = $('#subcompany option:selected').val();
			var name = "";
			return {
				pageNumber: params.offset + 1,
				pageSize: params.limit,
				//companyId:subcompany,
				name: name
			};
		},
		columns: [
			{
				field:"id",
				visible:false
			},
			{
				field: 'name',
				title: '员工姓名',
				sortable:true,
				align: 'center'
			}, {
				field: 'phone',
				title: '员工手机号',
				align: 'center'
			}
		],
		url: "/app/pages/data.json"
	})
}
//加载杆塔信息
function initTowerInfo(){
	//加载列表
	$("#main-tower-table").bootstrapTable({
		height: getHeight(),
		pagination: true, //分页
		sidePagination: "client",//服务器端分页server  如果是客户端分页 是 client
		pageSize: 3,
		pageList: [10, 25, 50, 100],
		responseHandler: function(resp) {
			var data = resp.result;
			return data;
		},
		onLoadSuccess: function(data) { //加载成功时执行
			_contextMenu_(data);
			//							alert("加载成功" + data);
			//							$("th.bs-checkbox .th-inner").append("<div class='my-grid-checkbox'></div>");
		},
		onLoadError: function() { //加载失败时执行
			//							layer.msg("加载数据失败", { time: 1500, icon: 2 });
		},
		queryParams: function(params) { //查询的参数
			//var subcompany = $('#subcompany option:selected').val();
			var name = "";
			return {
				pageNumber: params.offset + 1,
				pageSize: params.limit,
				//companyId:subcompany,
				name: name
			};
		},
		columns: [
			{
				field:"id",
				visible:false
			},
			{
				field: 'name',
				title: '责任人',
				sortable:true,
				align: 'center'
			}, {
				field: 'phone',
				title: '塔杆编号',
				align: 'center'
			}, {
				field: 'id',
				title: 'ID',
				visible:false,
				align: 'center'
			}
		],
		url: "/app/pages/data.json"
	})
}
//加载作业信息
function loadRouteInfo(){
	$.ajax({
		type:"post",
		url:basePath +"/selectAllTask.do",
		dataType:"json",
		success:function(resp){
			checkLogin(resp);
			if(resp.success){
				//获取所有作业信息
				var data = resp.data;
				if(data.length>0){
					for(var i=0;i<data.length;i++){
						$(".curr-location-route").append("<option value='"+data[i].id+"'>"+data[i].name+"</option>");
					}
				}
			}
		},
		error:function(e){
			layer.msg("系统繁忙，请稍后再试！");
			return false;
		}
	});
}

function checkLocation(row){
	var lgt = row.dWJD,lat = row.dWWD;
	var obj = {
		name:row.peopleName,
		id:row.id,
		peopleId:row.peopleId,
		routeName:row.routeName
	};
	var pt = new BMap.Point(lgt, lat);
	var myIcon = new BMap.Icon("/app/img/map/photo.png", new BMap.Size(48,48),{
		//				anchor:new BMap.Size(32,64)
	});
	var marker = new BMap.Marker(pt,{icon:myIcon});  // 创建标注
	// selfMap.map.addOverlay(marker);
	selfMap.map.centerAndZoom(pt,15);
	var label = new BMap.Label(row.id);
	label.setStyle({
		display:"none"
	});
	marker.setLabel(label);
	//将员工的marker 添加到数组
	selfMap.markerArr.push(marker);
	//给标注添加click事件
	selfMap.addMarkerClick(obj,marker,pt,"person");
	//点击行的时候，通过作业Id获取本次作业上的所有杆塔。并用红色实线连接起来
	var taskId = row.taskId;
	if(taskId){
		selfMap.map.removeOverlay(selfMap.polineArr[0]);
		selfMap.polineArr.length = 0;
		$.ajax({
			type:"POST",
			url:basePath+"/selectTowerByTaskId.do",
			data:{
				id:taskId
			},
			dataType:"json",
			success:function(resp){
				if(resp.success){
					var data = resp.data;
					var toweArr = [];
					if(data.length>1){
						for(var i=0;i<data.length;i++){
							var lat = data[i].lat,
								lng = data[i].lan;
							var point = new BMap.Point(lng, lat);
							toweArr.push(point);
						}
					}
					var BPoint  = GpsToBaiduPoints(toweArr);

					var polyline = new BMap.Polyline(BPoint, {strokeColor:"red", strokeWeight:2, strokeOpacity:0.5});   //创建折线
					//下面的是虚线
					// var polyline = new BMap.Polyline(BPoint, {strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5,strokeStyle:"dashed"});   //创建折线
					selfMap.polineArr.push(polyline);
					selfMap.map.addOverlay(selfMap.polineArr[0]);   //增加折线
				}
			},
			error: function(){
				layer.msg("系统繁忙，请稍后再试！");
				return false;
			}
		});
	}
}
//右键菜单
function _contextMenu_(data){
	var menu = new BootstrapMenu('.main-self-table tr', {
		fetchElementData: function(rowElem) {
			var rowId = rowElem.data('index');
			return data[rowId];
		},
		actions: [
			{
				name:"查看轨迹",
				onClick: function(row){
					clearTimeout(markerMove);
					selfMap.map.removeOverlay(polyline);
					selfMap.map.removeOverlay(polyMarker);
					var eqNum = row.sIMH;
					$.ajax({
						type:"POST",
						url:basePath+"/selectGJ.do",
						data:{
							eqNum:eqNum
						},
						dataType:"json",
						success:function(resp){
							if(resp.success){
								var data = resp.data;

								if(data.length==0){
									layer.msg("当前设备暂无轨迹信息！");
									return false;
								}
								var pts = [];
								for(var i=0;i<data.length;i++){
									var lat = data[i].lat,
										lng = data[i].lan;
									var pt = new BMap.Point(lng,lat);
									pts.push(pt);
								}
								var BPoint  = GpsToBaiduPoints(pts);//转换成百度坐标

								polyline = new BMap.Polyline(BPoint,
									{
										strokeColor:"blue",//设置颜色
										strokeWeight:3, //宽度
										strokeOpacity:1,
										strokeStyle:"dashed"
									});//透明度

								selfMap.map.addOverlay(polyline);
								var myIcon = new BMap.Icon("/app/img/map/photo.png", new BMap.Size(48,48),{
									anchor:new BMap.Size(12,48)
								});
								polyMarker = new BMap.Marker(BPoint[0],{icon:myIcon});
								selfMap.map.addOverlay(polyMarker);

								selfMap.addMarkerClick(row,polyMarker,BPoint[0],"person");

								i=0;
								var paths = BPoint.length;    //获得有几个点
								function resetMkPoint(i){
									polyMarker.setPosition(BPoint[i]);
									if(i < paths){
										markerMove = setTimeout(function(){
											i++;
											resetMkPoint(i);
										},1000);
									}
								}
								resetMkPoint(0);
							}
						},
						error: function(){
							layer.msg("系统繁忙，请稍后再试！");
							return false;
						}
					});
				}
			}
		]
	});
}

/**
 * 加载警告信息
 **/
loadWarningList();
function loadWarningList(){
	$(".warnint-table").bootstrapTable({
		height: 170,
		method:'post',
        contentType : "application/x-www-form-urlencoded",
		responseHandler: function(resp) {
			var data = [];
			if(resp.success){
				data  = resp.data;
				if(data.length>0){
					hasWarnData = true;//有警告信息
					$(".show-waring-icon").show();
					var warnType = $(".open-warning").attr("checked");
					if(warnType && warnType == "checked"){
						var audio = document.getElementById('warn-voice');
						audio.play();
					}
				}else {
					hasWarnData = false;
					$(".show-waring-icon").css("display","none");
				}
			}
			return data;
		},
		onLoadSuccess: function(data) { //加载成功时执行
		},
		onLoadError: function() { //加载失败时执行
		},
		queryParams: function(params) { //查询的参数
			return {
				status: "0"
			};
		},
		columns: [
			{
				field: 'id',
				visible:false
			},
			{
				field: 'eqNum',
				title: '终端号',
				sortable:true,
				align: 'center'
			}, {
				field: 'person',
				title: '姓名',
				align: 'center'
			}, {
				field: 'phone',
				title: '手机号',
				align: 'center'
			}, {
				field: 'time',
				title: '报警时间',
				align: 'center',
				formatter: function(val){
					var time = dateFormatterFn(val,"full");
					return time;
				}
			}, {
				field: 'content',
				title: '报警内容',
				align: 'center'
			}, {
                field: 'lng',
                title: '经度',
                align: 'center'
            }, {
                field: 'lat',
                title: '纬度',
                align: 'center'
            },{
				title: '操作',
				align: 'center',
				formatter:function(value, row, index){
					return "<a class='handle-case'>处理</a>"
				},
				events:{
					"click .handle-case" : function(e, value, row, index){
						var id = row.id;//获取当前数据的id
                        handleWarnFn(id);
					}
				}
			}
		],
		url: basePath+"/selectWarnInfo.do"
	});
}

//监听是否开启报警声音
$(".open-warning").change(function(){
	var isChecked = $(this).prop("checked");//true 选中了，false 未选中
	if(isChecked){
		layer.msg("开启了报警声音");
		var _cookie = "yes";
		setCookie("openWarn",_cookie,1);
	}else{
		layer.msg("关闭了报警声音");
		var _cookie = "no";
		setCookie("openWarn",_cookie,1);
	}
});
//一键处理
$(".fast-handle").click(function(){
	// layer.alert("您点击了一键处理事件");
   var datas =  $('.warnint-table').bootstrapTable("getData");
   var ids = [];
   for(var i=0;i<datas.length;i++){
   		ids.push(datas[i].id);
   }
	handleWarnFn(ids.join(","));
});
//处理报警请求
function handleWarnFn(ids){
	if(ids){
		$.ajax({
			type:'post',
			url:basePath+'/updateWarnInfo.do',
			data:{ids:ids},
			dataType:"json",
			success:function(resp){
				if(resp.success){
                    $('.warnint-table').bootstrapTable("refresh");
                    layer.msg("已处理！");
				}else{
                    layer.msg("系统繁忙，请稍后再试！");
				}
			},
			error:function(){
				layer.msg("系统繁忙，请稍后再试！");
				return false;
			}
		});
	}
}
//插入行，后期删掉
$(".add-handle").click(function(){
	// layer.alert("您点击了一键处理事件");
	var data = {
        id:"2",
        eqNum:"DHCG737733",
        person:"李四",
        phone:"13555554444",
        time:"2017-05-01 14:22:22",
        content:"电量不足",
		lat:"29.999999",
		lng:"101.2222222"
    }
    //新插入行，永远处在第一行
    $('.warnint-table').bootstrapTable('insertRow', {
        index:0,
        row:data
    });
	//向末尾插入行
    // $('.warnint-table').bootstrapTable('append', params);
});
//关闭警示框
$(".close-map-tips").click(function(){
	$(".map-tips").animate({
		height:0
	},100,"linear",function(){
		$(".map-tips").removeClass("my-fadeInUp");
		if(hasWarnData){
			$(".show-waring-icon").fadeIn(200);
		}
	});
});
//显示报警警示框
$(".show-waring-icon").click(function(){
	$(".map-tips").addClass("my-fadeInUp").height("200px");
	$(this).fadeOut(200);
});
//导出报警信息
$(".export-warn").click(function(){
    var url = basePath+"/tableToExcel.do",
		thead = $(".warnint-table thead").html(),
		tbody = $(".warnint-table tbody").html(),
        table = "<table>"+thead+tbody+"</table>";
    data = [
        {
            name:"html",
            value:table
        },
        {
            name:"sheetName",
            value:""
        },
        {
            name:"fileName",
            value:"报警信息.xls"
        },
        {
            name:"type",
            value:""
        }
    ];
    ajaxPostLoadFile(url,data);
});
//实时位置重置
$(".location-reset").click(function(){
	$(".search-location").find("input").val("");
	$(".search-location").find("select").val("");
});
//实时位置查询
$(".location-search").click(function(){
	var name = $(".curr-search-name").val().trim(),
			mobile = $(".curr-search-mobile").val().trim(),
			taskId = $(".curr-location-route").val();
	$.ajax({
		type:"post",
		url:basePath+"/selectCurrPosition.do",
		data:{
			peopleName:name,
			mobile:mobile,
			taskId:taskId
		},
		dataType:"json",
		success:function(resp){
			checkLogin(resp);
			if(resp){
				var data = resp.data;
				$("#main-location-table").bootstrapTable("load",data);
			}else{
				layer.msg("系统繁忙，请稍后再试！");
				return false;
			}
		},error:function(e){
			layer.msg("系统繁忙，请稍后再试！");
			return false;
		}
	});
});
//修改密码
$(".update-pwd").click(function(){
    layer_show(508,371,"修改密码","/app/pages/userMgr/updatePwd.html")
});
//退出系统
$(".login-out").click(function(){
	layer.confirm("是否确认退出系统？",
			{btn:["是","否"]},
			function(){
				$.ajax({
					type:"post",
					url:basePath+"/userLogout.do",
					success:function(resp){
						if(resp){
							top.location.href = basePath +"/app/login/login.jsp";
						}
					},
					error:function(e){
						layer.msg("系统繁忙，请稍后再试！");
						return false;
					}
				});
			}
	);
});

//伸缩按钮
$(".show-arrow").click(function(){
    var isClose = $(this).hasClass("close-arrow");
    var h =  $(".home-menu-outer").height();
    if(isClose){
        $(".home-menu-outer").width(260);
        // $(".home-menu-grid").css("overflow","auto");
    }else{
        $(".home-menu-outer").width(0);
        $(".home-menu-outer").height(h);
        $(".home-menu-grid").css("overflow","hidden");
    }
    $(this).toggleClass("close-arrow");
});
function closeInfoBox(){
	$(".person-info-box").remove();
}
//点击变电站人物头像
$(".person-photo").click(function(){
	// var html = `
	// 	<p>
	// 		<span>姓名：</span>张三
	// 	</p>
	// 	<p>
	// 		<span>手机号：</span>15655555555
	// 	</p>
	// 	<p>
	// 		<span>位置：</span>${selfMap.BDaddress}
	// 	</p>
	// `
	var html = `
		<div class="person-info-box show-marker-box animated flipInY">
                                    <p class="title"><span >人员信息</span>
                                    <span class="box-close" onclick="closeInfoBox()"><img src="/app/img/map/close.png" style="width: 18px;height: 18px;margin-top:4px"/></span></p>
                                    <div class="person-map-info">
                                    <div class="person-map-photo"><img src="'+basePath+'/pic/'+datas[0].photoImg+'" alt="暂无头像"></div>
                                    <div class="person-map-tip">
                                    <p class="box-info">姓&nbsp;&nbsp;名：<span>张三</span></p>
                                    <p class="box-info">岗&nbsp;&nbsp;位：<span>工程人员</span></p>
                                    <p class="box-info">所属部门：<span>工程部</span></p>
                                    <p class="box-info">紧急联系人：<span>张四</span></p>
                                    </div>
                                    </div>
                                    <div class="person-map-other">
                                    <p class="box-info">设备编号：<span>2008154547</span></p>
                                    <p class="box-info">变电站名称：<span>永沙</span></p>
                                    <p class="box-info">作业名称：<span>测试一</span></p>
                                    <p class="box-info">预警内容：<span>暂无</span></p>
                                    <p class="box-info">经&nbsp;&nbsp;度：<span>114.2541547</span></p>
                                    <p class="box-info">纬&nbsp;&nbsp;度：<span>25.6354785</span></p>
                                    <p class="box-info">实时位置：<span>河北省邯郸市</span></p>
                                    <p class="box-info">GPS 时间：<span>2018-01-11 11:21:25</span></p>
                                    </div>
                                    </div>
	`;
	$(".home-map").append(html);
	// layer.alert(html,{title:"人员信息"});
});

//变电站点击返回
$(".main-map-back").click(function(){
	$(".show-bian-dian").css("display","none");
});