var MainMap = function(option){
    this.config = {
        basePath : ''
    };
    this.map,
	this.plyArr = [],//电子围栏集合
	this.markerArr = [];//实时位置集合
    this.polineArr = [];
    this.infoBox;//地图弹出层
    this.init(this.config);
    this.BDaddress = "";
    this.showBox = true;
};

MainMap.prototype = {
    constructor:MainMap,
    init : function(options){
        this.config = $.extend(this.config,options||{});
        this.initMap();
    },
    initMap : function(){
        var that = this;
        that.map= new BMap.Map("allmap");    // 创建Map实例
        that.map.centerAndZoom(new BMap.Point(114.545628,36.629062), 12);  // 初始化地图,设置中心点坐标和地图级别
        that.map.setCurrentCity("邯郸");          // 设置地图显示的城市 此项是必须设置的
        that.map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
        that.map.enableKeyboard();
        that.map.addControl(new BMap.NavigationControl({anchor: BMAP_ANCHOR_BOTTOM_RIGHT}));
        that.map.addControl(new BMap.ScaleControl({anchor: BMAP_ANCHOR_BOTTOM_LEFT}));
        that.map.addControl(new BMap.MapTypeControl({
            anchor: BMAP_ANCHOR_TOP_RIGHT,
            type: BMAP_MAPTYPE_CONTROL_HORIZONTAL,
            mapTypes: [BMAP_NORMAL_MAP, BMAP_SATELLITE_MAP, BMAP_HYBRID_MAP]
        }));
        //地图加载完毕
        var _loadFirst = false;
        //tilesloaded 每次缩放或移动的时候 都会调用，所以加载的时候 只需要判定首次加载就行
        that.map.addEventListener("tilesloaded",function(){
            _loadFirst = true;
        });
        var _intval = setInterval(function(){
            if(_loadFirst){
                clearInterval(_intval);
                //加载变电站信息
                that.initTowerMarker();
                //加载电子围栏
                // that.showDangerArea();
                //加载人员位置
                that.initPersonLocation();
                //加载日期以及天气
                // that.initTimeAndSwither();
                //定时刷新
                that.timedRefresh();
            }
        },500);
    },
	//定时刷新
    timedRefresh : function(){
    	var that = this;
        setTimeout(function(){
            //清空人员marker，再打点
            if(that.markerArr.length>0){
                if( that.infoBox){
                    that.infoBox.close();
                }
                for(var i=0;i<that.markerArr.length;i++){
                    that.map.removeOverlay(that.markerArr[i])
                }
            }
            //加载人员位置
            that.initPersonLocation();
            that.timedRefresh();
            $(".location-search").trigger("click");
        },5*60000);
	},
    //加载杆塔信息
    initTowerMarker : function(){
        var that = this;
        $.ajax({
            type:"post",
            url:basePath+"/selectAllRoute.do",
            dataType:"json",
            success:function(resp){
                checkLogin(resp);
                if(resp.success){
                    var data = resp.data;
                    let juheMarkers = [];
                    for(var i=0;i<data.length;i++){
                        var pt = new BMap.Point(parseFloat(data[i].lng),parseFloat(data[i].lat));
                        var BPoints = [];
                        BPoints.push(pt);
                        var BPoint  = GpsToBaiduPoints(BPoints)[0];
                        var myIcon = new BMap.Icon("/app/img/map/biandianzhan.png", new BMap.Size(64,64),{
                            //				anchor:new BMap.Size(32,64)
                        });
                        var marker = new BMap.Marker(BPoint,{icon:myIcon});  // 创建标注
                        // that.map.addOverlay(marker);
                        juheMarkers.push(marker);
                        //给标注添加click事件
                        that.addMarkerClick(data[i],marker,BPoint,"tower");
                        //添加标注的mouseover事件
                        that.addMarkerFocus(data[i],marker,BPoint);
                    }

                    var markerClusterer = new BMapLib.MarkerClusterer(that.map, {markers:juheMarkers});

                }
            },
            error:function(){
                layer.msg("系统繁忙，请稍后再试！");
                return false;
            }
        });
    },
    //加载人员实时位置
    initPersonLocation : function(){
        var that = this;
        //清空marker集合
        that.markerArr.length = 0;
        $.ajax({
            type:"post",
            url:basePath+"/selectCurrPosition.do",
            dataType:"json",
            success:function(resp){
                if(resp.success=="false"){
                    //注销了
                }else if(resp.success){
                    var data = resp.data;
                    var sysTime = resp.currTime;
                    for(var i=0;i<data.length;i++){
                        var pt = new BMap.Point(parseFloat(data[i].dWJD),parseFloat(data[i].dWWD));
                        var BPoints = [];
                        BPoints.push(pt);
                        var BPoint  = GpsToBaiduPoints(BPoints)[0];
                        var iconType = "photo.png";
                        var gpsTime = data[i].gPST;
                        //超过30分钟 即为离线 原地不动？？等确认需求
                        if(sysTime - gpsTime>30*60000){
                            iconType = "photo_temp.png";
                        }
                        var myIcon = new BMap.Icon("/app/img/map/"+iconType, new BMap.Size(30,50),{
                            //				anchor:new BMap.Size(32,64)
                        });
                        var marker = new BMap.Marker(BPoint,{icon:myIcon});  // 创建标注
						that.markerArr.push(marker);
                        that.map.addOverlay(marker);
                        //给标注添加click事件
                        that.addMarkerClick(data[i],marker,BPoint,"person");
                    }
                }
            },
            error:function(e){
                console.log(e);
            }
        });
    },
    //鼠标移动到标注上，显示标注信息
    addMarkerFocus: function(obj,marker,point){
        var that = this;
        marker.addEventListener("mouseover",function(e){
                if(that.showBox){
                    $(".infoBox").remove();
                }
                that.BMapAdress(e,function(address){
                    var powerLevel = ["10KV","35KV","220kV","500kV"];
                    var powerType = ["类型一","类型二"];
                    var towerStatus = "<br/>";
                    var _flat = obj.taskNames||"暂无";
                    if(_flat){
                        var _fArr = _flat.split(";");
                        for(var k=0;k<_fArr.length;k++){
                            var tsk = _fArr[k].split("@");
                            towerStatus += tsk[0]+(tsk[1]=="已完成"?"<span style='color:green'>已完成</span>":"<span style='color:red'>未完成</span>")+"<br/>";
                        }
                    }
                    var box = '<div class="show-marker-box show-marker-box-tower animated flipInY">'+
                        '<p class="title"><span >变电站信息</span>'+
                        '<span class="box-close" onclick="closeInfoBox()"><img src="/app/img/map/close.png" style="width: 18px;height: 18px;margin-top:4px"/></span></p>'+
                        '<p class="box-info">变电站名称：<span>'+that.formatterStr(obj.name)+'</span></p>'+
                        '<p class="box-info">变电站类型：<span>'+that.formatterStr(powerType[obj.type])+'</span></p>'+
                        '<p class="box-info">电压等级：<span>'+that.formatterStr(powerLevel[obj.voltageLevel])+'</span></p>'+
                        '<p class="box-info">责任人&nbsp;：<span>'+that.formatterStr(obj.header)+'</span></p>'+
                        '<p class="box-info">作业人&nbsp;：<span>'+that.formatterStr(obj.perspnName)+'</span></p>'+
                        '<p class="box-info">作业状态：<span>'+towerStatus+'</span></p>'+
                        '<p class="box-info">地&nbsp;&nbsp;址：<span>'+address+'</span></p>'+
                        '</div>';
                    that.infoBox = new BMapLib.InfoBox(that.map,box,{
                        boxStyle:{
                            width: "220px",
                            //				marginLeft: "110px"
                        },
                        offset:{
                            width:0,
                            height:10
                        },
                        enableAutoPan: true,
                        align: INFOBOX_AT_TOP
                    });
                    if(that.showBox){
                        that.infoBox.open(marker);
                        that.showBox = false;
                    }

                });
        });
        marker.addEventListener("mouseout",function(){
            that.showBox = true;
        });
    },
    //标注点击事件
    addMarkerClick : function(obj,marker,point,type){
        var that = this;
        var opts = {
            width : 200,     // 信息窗口宽度
            height: 100,     // 信息窗口高度
            title : "提示"  // 信息窗口标题
        };
        //点击变电站，进入详细页面
        marker.addEventListener("click",function(e){
            that.BMapAdress(e,function(address){
                $(".infoBox").remove();
                var _box1 = "";
                var powerLevel = ["10KV","35KV","220kV","500kV"];
                //杆塔弹窗
                var _box2 = "";
                if(type=="person"){
                    var datas  =[];
                    $.ajax({
                        type:"post",
                        url:basePath+"/selectPersonByParams.do",
                        data:{id:obj.peopleId,equipmentNo:obj.sIMH},
                        dataType:"json",
                        async:false,
                        success:function(resp){
                            checkLogin(resp);
                            if(resp.success){
                                datas = resp.data;
                                if(datas.length==0){
                                    // layer.msg("该设备【"+obj.sIMH+"】未绑定人员信息！");
                                    var tips ="<p>设备终端号："+obj.sIMH+"</p>" +
                                        "<p>GPS定位时间："+dateFormatterFn(obj.gPST,"full")+"</p>"+
                                        "<p>经度："+(obj.dWJD).toFixed(5)+"</p>"+
                                        "<p>纬度："+(obj.dWWD).toFixed(5)+"</p>"+
                                        "<p>位置："+address+"</p>";
                                    layer.alert(tips,{title:"该设备未绑定人员信息"});
                                    return;
                                }
                                var depart = that.formatterStr(datas[0].departmentId);
                                depart = depart=="暂无"?"暂无":depart=="1"?"工程部":"运检部";
                                //人员弹窗
                                _box1 ='<div class="show-marker-box animated flipInY">'+
                                    '<p class="title"><span >人员信息</span>'+
                                    '<span class="box-close" onclick="closeInfoBox()"><img src="/app/img/map/close.png" style="width: 18px;height: 18px;margin-top:4px"/></span></p>'+

                                    '<div class="person-map-info">' +
                                    '<div class="person-map-photo"><img src="'+basePath+'/pic/'+datas[0].photoImg+'" alt="暂无头像"></div>' +
                                    '<div class="person-map-tip">' +
                                    '<p class="box-info">姓&nbsp;&nbsp;名：<span>'+that.formatterStr(datas[0].name+'【'+datas[0].mobile+'】')+'</span></p>'+
                                    '<p class="box-info">岗&nbsp;&nbsp;位：<span>'+that.formatterStr(datas[0].position)+'</span></p>'+
                                    '<p class="box-info">所属部门：<span>'+depart+'</span></p>'+
                                    '<p class="box-info">紧急联系人：<span>'+that.formatterStr(datas[0].relativeName+'【'+datas[0].relativeMobile+'】')+'</span></p>'+
                                    '</div>' +
                                    '</div>'+
                                    '<div class="person-map-other">' +
                                    '<p class="box-info">设备编号：<span>'+that.formatterStr(datas[0].equipmentNo)+'</span></p>'+
                                    '<p class="box-info">变电站名称：<span>'+that.formatterStr(obj.routeName)+'</span></p>'+
                                    '<p class="box-info">作业名称：<span>'+that.formatterStr(datas[0].taskNames)+'</span></p>'+
                                    '<p class="box-info">预警内容：<span>'+that.formatterStr(datas[0].warnInfo)+'</span></p>'+
                                    '<p class="box-info">经&nbsp;&nbsp;度：<span>'+(obj.dWJD).toFixed(5)+'</span></p>'+
                                    '<p class="box-info">纬&nbsp;&nbsp;度：<span>'+(obj.dWWD).toFixed(5)+'</span></p>'+
                                    '<p class="box-info">实时位置：<span>'+address+'</span></p>'+
                                    '<p class="box-info">GPS 时间：<span>'+dateFormatterFn(obj.gPST,"full")+'</span></p>'+
                                    '</div>'+
                                    '</div>';
                            }else{
                                layer.msg("系统繁忙，请稍后再试！");
                                return false;
                            }
                        },
                        error:function(e){
                            layer.msg("系统繁忙，请稍后再试！");
                            return false;
                        }

                    });
                }else{
                    that.BDaddress = address;
                    var _posArr =[
                        {b:"25%",l:"20%"},
                        {b:"23%",l:"64%"},
                        {b:"38%",l:"72%"},
                        {b:"50%",l:"64%"},
                        {b:"13%",l:"28%"}
                    ];
                    var ran = Math.floor(Math.random()*(5));
                    $(".person-photo").css({
                        bottom:_posArr[ran].b,
                        left:_posArr[ran].l
                    });
                    $(".show-bian-dian").show();
                    return false;
                    var status = obj.reachInfo;
                    var flag = "未完成",reachTime = "";
                    var towerStatus = "<br/>";
                    if(status){
                        var statusArr = status.split("@");
                        reachTime = statusArr[1];
                        flag = "已完成("+reachTime+")";
                        var routeList = status.split("#");
                        if(routeList.length>0){
                            for(var i=0;i<routeList.length;i++){
                                var routeItems = routeList[i].split("@");
                                if(routeItems[2] && routeItems[2]!="null"){
                                    //已经巡检过了
                                    towerStatus += routeItems[0]+"<span style='color:green'>已完成</span>【"+routeItems[2]+"】<br/>";
                                }else{
                                    towerStatus += routeItems[0]+"<span style='color:red'>未完成</span><br/>";
                                }
                            }
                        }

                    }
                    _box2 = '<div class="show-marker-box show-marker-box-tower animated flipInY">'+
                        '<p class="title"><span >杆塔信息</span>'+
                        '<span class="box-close" onclick="closeInfoBox()"><img src="/app/img/map/close.png" style="width: 18px;height: 18px;margin-top:4px"/></span></p>'+
                        '<p class="box-info">杆塔名称：<span>'+that.formatterStr(obj.name)+'</span></p>'+
                        '<p class="box-info">线路名称：<span>'+that.formatterStr(obj.routeNames)+'</span></p>'+
                        '<p class="box-info">塔型名称：<span>直线塔型</span></p>'+
                        '<p class="box-info">电压等级：<span>'+that.formatterStr(powerLevel[obj.voltegeClass])+'</span></p>'+
                        '<p class="box-info">责任人&nbsp;：<span>'+that.formatterStr(obj.people)+'</span></p>'+
                        '<p class="box-info">作业人&nbsp;：<span>'+that.formatterStr(obj.personNames)+'</span></p>'+
                        '<p class="box-info">作业状态：<span>'+towerStatus+'</span></p>'+
                        '<p class="box-info">地&nbsp;&nbsp;址：<span>'+address+'</span></p>'+
                        '</div>';
                }
                var box = type=="tower"?_box2:_box1;
                that.infoBox = new BMapLib.InfoBox(that.map,box,{
                    boxStyle:{
                        width: "220px",
                        //				marginLeft: "110px"
                    },
                    offset:{
                        width:0,
                        height:10
                    },
                    enableAutoPan: true,
                    align: INFOBOX_AT_TOP
                });
                that.infoBox.open(marker);
            });
        });
    },
    formatterStr :function(str){
        if(!str){
            str = "暂无";
        }
        return str;
    },
    BMapAdress:function(e,callback){
        var address = "";
        var geoc = new BMap.Geocoder();
        var pt = e.point;
        geoc.getLocation(pt, function(rs){
            var addComp = rs.addressComponents;
            address = addComp.province + ", " + addComp.city + ", " + addComp.district + ", " + addComp.street + ", " + addComp.streetNumber;
            callback(address);
        });
    },
    showDangerArea : function(){
        var that = this;
        $.ajax({
            type:"post",
            url:basePath+"/selectAllTrap.do",
            dataType:"json",
            success:function(resp){
                checkLogin(resp);
                if(resp.success){
                    var polygonArr = [];
                    var data = resp.data;
                    if(data.length>0){
                        for(var i=0;i<data.length;i++){
                            //先判断是圆形还是矩形 0 圆圈；1 长方形；2多边形'
                            var perperty = data[i].perperty;
                            var id = data[i].id;
                            var name = data[i].name;
                            if(perperty == 0){
                                var lng = data[i].circleLng,
                                    lat = data[i].circleLat,
                                    raduis = data[i].circleRadius;
                                polygonArr.push({
                                    id:id,
                                    type:"circle",
                                    name:name,
                                    points:[
                                        new BMap.Point(lng,lat)
                                    ],
                                    raduis:raduis
                                })
                            }else if(perperty == 1){
                                var leftTopLat = data[i].leftTopLat,
                                    leftTopLng = data[i].leftTopLng,
                                    rightTopLat = data[i].rightTopLat,
                                    rightTopLng = data[i].rightTopLng,
                                    rightBottomLat = data[i].rightBottomLat,
                                    rightBottomLng = data[i].rightBottomLng,
                                    leftBottomLat = data[i].leftBottomLat,
                                    leftBottomLng = data[i].leftBottomLng;
                                polygonArr.push({
                                    id:id,
                                    type:"rectangle",
                                    name:name,
                                    points:[
                                        new BMap.Point(leftTopLng,leftTopLat),
                                        new BMap.Point(rightTopLng,rightTopLat),
                                        new BMap.Point(rightBottomLng,rightBottomLat),
                                        new BMap.Point(leftBottomLng,leftBottomLat)
                                    ]
                                });
                            }
                        }
                        that.showTrap(polygonArr);
                    }
                }
            },
            error:function(e){
                layer.msg("系统繁忙，请稍后再试！");
                return false;
            }
        });
        //
        //for(var i=0;i<polygonArr.length;i++){
        //	 //显示电子围栏
        //	let _p = polygonArr[i].points,
        //	 	id = polygonArr[i].id;
        //    let _polygon = new BMap.Polygon(_p, {strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5,id:"1001"});  //创建多边形
        //	//将一个个电子围栏放入数组中，供以后判断是否有人进入这个区域使用
        //	that.plyArr.push(_polygon);
        //	//将电子围栏显示出来
        //	that.map.addOverlay(_polygon);
        //	var pt=new BMap.Point(122.031989,39.727614);
        //		that.map.addOverlay(new BMap.Marker(pt));
        //	if (BMapLib.GeoUtils.isPointInPolygon(pt, _polygon)) {//如果点在区域内，返回true
        //       layer.msg("警告，有人进入危险区了！")
        //    }else{
        //    	layer.msg("处在安全区");
        //	}
        //}
    },
    closeInfoBox : function(){
        this.infoBox.close();
    },
    showTrap : function(polygonArr) {
        var polygon = [];
        for(var i=0;i<polygonArr.length;i++){
            //显示电子围栏
            var  _p = polygonArr[i].points,
                type = polygonArr[i].type,
                name = polygonArr[i].name,
                id = polygonArr[i].id;
            if(type=="circle"){
                var radius = polygonArr[i].raduis;
                var point = new BMap.Point(116.404, 39.915);
                var circle = new BMap.Circle(_p[0],radius,{strokeColor:"blue", strokeWeight:2, strokeOpacity:0.3});
                this.map.addOverlay(circle);
                continue;
            }
            var _polygon = new BMap.Polygon(_p, {strokeColor:"blue", strokeWeight:2, strokeOpacity:0.3,id:"1001"});  //创建多边形
            //将一个个电子围栏放入数组中，供以后判断是否有人进入这个区域使用
            polygon.push(_polygon);
            this.map.addOverlay(_polygon);
        }
    }
}

var selfMap = new MainMap();
var polyline,polyMarker,markerMove;
//关闭地图弹窗
function closeInfoBox(){
    selfMap.closeInfoBox()
}
// window.onresize = function(){
//     selfMap.map.resize();
// }