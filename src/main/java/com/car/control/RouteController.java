package com.car.control;

import com.alibaba.fastjson.JSON;
import com.car.service.RouteService;
import com.car.vo.RouteVO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by yinqing on 2017/10/24.
 */
@Controller
public class RouteController {
    @Autowired
    private RouteService service;

    private static Logger logger = Logger.getLogger(RouteController.class);


    /**
     * 查询所有路线（作业）
     * @param routeVO
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/selectAllRoute.do", method = RequestMethod.POST)
    public void selectAllRoute(RouteVO routeVO,
                               HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        try {
            List<RouteVO> list = service.selectAllRoute(routeVO);
            json = JSON.toJSON(list).toString();
            json = "{\"success\":true,\"dataCount\":"+list.size()+",\"data\":"+json+"}";
            logger.info("查询所有线路信息总数======"+list.size());
        }catch (Exception e){
            e.printStackTrace();
            json = "{\"success\":false,\"message\":"+e+"}";
        }
        response.getWriter().write(json);
    }

    /**
     * 新增路线（作业）
     * @param routeVO
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/addRoute.do", method = RequestMethod.POST)
    public void addRoute(RouteVO routeVO,
                         HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        String tips = "";
        try{
            if(null!=routeVO.getName() && routeVO.getName().length()>0){
                service.addRoute(routeVO);
                tips = "添加成功";
                json = "{\"success\":true,\"message\":\""+tips+"\"}";
            }else{
                tips = "名称不能为空";
                json = "{\"success\":false,\"message\":\""+tips+"\"}";
            }
        }catch (Exception e){
            e.printStackTrace();
            json = "{\"success\":false,\"message\":\""+e+"\"}";
        }
        response.getWriter().write(json);
    }

    /**
     * 修改路线（作业）
     * @param routeVO
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/updateRoute.do", method = RequestMethod.POST)
    public void updateRoute(RouteVO routeVO,
                            HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        String tips = "";
        try{

            if(null!=routeVO.getName() && routeVO.getName().length()>0){
                service.updateRoute(routeVO);
                tips = "修改成功";
                json = "{\"success\":true,\"message\":\""+tips+"\"}";
            }else{
                tips = "名称不能为空";
                json = "{\"success\":false,\"message\":\""+tips+"\"}";
            }
        }catch (Exception e){
            e.printStackTrace();
            json = "{\"success\":false,\"message\":"+e+"}";
        }
        response.getWriter().write(json);
    }

    /**
     * 删除线路（作业）信息
     * @param ids
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/deleteRouteById.do", method = RequestMethod.POST)
    public void deleteRouteById(String[] ids,
                                HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        String msg = "";
        try{
            service.deleteRouteById(ids);
            msg = "删除成功！";
            json = "{\"success\":true,\"message\":\""+msg+"\"}";
        }catch (Exception e){
            e.printStackTrace();
            msg = "系统繁忙，请稍后再试！";
            json = "{\"success\":false,\"message\":\""+msg+"\"}";
        }
        response.getWriter().write(json);
    }
}
