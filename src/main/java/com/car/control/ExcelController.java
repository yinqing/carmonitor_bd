package com.car.control;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.car.service.ImportExcelService;
import com.car.util.FileDownloadHelper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 批量导入数据
 * yinqing 2017-12-11
 */
@Controller
public class ExcelController {

    @Autowired
    private ImportExcelService service;

    private static Logger logger  = Logger.getLogger(ExcelController.class);


    @RequestMapping(value = "/importExcel.do", method = RequestMethod.POST)
    public void importPerson(MultipartFile file,String name, HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        logger.info("导入模块是：========"+name);
        try{
            //判断文件是否为空
            if(file==null){
                String tips = "请先上传文件!";
                json = "{\"success\":false,\"message\":\""+tips+"\"}";
                response.getWriter().write(json);
                return;
            }
            //获取文件名
            String filename=file.getOriginalFilename();
            //进一步判断文件是否为空（即判断其大小是否为0或其名称是否为null）
            long size=file.getSize();
            if(filename==null || ("").equals(filename) && size==0) {
                String tips = "未选择导入模板!";
                json = "{\"success\":false,\"message\":\""+tips+"\"}";
                response.getWriter().write(json);
                return;
            };
            String code = "";
            if("xlsx".equalsIgnoreCase(filename.substring(filename.lastIndexOf(".")+1))){
                code = service.handleDataForXlsx(name,filename,file.getInputStream());
            }else{
                code = service.handleDataForXls(name,filename,file.getInputStream());
            }
            if("0".equalsIgnoreCase(code)){
                JSONObject jsStr = JSONObject.parseObject(code);
                String msg = jsStr.getString("count");
                json = "{\"success\":true,\"message\":\"成功导入"+msg+"条数据!\"}";
            }else if(code == "1"){
                json = "{\"success\":false,\"message\":\"对不起,您的模板格式有误,请更换成最新的导入模板!\"}";
            }else {
                json = "{\"success\":false,\"message\":\""+code+"\"}";
            }
        }catch (Exception e){
            e.printStackTrace();
            String tips = "系统繁忙，请稍后再试！";
            json = "{\"success\":false,\"message\":\""+tips+"\"}";
        }
        response.getWriter().write(json);
    }


    @RequestMapping(value = "/downloadExcel.do",method = {RequestMethod.POST,RequestMethod.GET})
    public void downloadExcel(String name, HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        logger.info("要下载的模板是：======="+name);
        FileDownloadHelper.downloadTemplate("template/",name,"",request,response);
    }
}
