package com.car.control;

import com.alibaba.fastjson.JSON;
import com.car.service.WarnService;
import com.car.vo.WarnVO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


@Controller
public class WarnController {

    @Autowired
    private WarnService service;

    private static Logger logger = Logger.getLogger(WarnController.class);

    /**
     * 查询报警信息
     * @param warnVO
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/selectWarnInfo.do",method = RequestMethod.POST)
    public void selectWarnInfo(WarnVO warnVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        try{
            List<WarnVO> list = service.selectWarnInfo(warnVO);
            json = JSON.toJSON(list).toString();
            json = "{\"success\":true,\"dataCount\":"+list.size()+",\"data\":"+json+"}";
        }catch (Exception e){
            e.printStackTrace();
            json = "{\"success\":false,\"message\":"+e+"}";
        }
        response.getWriter().write(json);
    }

    /**
     * 一键处理或者单独处理报警信息
     * @param ids 每条数据的Id，多条用逗号隔开
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/updateWarnInfo.do", method = RequestMethod.POST)
    public void updateWarnInfo(String ids ,HttpServletRequest request,HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String json = "";
        try{
            String [] idArr = ids.split(",");
            if(null!=idArr && idArr.length>0){
                for(int i=0;i<idArr.length;i++){
                    service.updateWarnInfo(idArr[i]);
                }
                json = "{\"success\":true,\"code\":0}";
            }
        }catch (Exception e){
            e.printStackTrace();
            json = "{\"success\":false,\"message\":"+e+"}";
        }
        response.getWriter().write(json);
    }

}
