package com.car.control;

import com.alibaba.fastjson.JSON;
import com.car.service.TrapService;
import com.car.vo.TrapVO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 电子围栏
 * Created by yinqing on 2017/10/25.
 */
@Controller
public class TrapController {

    @Autowired
    private TrapService service;

    private static Logger logger = Logger.getLogger(TrapController.class);

    /**
     * 查询所有围栏信息
     * @param trapVO
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/selectAllTrap.do", method = RequestMethod.POST)
    public void selectAllTrap(TrapVO trapVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        String json = "";
        try {
            List<TrapVO> list = service.selectAllTrap(trapVO);
            json = JSON.toJSON(list).toString();
            json = "{\"success\":true,\"dataCount\":"+list.size()+",\"data\":"+json+"}";
        }catch (Exception e) {
            e.printStackTrace();
            json = "{\"success\":false,\"message\":"+e+"}";
        }
        response.getWriter().write(json);
    }
    /**
     * 新增电子围栏信息
     * @param trapVO
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/addTrap.do", method = RequestMethod.POST)
    public void addTrap(TrapVO trapVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        String json = "";
        String msg = "";
        try{

            int num = service.addTrap(trapVO);
            if(num==0){
                msg = "添加成功";
                json = "{\"success\":true,\"message\":\""+msg+"\"}";
            }else{
                msg = "系统繁忙，请稍后再试";
                json = "{\"success\":false,\"message\":\""+msg+"\"}";
            }
        }catch (Exception e){
            e.printStackTrace();
            json = "{\"success\":false,\"message\":\""+e+"\"}";
        }
        response.getWriter().write(json);
    }

    /**
     * 修改电子围栏信息
     * @param trapVO
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/updateTrap.do", method = RequestMethod.POST)
    public void updateTrap(TrapVO trapVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        String json = "";
        String msg = "";
        try{
            int num = service.updateTrap(trapVO);
            if(num==0){
                msg = "修改成功";
                json = "{\"success\":true,\"message\":\""+msg+"\"}";
            }else{
                msg = "系统繁忙，请稍后再试";
                json = "{\"success\":false,\"message\":\""+msg+"\"}";
            }
        }catch (Exception e){
            e.printStackTrace();
            json = "{\"success\":false,\"message\":\""+e+"\"}";
        }
        response.getWriter().write(json);
    }

    /**
     * 批量删除电子围栏
     * @param deleteInfo: 同一条数据由id和perperty 用都和隔开，不同的数据用分号隔开，例如 "1001,0;1002,1;1003,0"
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/deleteTrapById.do", method = RequestMethod.POST)
    public void deleteTrapById(String deleteInfo,HttpServletRequest request,HttpServletResponse response) throws Exception {
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        String json = "";
        String msg = "";
        try{
            //先将每一条记录分割成数组
            String[] delArr = deleteInfo.split(";");
            for(int i=0;i<delArr.length;i++){
                //获取每一条数据的 id+perperty
                String oneArr = delArr[i];
                //将id和type分开
                String[] delArr2 = oneArr.split(",");
                int id = Integer.parseInt(delArr2[0]);
                int perperty = Integer.parseInt(delArr2[1]);
                service.deleteTrapById(id,perperty);
            }
            msg = "删除成功";
            json = "{\"success\":true,\"message\":\""+msg+"\"}";
        }catch (Exception e){
            e.printStackTrace();
            json = "{\"success\":false,\"message\":\""+e+"\"}";
        }
        response.getWriter().write(json);
    }
}
