package com.car.dao;

import com.car.vo.TowerVO;

import java.util.List;

/**
 * Created by Administrator on 2017/10/23 0023.
 */
public interface ITowerDao {
    List<TowerVO> selectAllTower(TowerVO towerVO) throws Exception;

    String selectPersonNameByPId(Integer pId) throws Exception;

    void addTower(TowerVO towerVO) throws Exception;

    void updateTower(TowerVO towerVO) throws Exception;

    void deleteTowerById(int tId) throws Exception;

    //根据作业管理信息查询杆塔信息
    List<TowerVO> selectTowerByTaskInfo(int taskId) throws Exception;

    List<TowerVO> selectTowerByRouteId(int routeId) throws Exception;


    List<TowerVO> selectTowerByTaskId(Integer taskId) throws Exception;
}
