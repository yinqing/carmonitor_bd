package com.car.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.car.bean.User;

public interface IUserDao {
	public void insertUser(User user) throws Exception;
	public void deleteUserById(String id) throws Exception;
	public void updateUser(User user) throws Exception;
	public User selectUserById(String id) throws Exception;
	public List<User> selectUserByParam(User user) throws Exception;
	public User findUserByNameAndPassword(@Param("name") String name, @Param("password") String password) throws Exception;
	//添加合作伙伴的时候，对用户名和账户名的唯一性检验
	public List<User> selectOnlyByParam(User user) throws Exception;
}