package com.car.dao;

import com.car.bean.People;

import java.util.List;

import com.car.vo.PeopleVO;
import com.car.vo.RouteVO;
import org.apache.ibatis.annotations.Param;

public interface IPeopleDao {

    List<PeopleVO> selectPersonByParams(PeopleVO pVo) throws Exception;

    int addPerson(PeopleVO pVo) throws Exception;

    public void updatePerson(PeopleVO pVo) throws Exception;

    public void deletePersonById(int pId) throws Exception;

    public void insertPersonToEquipment(PeopleVO pVo) throws Exception;

    public void updatePersonToEquipment(PeopleVO pVo) throws Exception;

    //根据设备id删除
    public void deletePersonToEquipment(int id) throws Exception;
    //根据peopleId删除
    public void deletePersonToEqByPid(Integer pId) throws Exception;

    List<PeopleVO> selectPersonByTowerId(int id) throws Exception;

    //根据作业管理ID查询关联的人员信息
    List<PeopleVO> selectPersonByTaskInfo(int taskId) throws Exception;
}