package com.car.dao;

import com.car.vo.HistoryPositionVO;

import java.util.List;

/**
 * 历史查询
 * Created by yinqing on 2017/12/16 0016.
 */
public interface IHistoryPositionDao {
    List<HistoryPositionVO> selectGJ(String eqNum) throws Exception;
}
