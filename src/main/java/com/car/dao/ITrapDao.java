package com.car.dao;

import com.car.vo.TrapVO;

import java.util.List;

/**
 * 电子围栏
 * Created by yinqing on 2017/10/25.
 */
public interface ITrapDao {
    /**
     * 添加电子围栏主表信息
     * @param trapVO
     * @throws Exception
     */
    void addTrap(TrapVO trapVO) throws Exception;

    /**
     * 添加圆形电子围栏
     * @param trapVO
     * @throws Exception
     */
    void addCircle(TrapVO trapVO) throws Exception;

    /**
     * 添加矩形电子围栏
     * @param trapVO
     * @throws Exception
     */
    void addRectangle(TrapVO trapVO) throws Exception;

    /**
     * 修改电子围栏
     * @param trapVO
     * @throws Exception
     */
    void updateTrap(TrapVO trapVO) throws Exception;

    /**
     * 修改圆形电子围栏
     * @param trapVO
     * @throws Exception
     */
    void updateCircle(TrapVO trapVO) throws Exception;

    /**
     * 修改矩形电子围栏
     * @param trapVO
     * @throws Exception
     */
    void updateRectangle(TrapVO trapVO) throws Exception;

    /**
     * 删除电子围栏主表信息
     * @param id
     * @throws Exception
     */
    void deleteTrapById(int id) throws Exception;

    /**
     * 删除圆形电子围栏
     * @param id
     * @throws Exception
     */
    void deleteCircleByTrapId(int id) throws Exception;

    /**
     * 删除矩形电子围栏
     * @param id
     * @throws Exception
     */
    void deleteRectangleByTrapId(int id) throws Exception;

    /**
     * 查询围栏主表信息
     * @param trapVO
     * @return
     * @throws Exception
     */
    List<TrapVO> selectAllTrap(TrapVO trapVO) throws Exception;

    /**
     * 查询圆形围栏信息
     * @param id
     * @return
     * @throws Exception
     */
    List<TrapVO> selectCircleByTrapId(int id) throws Exception;

    /**
     * 查询矩形围栏信息
     * @param id
     * @return
     * @throws Exception
     */
    List<TrapVO> selectRectangleByTrapId(int id) throws Exception;
}
