package com.car.vo;

/**
 * 电子围栏
 * Created by yinqing on 2017/10/25.
 */
public class TrapVO {

    private Integer id;//主键

    private String name;

    private Double lat;

    private Double lan;

    private Float radis;

    private Integer terminalId;//设备ID

    private Integer type;//'围栏类型，0 内；1外',

    private Integer perperty;//'围栏类型：0 圆圈；1 长方形；2多边形'

    private Double circleLat;//圆形的中心纬度

    private Double circleLng;//圆形的中心经度

    private Float circleRadius;//圆形的半径

    private Double leftTopLng;//矩形左上经度

    private Double leftTopLat;//矩形左上纬度

    private Double rightTopLng;//矩形右上经度

    private Double rightTopLat;//矩形右上纬度

    private Double leftBottomLng;//矩形左下经度

    private Double leftBottomLat;//矩形左下纬度

    private Double rightBottomLng;//矩形右下经度

    private Double rightBottomLat;//矩形右下纬度


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLan() {
        return lan;
    }

    public void setLan(Double lan) {
        this.lan = lan;
    }

    public Float getRadis() {
        return radis;
    }

    public void setRadis(Float radis) {
        this.radis = radis;
    }

    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getPerperty() {
        return perperty;
    }

    public void setPerperty(Integer perperty) {
        this.perperty = perperty;
    }

    public Double getCircleLat() {
        return circleLat;
    }

    public void setCircleLat(Double circleLat) {
        this.circleLat = circleLat;
    }

    public Double getCircleLng() {
        return circleLng;
    }

    public void setCircleLng(Double circleLng) {
        this.circleLng = circleLng;
    }

    public Float getCircleRadius() {
        return circleRadius;
    }

    public void setCircleRadius(Float circleRadius) {
        this.circleRadius = circleRadius;
    }

    public Double getLeftTopLng() {
        return leftTopLng;
    }

    public void setLeftTopLng(Double leftTopLng) {
        this.leftTopLng = leftTopLng;
    }

    public Double getLeftTopLat() {
        return leftTopLat;
    }

    public void setLeftTopLat(Double leftTopLat) {
        this.leftTopLat = leftTopLat;
    }

    public Double getRightTopLng() {
        return rightTopLng;
    }

    public void setRightTopLng(Double rightTopLng) {
        this.rightTopLng = rightTopLng;
    }

    public Double getRightTopLat() {
        return rightTopLat;
    }

    public void setRightTopLat(Double rightTopLat) {
        this.rightTopLat = rightTopLat;
    }

    public Double getLeftBottomLng() {
        return leftBottomLng;
    }

    public void setLeftBottomLng(Double leftBottomLng) {
        this.leftBottomLng = leftBottomLng;
    }

    public Double getLeftBottomLat() {
        return leftBottomLat;
    }

    public void setLeftBottomLat(Double leftBottomLat) {
        this.leftBottomLat = leftBottomLat;
    }

    public Double getRightBottomLng() {
        return rightBottomLng;
    }

    public void setRightBottomLng(Double rightBottomLng) {
        this.rightBottomLng = rightBottomLng;
    }

    public Double getRightBottomLat() {
        return rightBottomLat;
    }

    public void setRightBottomLat(Double rightBottomLat) {
        this.rightBottomLat = rightBottomLat;
    }
}
