package com.car.vo;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class TaskVO {
    private Integer id;//主键

    private String name;//作业名称

    private String type;//作业类型 0：月度，1：日常

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date beginTime;//作业开始时间

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endTime;//作业截止时间

    private String monthBgtime;//作业开始时间(月度作业)

    private String monthEdtime;//作业截止时间（月度作业）

    private Integer routeId;//线路ID

    private String routeName;//作业线路名称

    private String peopleIds;//作业线路人员id
    private String peopleNames;//作业线路人员姓名

    private String towerIds;//杆塔ID集合
    private String towerNames;//杆塔名称集合

    private Integer towerCount;//杆塔数量
    private String isEnd;//是否巡检结束

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getMonthBgtime() {
        return monthBgtime;
    }

    public void setMonthBgtime(String monthBgtime) {
        this.monthBgtime = monthBgtime;
    }

    public String getMonthEdtime() {
        return monthEdtime;
    }

    public void setMonthEdtime(String monthEdtime) {
        this.monthEdtime = monthEdtime;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getPeopleIds() {
        return peopleIds;
    }

    public void setPeopleIds(String peopleIds) {
        this.peopleIds = peopleIds;
    }

    public String getPeopleNames() {
        return peopleNames;
    }

    public void setPeopleNames(String peopleNames) {
        this.peopleNames = peopleNames;
    }

    public String getTowerIds() {
        return towerIds;
    }

    public void setTowerIds(String towerIds) {
        this.towerIds = towerIds;
    }

    public String getTowerNames() {
        return towerNames;
    }

    public void setTowerNames(String towerNames) {
        this.towerNames = towerNames;
    }

    public Integer getTowerCount() {
        return towerCount;
    }

    public void setTowerCount(Integer towerCount) {
        this.towerCount = towerCount;
    }

    public String getIsEnd() {
        return isEnd;
    }

    public void setIsEnd(String isEnd) {
        this.isEnd = isEnd;
    }
}
