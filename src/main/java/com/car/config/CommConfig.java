package com.car.config;

/**
 * 公共配置
 * @author zhudingwei
 * @date 2016-6-17
 * @version 1.0
 * @decription
 *
 */
public interface CommConfig {

	//用户类型
	public static final String USER_TYPE_CAR = "0"; //驾校用户
	public static final String USER_TYPE_ADMIN = "1"; //超级管理员
	public static final String USER_TYPE_PARTNER = "2"; //合作伙伴
	
	//设备状态
	public static final String EQ_STATUS_DCK = "0"; //待出库
	public static final String EQ_STATUS_YCK = "1"; //已出库
	public static final String EQ_STATUS_DWX = "2"; //待维修
	
	//业务状态
	public static final String BIZ_STATUS_WKT = "0"; //未开通
	public static final String BIZ_STATUS_YKT = "1"; //已开通
	public static final String BIZ_STATUS_ZT = "2"; //已暂停
	public static final String BIZ_STATUS_YQF = "3"; //已欠费
}
