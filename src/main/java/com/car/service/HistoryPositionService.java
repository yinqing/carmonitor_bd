package com.car.service;

import com.car.dao.IHistoryPositionDao;
import com.car.vo.HistoryPositionVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 历史查询（包括轨迹回放）
 * Created by yinqing on 2017/12/16 0016.
 */
@Service
public class HistoryPositionService {
    @Autowired
    private IHistoryPositionDao dao;
    public List<HistoryPositionVO> selectGJ(String eqNum) throws Exception{
        List<HistoryPositionVO> list = dao.selectGJ(eqNum);
        return list;
    }
}
