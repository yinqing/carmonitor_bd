package com.car.service;

import com.car.dao.IWarnDao;
import com.car.vo.WarnVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WarnService {
    @Autowired
    private IWarnDao dao;

    public List<WarnVO> selectWarnInfo(WarnVO warnVO) throws Exception{
        List<WarnVO> list = dao.selectWarnInfo(warnVO);

        return list;
    }

    public void updateWarnInfo(String id) throws Exception{
        dao.updateWarnInfo(id);
    }
}
