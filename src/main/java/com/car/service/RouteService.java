package com.car.service;

import com.car.dao.IPeopleDao;
import com.car.dao.IRouteDao;
import com.car.dao.ITaskDao;
import com.car.vo.PeopleVO;
import com.car.vo.RouteVO;
import com.car.vo.TaskVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by yinqing on 2017/10/24.
 */
@Service
public class RouteService {
    @Autowired
    private IRouteDao routeDao;
    @Autowired
    private IPeopleDao peopleDao;
    @Autowired
    private ITaskDao taskDao;

    /**
     * 查询所有的线路信息（作业信息）
     * @param routeVO
     * @return
     * @throws Exception
     */
    public List<RouteVO> selectAllRoute(RouteVO routeVO) throws Exception{
        //查询未巡检完成的
        List<RouteVO> list = routeDao.selectAllRoute(routeVO);
        //查询已巡检的
        List<RouteVO> list2 = routeDao.selectRouteOld(routeVO);
        list.addAll(list2);

        TaskVO taskVO = new TaskVO();
        if(null!=list && list.size()>0){
            for (int i=0;i<list.size();i++){
                int id = list.get(i).getId();
                taskVO.setRouteId(id);
                    List<RouteVO> plist = routeDao.selectPeopleInfo(id);
                    List<RouteVO>  tlist = routeDao.selectTowerInfo(id);

                    List<TaskVO>  taskVOList = taskDao.selectAllTaskByParams(taskVO);

                    plist.removeAll(Collections.singleton(null));
                    tlist.removeAll(Collections.singleton(null));
                    String pNames = "";
                    String pIds = "";
                    String tNames = "";
                    String tIds = "";
                    String taskNames = "";
                    if(null!=plist && plist.size()>0){
                       for(int j=0;j<plist.size();j++){
                               String pName = plist.get(j).getPerspnName();
                               Integer pId = plist.get(j).getGroupId();
                               pNames += pName+";";
                               pIds += pId+";";
                       }
                    }
                    if(null!=tlist && tlist.size()>0){
                        list.get(i).setTowerCount(tlist.size());
                        for(int k=0;k<tlist.size();k++){
                                String tName = tlist.get(k).getTowerName();
                                Integer tId = tlist.get(k).getTowerId();
                                tNames += tName+";";
                                tIds += tId+";";
                        }
                    }
                    if(null!=taskVOList && taskVOList.size()>0){
                        for(int m=0;m<taskVOList.size();m++){
                                String _tnames = taskVOList.get(m).getName();
                                String isEnd = taskVOList.get(m).getIsEnd();//是否完毕
                                isEnd = (null!=isEnd)?"已完成":"未完成";
                                if(null!=_tnames){
                                    taskNames += _tnames+"@"+isEnd+";";
                                }

                        }
                    }
                    pNames =(pNames.length()>0)? pNames.substring(0,pNames.length()-1):pNames;
                    pIds =(pIds.length()>0)? pIds.substring(0,pIds.length()-1):pIds;
                    tNames =(tNames.length()>0)? tNames.substring(0,tNames.length()-1):tNames;
                    taskNames =(taskNames.length()>0)? taskNames.substring(0,taskNames.length()-1):taskNames;
                    tIds =(tIds.length()>0)? tIds.substring(0,tIds.length()-1):tIds;
                    list.get(i).setPerspnName(pNames);
                    list.get(i).setPersonIds(pIds);
                    list.get(i).setTowerName(tNames);
                    list.get(i).setTowerIds(tIds);
                    list.get(i).setTaskNames(taskNames);
            }
        }
        return list;

    }

    /**
     * 新增线路（作业）信息
     * @param routeVO
     * @throws Exception
     */
    public void addRoute(RouteVO routeVO) throws Exception{
        String tIds = routeVO.getTowerIds();
        routeDao.addRoute(routeVO);
        int id = routeVO.getId();
        //如果有tower信息 则添加进去
        this.addMiddleTable(id,tIds);
    }

    /**
     * 修改线路（作业）信息
     * @param routeVO
     * @throws Exception
     */
    public void updateRoute(RouteVO routeVO) throws Exception{
        //获取修改信息的主键
        int id = routeVO.getId();
        routeDao.updateRoute(routeVO);
        //修改的时候，将中间表对应的数据删除 再新增
        routeDao.deleteRouteRelationById(id);
        //如果有tower或者people信息 则添加进去
        String towerIds = routeVO.getTowerIds();
        this.addMiddleTable(id,towerIds);
    }

    /**
     * 往两张中间表插值
     * @param id
     * @param towerIds
     * @throws Exception
     */
    public void addMiddleTable(int id,String towerIds) throws Exception{
        //先判断是否有选中杆塔；
        if(null!=towerIds && towerIds.length()>0){
            String[] tIds = towerIds.split(",");
            for (String tId:tIds){
                HashMap<String,String> map = new HashMap();
                map.put("routeId",String.valueOf(id));
                map.put("towerId",tId);
                routeDao.addRouteRelation(map);
            }
            return;
        }
    }

    /**
     * 删除线路（作业）信息
     * @param ids
     * @throws Exception
     */
    public void deleteRouteById(String[] ids) throws Exception{
        if(null!=ids && ids.length>0){
            for(String rId:ids){
                int id = Integer.parseInt(rId);
                routeDao.deleteRouteById(id);
                //将关联表里的数据一并删除
                routeDao.deleteRouteRelationById(id);

                //去route_old里也删一遍
                routeDao.deleteRouteOldByRouteId(id);
            }

        }
    }

    public void updateEquipmentNum(Integer id, String equipmentNo) throws Exception{
        HashMap<String,String> map = new HashMap();
        //先查询这个人有没有在route_ralation表里，如果有，则做更改
       int count =  routeDao.selectRouteRalationByPeopleId(id);
       if(count>0){
           map.put("id",String.valueOf(id));
           map.put("eqNum",equipmentNo);
           routeDao.updateRouteRelationByPeopleId(map);
       }
    }

    public void moveDataToOld() throws Exception{

       List<RouteVO> list = routeDao.checkRouteStatus();
       if(null!=list && list.size()>0){
           for(int i=0;i<list.size();i++){
               int routeId = list.get(i).getId();
               routeDao.deleteRouteById(routeId);
               routeDao.moveDataToOld(list.get(i));
           }
       }
    }
}
