package com.car.bean;

/**
 * Created by yinqing on 2017/10/23 0023.
 */
public class Tower {
    private Integer id;//主键
    private String name;//塔杆名称
    private Short type;//塔杆类型
    private String towerDesc;//塔杆描述
    private Double lat;//纬度
    private Double lan;//经度
    private Float high;//高度
    private Short voltegeClass;//电压级别
    private Integer peopleId;//负责人的ID ，多个由分号相连
    private Float ratius;//半径
    private Integer companyId;//公司ID

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public String getTowerDesc() {
        return towerDesc;
    }

    public void setTowerDesc(String towerDesc) {
        this.towerDesc = towerDesc;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLan() {
        return lan;
    }

    public void setLan(Double lan) {
        this.lan = lan;
    }

    public Float getHigh() {
        return high;
    }

    public void setHigh(Float high) {
        this.high = high;
    }

    public Short getVoltegeClass() {
        return voltegeClass;
    }

    public void setVoltegeClass(Short voltegeClass) {
        this.voltegeClass = voltegeClass;
    }

    public Integer getPeopleId() {
        return peopleId;
    }

    public void setPeopleId(Integer peopleId) {
        this.peopleId = peopleId;
    }

    public Float getRatius() {
        return ratius;
    }

    public void setRatius(Float ratius) {
        this.ratius = ratius;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }
}
