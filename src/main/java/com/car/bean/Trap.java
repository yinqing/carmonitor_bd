package com.car.bean;

/**
 * 电子围栏
 * Created by yinqing on 2017/10/25.
 */
public class Trap {

    private Integer id;//主键

    private String name;

    private Double lat;

    private Double lan;

    private Float radis;

    private Integer terminalId;//设备ID

    private Integer type;//'围栏类型，0 内；1外',

    private Integer perperty;//'围栏类型：0 圆圈；1 长方形；2多边形'

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLan() {
        return lan;
    }

    public void setLan(Double lan) {
        this.lan = lan;
    }

    public Float getRadis() {
        return radis;
    }

    public void setRadis(Float radis) {
        this.radis = radis;
    }

    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getPerperty() {
        return perperty;
    }

    public void setPerperty(Integer perperty) {
        this.perperty = perperty;
    }
}
