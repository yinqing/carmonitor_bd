package com.car.bean;

import java.util.Date;

/**
 * 历史查询
 * Created by yinqing on 2017/12/16 0016.
 */
public class HistoryPosition {
    private Integer id;//主键
    private String simh;//设备编号
    private Double lat;//纬度
    private Double lan;//经度
    private Long speed;//速度
    private Long high;//高度
    private Date gpst;//GPS时间
    private Date rqsj;//接收时间  即数据上传时间
    private Float direction;//方向
    private Integer clzt;//状态
    private String plateNumber;
    private String orgId;
    private String carId ;
    private String electric;//电量

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSimh() {
        return simh;
    }

    public void setSimh(String simh) {
        this.simh = simh;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLan() {
        return lan;
    }

    public void setLan(Double lan) {
        this.lan = lan;
    }

    public Long getSpeed() {
        return speed;
    }

    public void setSpeed(Long speed) {
        this.speed = speed;
    }

    public Long getHigh() {
        return high;
    }

    public void setHigh(Long high) {
        this.high = high;
    }

    public Date getGpst() {
        return gpst;
    }

    public void setGpst(Date gpst) {
        this.gpst = gpst;
    }

    public Date getRqsj() {
        return rqsj;
    }

    public void setRqsj(Date rqsj) {
        this.rqsj = rqsj;
    }

    public Float getDirection() {
        return direction;
    }

    public void setDirection(Float direction) {
        this.direction = direction;
    }

    public Integer getClzt() {
        return clzt;
    }

    public void setClzt(Integer clzt) {
        this.clzt = clzt;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getElectric() {
        return electric;
    }

    public void setElectric(String electric) {
        this.electric = electric;
    }
}
