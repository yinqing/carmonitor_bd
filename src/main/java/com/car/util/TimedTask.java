package com.car.util;

import com.car.service.RouteService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimedTask {

    @Autowired
    private RouteService routeService;
    private static Logger logger  = Logger.getLogger(TimedTask.class);

    /**
     * 检测线路状态，如果status等于1，则，将这条数据放到route_old表中，
     * 并将这条数据从route表中删除
     */
    public void checkRouteStatus() throws Exception{
        SimpleDateFormat dateFormat = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
        String now = dateFormat.format(new Date());
        // 执行业务逻辑
        try{
            System.out.println(now+"定时执行检测线路是否巡检完毕！");
            logger.info(now+"定时执行检测线路是否巡检完毕！");
            routeService.moveDataToOld();
        }catch (Exception e){
            e.printStackTrace();
            logger.error(now+"定时执行检测线路出错！");
        }

    }
}
