package com.car.util;

import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import com.car.bean.LoginUser;
import org.apache.commons.lang3.time.DateUtils;
import sun.misc.BASE64Encoder;

//import com.beidouapp.web.bean.LoginUser;

/**
 * 工具类
 * @author zhudingwei
 * @date 201126
 * @description
 * @version 1.0
 */
public class CommUtil {
	private CommUtil() {}
	
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>主键>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	/**
	 * 产生32位的UUID
	 * @return
	 */
	public static String generate32BitUUID() {
		return java.util.UUID.randomUUID().toString().replace("-", "");
	}
	//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<主键<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>加密解密>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	/**
	 * 产生md5码
	 * @return
	 */
	public static String generateMD5Str(String value) {
		try {
			return new BASE64Encoder().encode(MessageDigest.getInstance("md5").digest(value.getBytes()));
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}
	//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<加密解密<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>日期>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	/**
	 * 以字符串形式返回当前系统时间
	 * @return
	 */
	public static String getCurrentSysTimeToString(String format) {
		try {
			if(format == null || "".equals(format.trim())) {
				format = "yyyy-MM-dd HH:mm:ss";
			}
			SimpleDateFormat df = new SimpleDateFormat(format);
			String time = df.format(new Date());
			return time;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	//将字符串形式的日期转成长整形,例如:2015-12-18
	public static long changeDateToLong(String date) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		df.setLenient(false);
		try {
			Date d = df.parse(date);
			return d.getTime();
		} catch (ParseException e) {
			return 0;
		}
	}
	
	//将字符串形式的日期转成长整形,例如:2015-12-18 18:30:30
	public static long changeTimeToLong(String dateTime) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setLenient(false);
		try {
			Date date = df.parse(dateTime);
			return date.getTime();
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}
	
	//判断字符串是否为有效日期
	public static boolean isValidDate(String date) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		df.setLenient(false);
		try {
			Date d = df.parse(date);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}
	//从Excel中读出的日期格式是43090类似这种；此数字是距离1900年1月1日的天数，以下是转换成2016-8-23格式代码
	public static String getExcelTime(Integer dateNum){
		Calendar c = new GregorianCalendar(1900,0,-1);
		Date d = c.getTime();
		Date _d = DateUtils.addDays(d, dateNum);  //dateNum是距离1900年1月1日的天数
		String date = _d.toLocaleString();

		return date;
	}
	
	//判断字符串是否为有效时间
	public static boolean isValidTime(String date) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setLenient(false);
		try {
			Date d = df.parse(date);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}

	//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<日期<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>字符>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	public static boolean isEmptyStr(String str) {
		if(str == null || "".equals(str.trim()))
			return true;
		return false;
	}
	
	public static boolean isNotEmptyStr(String str) {
		if(str != null && !"".equals(str.trim()))
			return true;
		return false;
	}
	
	//是否中文字
	public static boolean isChineseChar(int code) {
		if(code >=19968 && code <=40869) return true;
		return false;
	}
	
	//是否字母A-Za-z
	public static boolean isEnglishChar(int code) {
		if((code>=65 && code<=90) || (code>=97 && code<=122)) return true;
		return false;
	}
	
	//是否数字
	public static boolean isNumeric(int code) {
		if(code>=48 && code<=57) return true;
		return false;
	}
	
	/**
	 * 将src代表的字符添加到desc代表的字符中,以separator作分隔符
	 * @param desc
	 * @param src
	 * @param separator
	 * @return
	 */
	public static String joinToStr(String desc, String src, String separator) {
		if(CommUtil.isEmptyStr(desc)) {
			desc = src;
		} else {
			if(CommUtil.isNotEmptyStr(src))
				desc += separator + src;
		}
		return desc;
	}
	
	public static void joinToStr(StringBuffer sb,String src,String separator) {
		if(sb.length() == 0) 
			sb.append(src);
		else
			sb.append(separator).append(src);
	}
	//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<字符<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>登录用户>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	public static LoginUser getLoginUserFromSession(HttpServletRequest request){
		return (request.getSession(false)==null?null:(LoginUser) request.getSession().getAttribute("loginUser"));
	}
	//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<登录用户<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	
	//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<验证<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	//验证手机号
	public static boolean isMobile(String mobile) {   
		Pattern p = null;  
        Matcher m = null;  
        boolean b = false;
        
        p = Pattern.compile("^[0-9]{0,16}$"); // 验证手机号  
//        p = Pattern.compile("^[1][3,4,5,8][0-9]{9}$"); // 验证手机号  
        m = p.matcher(mobile);  
        b = m.matches();
        
        return b;  
     }
	
	//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<验证码<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	/**
	 * 产生数字验证码
	 * @param len 验证码长度
	 */
	public static String generateDigitCode(int len) { 
		StringBuffer sb = new StringBuffer();
		char[] c = {'0','1','2','3','4','5','6','7','8','9'};
		
		Random random = new Random(System.currentTimeMillis());
		
		for(int i=0; i<len; i++) {
			int idx = random.nextInt(c.length);
			sb.append(c[idx]);
		}
		return sb.toString();
	}
	
	/**
	 * 产生字符验证码(只包含英文字母和数字)
	 * @param len 验证码长度
	 */
	public static String generateCharacterCode(int len) {
		StringBuffer sb = new StringBuffer();
		char[] c = {'0','1','2','3','4','5','6','7','8','9',
				'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
				'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
		
		Random random = new Random(System.currentTimeMillis());
		
		for(int i=0; i<len; i++) {
			int idx = random.nextInt(c.length);
			sb.append(c[idx]);
		}
		return sb.toString();
	}
	//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<验证码<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	
	/**
	 * 获取类属性集合
	 * @param clazz 
	 * @param bean
	 * @param listExp 要排除的属性
	 * @return 返回字符串格式: 属性名1@属性值1|属性名2@属性值2
	 */
	public static String getBeanDetail(Class clazz,Object bean,List<String> listExp) throws Exception {
		String detail = null;
		Method[] methods = clazz.getMethods();
		for(Method m : methods) {
			String name = m.getName();
			if(name.indexOf("get") == 0 && !"getClass".equals(name)) {
				if(listExp != null && listExp.contains(name)) continue;
				
				String value = String.valueOf(m.invoke(bean, null));
				detail = CommUtil.joinToStr(detail, name.substring(3)+"@"+value, "|");
			}
		}
		return detail;
	}
	
	public static void main(String[] args) {
		System.out.println(CommUtil.generateMD5Str("sys_Admin"));
	}
}
