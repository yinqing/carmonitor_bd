package com.car.control;

import com.alibaba.fastjson.JSON;
import com.car.bean.LoginUser;
import com.car.bean.User;
import com.car.config.CommConfig;
import com.car.service.UserService;
import com.car.util.CommUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


/**
 * 用户
 * @author zhudingwei
 * @date 2016-6-16
 * @version 1.0
 * @decription
 *
 */
@Controller
public class UserController {
	@Autowired
	private UserService service;
	
	//用户首页
	@RequestMapping(value = "/index.do")
	public String indexPage(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String path = request.getContextPath();
		String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
		LoginUser loginUser = CommUtil.getLoginUserFromSession(request);
		if(loginUser != null) {
			HttpSession session = request.getSession();
			session.setAttribute("loginUser", loginUser);
			response.sendRedirect(basePath+"/app/main.jsp");
			return null;
		}
		response.sendRedirect(basePath+"/app/login/login.jsp");
		return null;
//		return "/login.jsp";
	}
	
	/**
	 * 用户登录
	 * @param userName
	 * @param password
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value="/login.do",method=RequestMethod.POST)
	public void login(String userName, String password,HttpServletRequest request, HttpServletResponse response) throws Exception {
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		try {
			LoginUser loginUser = CommUtil.getLoginUserFromSession(request);
			if(loginUser == null) {
				User user = service.findUserByNameAndPassword(userName.trim(),CommUtil.generateMD5Str(password));
				if(user != null) {
					this.setLoginUser(user, request);
					response.getWriter().write("true");
				} else {	
					response.getWriter().write("false");
				}
			} else if(loginUser.getUserName().equals(userName.trim())) {

				response.getWriter().write("true");
			} else {
				response.getWriter().write("{\"success\":\"false\",\"message\":\"已有用户登录了，不能同时登录多个用户！\"}");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.getWriter().write("false");
		}
	}
	private void setLoginUser(User user,HttpServletRequest request) {
		LoginUser loginUser = new LoginUser();
		loginUser.setUserId(String.valueOf(user.getId()));
		loginUser.setUserType(user.getType());
		loginUser.setUserName(user.getName());
		loginUser.setRealName(user.getRealname());
		loginUser.setNickName(user.getNickname());
		loginUser.setOrgId(user.getOrgId());
		if(!CommConfig.USER_TYPE_ADMIN.equals(user.getType())) {
			loginUser.setAuthOrgId(user.getOrgId());
		}
		loginUser.setIp(request.getRemoteAddr());
		
		HttpSession session = request.getSession();
		session.setAttribute("loginUser", loginUser);
	}
	/**
	 * 用户注销
	 * @param request
	 * @param response
	 * @throws IOException*/

	@RequestMapping(value="/userLogout.do")
	protected void userLogout(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			request.getSession().setAttribute("loginUser", null);
			response.getWriter().write("true");
			return;
		} catch(Exception e) {
			e.printStackTrace();
		}
		response.getWriter().write("false");
	}

	/**
	 * 新增用户
	 * @param user
	 * @param request
	 * @param response
     * @throws Exception
     */
	@RequestMapping(value = "/addUser.do", method = RequestMethod.POST)
	public void addUser(User user, HttpServletRequest request, HttpServletResponse response) throws Exception {
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		String json = "";
		String msg = "";
		int code = 0;
		try {
			LoginUser loginUser = CommUtil.getLoginUserFromSession(request);
			if(loginUser == null) {
				msg = "登录过期，请重新登录";
				json = "{\"success\":false,\"message\":\""+msg+"\",\"code\":"+code+"}";
			} else {
				String type = loginUser.getUserType();
				if("1".equals(type)){
					user.setCreateUser(loginUser.getUserName());
					user.setCreateUserid(loginUser.getUserId());
					user.setCreateTime(CommUtil.getCurrentSysTimeToString(null));
					code = service.addUser(user);
					if(code==0){
						msg = "添加成功";
						json = "{\"success\":true,\"message\":\""+msg+"\",\"code\":"+code+"}";
					}else if(code==1){
						msg = "用户名已存在，请重新填写";
						json = "{\"success\":false,\"message\":\""+msg+"\",\"code\":"+code+"}";
					}
				}else {
					msg = "您是普通管理员，没有添加用户的权限；";
					code = 2;
					json = "{\"success\":false,\"message\":\""+msg+"\",\"code\":"+code+"}";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			msg = "系统繁忙，请稍后";
			code = 9;
			json = "{\"success\":false,\"message\":\""+msg+"\",\"code\":"+code+"}";
		}
		response.getWriter().write(json);
	}

	/**
	 * 修改用户名
	 * @param user
	 * @param request
	 * @param response
	 * @throws Exception
     */
	@RequestMapping(value = "/updateUser.do", method = RequestMethod.POST)
	public void updateUser(User user, HttpServletRequest request, HttpServletResponse response) throws Exception {
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		String json = "";
		String msg = "";
		int code = 0;
		try {
			LoginUser loginUser = CommUtil.getLoginUserFromSession(request);
			if(loginUser == null) {
				code = 3;
				msg = "登录过期，请重新登录";
				json = "{\"success\":false,\"message\":\""+msg+"\",\"code\":"+code+"}";
			} else {
				String type = loginUser.getUserType();
				if("1".equals(type)){
					user.setUpdateUser(loginUser.getUserName());
					user.setUpdateUserid(loginUser.getUserId());
					user.setUpdateTime(CommUtil.getCurrentSysTimeToString(null));
					code = service.updateUser(user);
					if(code==0){
						msg = "修改成功";
						json = "{\"success\":true,\"message\":\""+msg+"\",\"code\":"+code+"}";
					}else if(code==1){
						msg = "用户名已存在，请重新填写";
						json = "{\"success\":false,\"message\":\""+msg+"\",\"code\":"+code+"}";
					}
				}else {
					msg = "您是普通管理员，没有修改用户的权限；";
					code = 2;
					json = "{\"success\":false,\"message\":\""+msg+"\",\"code\":"+code+"}";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			msg = "系统繁忙，请稍后";
			code = 9;
			json = "{\"success\":false,\"message\":\""+msg+"\",\"code\":"+code+"}";
		}
		response.getWriter().write(json);
	}

	/**
	 * 删除用户
	 * @param ids
	 * @param request
	 * @param response
	 * @throws Exception
     */
	@RequestMapping(value = "/deleteUserById.do", method = RequestMethod.POST)
	public void deleteUserById(String[] ids, HttpServletRequest request, HttpServletResponse response) throws Exception {
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		String json = "";
		String msg = "";
		int code = 0;
		try{
			LoginUser loginUser = CommUtil.getLoginUserFromSession(request);
			if(loginUser == null) {
				code = 3;
				msg = "登录过期，请重新登录";
				json = "{\"success\":false,\"message\":\""+msg+"\",\"code\":"+code+"}";
			}else{
				String type = loginUser.getUserType();
				if("1".equals(type)){
					service.deleteUserById(ids);
					msg = "删除成功";
					json = "{\"success\":true,\"message\":\""+msg+"\",\"code\":"+code+"}";
				}else {
					msg = "您是普通管理员，没有删除用户的权限；";
					code = 2;
					json = "{\"success\":false,\"message\":\""+msg+"\",\"code\":"+code+"}";
				}
			}
		}catch (Exception e){
			e.printStackTrace();
			msg = "系统繁忙，请稍后再试！";
			json = "{\"success\":false,\"message\":\""+msg+"\"}";
		}
		response.getWriter().write(json);
	}

	/**
	 * 查询用户信息
	 * @param user
	 * @param request
	 * @param response
	 * @throws Exception
     */
	@RequestMapping(value = "/selectUserByParam.do", method = RequestMethod.POST)
	public void selectUserByParam(User user, HttpServletRequest request, HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String json = "";
		try{
			List<User> list = service.selectUserByParam(user);
			json = JSON.toJSON(list).toString();
			json = "{\"success\":true,\"dataCount\":"+list.size()+",\"data\":"+json+"}";
		}catch (Exception e) {
			e.printStackTrace();
			json = "{\"success\":false,\"message\":\"系统繁忙，请稍后再试！\"}";
		}
		response.getWriter().write(json);
	}

	/**
	 * 修改密码
	 * @param oldPwd ：原密码
	 * @param newPwd：新密码
	 * @param request
	 * @param response
     * @throws Exception
	 * code:0:成功；1：原密码不正确； 3：登录过期； 9：系统繁忙（报错）
     */
	@RequestMapping(value = "/updatetPassword.do",method = RequestMethod.POST)
	public void updatetPassword(String oldPwd,String newPwd, HttpServletRequest request, HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String json = "";
		String msg = "";
		int code = 0;
		try {
			//查询原密码是否正确
			LoginUser loginUser = CommUtil.getLoginUserFromSession(request);
			if(loginUser == null) {
				code = 3;
				msg = "登录过期，请重新登录";
				json = "{\"success\":false,\"message\":\""+msg+"\",\"code\":"+code+"}";
			}else{
				//获取当前用户id
				String id = loginUser.getUserId();
				code = service.updatetPassword(id,oldPwd,newPwd);
				if(code==1){
					msg = "原密码不正确！";
					json = "{\"success\":false,\"message\":\""+msg+"\",\"code\":"+code+"}";
				}else {
					//清空session
					request.getSession().setAttribute("loginUser", null);
					msg = "密码修改成功！";
					json = "{\"success\":true,\"message\":\""+msg+"\",\"code\":"+code+"}";
				}
			}
		}catch (Exception e){
			e.printStackTrace();
			msg = "系统繁忙，请稍后再试！";
			code = 9;
			json = "{\"success\":false,\"message\":\""+msg+"\",\"code\":"+code+"}";
		}
		response.getWriter().write(json);
	}
}
