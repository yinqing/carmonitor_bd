/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.5.28 : Database - car_monitor
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`car_monitor` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `car_monitor`;

/*Table structure for table `alarm` */

DROP TABLE IF EXISTS `alarm`;

CREATE TABLE `alarm` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT '报警名称',
  `type` int(11) DEFAULT NULL COMMENT '报警类型',
  `time` datetime DEFAULT NULL COMMENT '报警GPS时间',
  `lat` double DEFAULT NULL COMMENT '经度',
  `lan` double DEFAULT NULL COMMENT '纬度',
  `terminal_id` varchar(45) DEFAULT NULL COMMENT '报警终端sn号码',
  `describe` varchar(512) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `alarm` */

/*Table structure for table `circle` */

DROP TABLE IF EXISTS `circle`;

CREATE TABLE `circle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trap_id` int(11) DEFAULT NULL COMMENT '电子围栏id',
  `lng` double DEFAULT NULL COMMENT '经度',
  `lat` double DEFAULT NULL COMMENT '纬度',
  `radius` float DEFAULT NULL COMMENT '半径',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `circle` */

insert  into `circle`(`id`,`trap_id`,`lng`,`lat`,`radius`) values (1,1,101.446998,35.668495,227891),(2,5,107.738873,39.016612,117063),(3,6,94.272052,39.045312,203280),(4,8,120.21224,44.979338,423687);

/*Table structure for table `class` */

DROP TABLE IF EXISTS `class`;

CREATE TABLE `class` (
  `id` int(11) NOT NULL COMMENT '班id',
  `name` varchar(255) NOT NULL COMMENT '班名称',
  `company_id` int(11) DEFAULT NULL COMMENT '公司Id',
  `department_id` int(11) DEFAULT NULL COMMENT '部门id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `class` */

/*Table structure for table `company` */

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `contact_person` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `company` */

/*Table structure for table `dbpooltest` */

DROP TABLE IF EXISTS `dbpooltest`;

CREATE TABLE `dbpooltest` (
  `a` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `dbpooltest` */

/*Table structure for table `department` */

DROP TABLE IF EXISTS `department`;

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `response_person` varchar(45) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `department` */

/*Table structure for table `equipment` */

DROP TABLE IF EXISTS `equipment`;

CREATE TABLE `equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` smallint(6) DEFAULT NULL COMMENT '终端类型',
  `model` varchar(255) DEFAULT NULL COMMENT '设备型号',
  `sn` varchar(45) DEFAULT NULL COMMENT '终端编号',
  `sim` varchar(45) DEFAULT NULL COMMENT 'sim卡',
  `peopel_id` int(11) DEFAULT NULL COMMENT '人员Id',
  `in_date` datetime DEFAULT NULL COMMENT 'sim卡开通时间',
  `end_date` date DEFAULT NULL COMMENT 'sim截止时间',
  `status` int(2) DEFAULT NULL COMMENT '状态（0：在线；1：离线）',
  `open_date` date DEFAULT NULL COMMENT 'SIM卡开通时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Data for the table `equipment` */

insert  into `equipment`(`id`,`type`,`model`,`sn`,`sim`,`peopel_id`,`in_date`,`end_date`,`status`,`open_date`) values (17,1,'11011','353456789012345','1354444444',NULL,'2017-10-23 15:23:40','2017-10-27',1,'2017-10-02'),(18,1,'SQWQ','QWEQWEQ','13222222222',NULL,'2017-10-23 15:43:56','2017-11-02',1,'2017-10-01'),(19,1,'3123','20171025','13222222222',NULL,'2017-10-23 16:00:46','2017-10-18',1,'2017-10-08'),(20,1,'fdfd','201508071002001','157777777777',NULL,'2017-10-23 16:11:38','2017-10-18',1,'2017-10-08'),(21,2,'W2333','20155555555','13344445556',NULL,'2017-10-25 20:45:03','2017-10-12',1,'2017-10-01');

/*Table structure for table `equipment_assign` */

DROP TABLE IF EXISTS `equipment_assign`;

CREATE TABLE `equipment_assign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `people_id` int(11) NOT NULL COMMENT '人员Id',
  `terminal_id` int(11) DEFAULT NULL COMMENT '终端id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `equipment_assign` */

insert  into `equipment_assign`(`id`,`people_id`,`terminal_id`) values (16,34,17),(17,35,19),(23,41,20);

/*Table structure for table `gjhf` */

DROP TABLE IF EXISTS `gjhf`;

CREATE TABLE `gjhf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `simh` varchar(255) NOT NULL COMMENT '终端sn号码',
  `lat` double DEFAULT NULL COMMENT '经度',
  `lan` double DEFAULT NULL COMMENT '纬度',
  `speed` float DEFAULT NULL COMMENT '速度',
  `high` float DEFAULT NULL COMMENT '高度',
  `gpst` datetime DEFAULT NULL COMMENT '设备GPS时间',
  `rqsj` datetime DEFAULT NULL COMMENT '系统时间',
  `direction` float DEFAULT NULL COMMENT '方向',
  `clzt` int(11) DEFAULT NULL COMMENT '状态',
  `plate_number` varchar(32) DEFAULT NULL,
  `org_id` varchar(16) DEFAULT NULL,
  `car_id` varchar(16) DEFAULT NULL,
  `electric` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

/*Data for the table `gjhf` */

insert  into `gjhf`(`id`,`simh`,`lat`,`lan`,`speed`,`high`,`gpst`,`rqsj`,`direction`,`clzt`,`plate_number`,`org_id`,`car_id`,`electric`) values (1,'353456789012345',2232.9806,11404.9355,0.0001,0,'2008-05-24 06:18:30','2017-10-24 16:32:57',323.87,NULL,NULL,NULL,NULL,NULL),(2,'null',2232.9806,11404.9355,0.0001,0,'2008-05-24 06:18:30','2017-10-25 09:38:55',323.87,NULL,NULL,NULL,NULL,NULL),(3,'null',2232.9806,11404.9355,0.0001,0,'2008-05-24 06:18:30','2017-10-25 10:12:44',323.87,NULL,NULL,NULL,NULL,NULL),(4,'201508071002001',2234.2229,11351.4511,0.0007,0,'2017-10-25 03:00:42','2017-10-25 11:00:53',0,NULL,NULL,NULL,NULL,NULL),(5,'201508071002001',2234.2441,11351.465,0.0001,0,'2017-10-25 03:11:07','2017-10-25 11:11:16',0,NULL,NULL,NULL,NULL,NULL),(6,'201508071002001',2234.2185,11351.4616,0,0,'2017-10-25 04:21:13','2017-10-25 12:21:24',0,NULL,NULL,NULL,NULL,NULL),(7,'353456789012345',22.549676666666667,114.08225833333333,0.0001,0,'2008-05-24 06:18:30','2017-10-25 13:26:40',323.87,NULL,NULL,NULL,NULL,NULL),(8,'353456789012345',22.549676666666667,114.08225833333333,0.0001,0,'2008-05-24 06:18:30','2017-10-25 13:33:24',323.87,NULL,NULL,NULL,NULL,NULL),(9,'353456789012345',22.549677,114.082259,0.0001,0,'2008-05-24 06:18:30','2017-10-25 13:40:59',323.87,NULL,NULL,NULL,NULL,NULL),(10,'201508071002001',22.570341,113.857721,0,0,'2017-10-25 06:01:25','2017-10-25 14:01:31',0,NULL,NULL,NULL,NULL,NULL),(11,'201508071002001',22.570194,113.857677,0.0002,0,'2017-10-25 06:11:24','2017-10-25 14:11:32',0,NULL,NULL,NULL,NULL,NULL),(12,'353456789054321',22.549677,114.082259,0.0001,0,'2008-05-24 06:18:30','2017-11-27 11:10:55',323.87,NULL,NULL,NULL,NULL,'080'),(13,'353456789054321',22.549677,114.082259,0.0001,0,'2008-05-24 06:18:30','2017-11-27 11:15:54',323.87,NULL,NULL,NULL,NULL,'080'),(14,'353456789054321',22.549677,114.082259,0.0001,0,'2008-05-24 06:18:30','2017-11-27 11:20:13',323.87,NULL,NULL,NULL,NULL,'080'),(15,'353456789054321',22.549677,115.082259,0.0001,0,'2008-05-24 06:18:30','2017-11-27 11:38:37',323.87,NULL,NULL,NULL,NULL,'080'),(16,'868020170825210',39.960919,116.459642,0.001,0,'2017-11-27 10:05:35','2017-11-27 18:21:34',0,NULL,NULL,NULL,NULL,'099'),(17,'868020170825210',39.960085,116.45499,0.0142,0,'2017-11-27 10:25:37','2017-11-27 18:26:09',276.39,NULL,NULL,NULL,NULL,'099'),(18,'868020170825314',39.945465,116.432496,0.0154,0,'2017-11-27 10:38:51','2017-11-27 18:39:29',255.73,NULL,NULL,NULL,NULL,'099'),(19,'868020170825210',39.933687,116.427365,0.0154,0,'2017-11-27 10:45:35','2017-11-27 18:46:13',177.03,NULL,NULL,NULL,NULL,'099'),(20,'868020170825314',39.93252,116.419939,0.0001,0,'2017-11-27 10:48:54','2017-11-27 18:49:31',0,NULL,NULL,NULL,NULL,'099'),(21,'868020170825210',39.931999,116.404315,0.0036,0,'2017-11-27 10:55:10','2017-11-27 18:55:46',0,NULL,NULL,NULL,NULL,'099'),(22,'868020170825314',39.930832,116.402352,0.0021,0,'2017-11-27 10:58:58','2017-11-27 18:59:37',0,NULL,NULL,NULL,NULL,'099'),(23,'868020170825210',39.931061,116.402862,0.0009,0,'2017-11-27 11:05:24','2017-11-27 19:05:56',0,NULL,NULL,NULL,NULL,'099'),(24,'868020170825314',39.93082,116.402141,0.0001,0,'2017-11-27 11:09:07','2017-11-27 19:09:36',0,NULL,NULL,NULL,NULL,'099'),(25,'868020170825314',39.931099,116.40289,0.0018,0,'2017-11-27 11:59:45','2017-11-27 20:00:25',0,NULL,NULL,NULL,NULL,'099'),(26,'868020170825210',39.931225,116.402236,0.0002,0,'2017-11-27 00:05:09','2017-11-27 20:06:18',0,NULL,NULL,NULL,NULL,'099'),(27,'868020170825314',39.930987,116.402485,0.0005,0,'2017-11-27 00:09:08','2017-11-27 20:09:38',0,NULL,NULL,NULL,NULL,'099'),(28,'868020170825210',39.931146,116.402565,0.0009,0,'2017-11-27 00:15:23','2017-11-27 20:15:52',0,NULL,NULL,NULL,NULL,'099'),(29,'868020170825314',39.93094,116.402567,0.0005,0,'2017-11-27 00:19:18','2017-11-27 20:19:47',0,NULL,NULL,NULL,NULL,'099'),(30,'868020170825210',39.930897,116.402612,0.0019,0,'2017-11-27 00:25:46','2017-11-27 20:26:18',0,NULL,NULL,NULL,NULL,'099'),(31,'868020170825210',39.93008,116.403226,0.0043,0,'2017-11-27 00:35:17','2017-11-27 20:35:53',195.86,NULL,NULL,NULL,NULL,'099'),(32,'868020170825314',39.929417,116.402012,0.0034,0,'2017-11-27 00:40:21','2017-11-27 20:40:51',0,NULL,NULL,NULL,NULL,'099'),(33,'868020170825210',39.929437,116.40194,0.0059,0,'2017-11-27 00:45:19','2017-11-27 20:45:56',20.83,NULL,NULL,NULL,NULL,'099'),(34,'868020170825314',39.930125,116.402446,0.0093,0,'2017-11-27 00:49:12','2017-11-27 20:49:50',31.05,NULL,NULL,NULL,NULL,'099'),(35,'868020170825289',39.960607,116.459542,0.0015,0,'2017-11-27 16:00:47','2017-11-28 00:01:16',0,NULL,NULL,NULL,NULL,'033'),(36,'868020170825289',39.960454,116.459464,0.0006,0,'2017-11-28 05:11:22','2017-11-28 13:11:50',0,NULL,NULL,NULL,NULL,'000');

/*Table structure for table `people` */

DROP TABLE IF EXISTS `people`;

CREATE TABLE `people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL COMMENT '姓名',
  `position` varchar(45) DEFAULT NULL COMMENT '职位',
  `mobile` varchar(45) DEFAULT NULL COMMENT '手机',
  `relative_name` varchar(45) DEFAULT NULL COMMENT '家属姓名',
  `relative_mobile` varchar(45) DEFAULT NULL COMMENT '家属手机号',
  `class_id` int(11) DEFAULT NULL COMMENT '班号',
  `department_id` int(11) DEFAULT NULL COMMENT '部门号',
  `company_id` int(11) DEFAULT NULL COMMENT '公司序号',
  `equipment_id` int(11) DEFAULT NULL,
  `sex` varchar(2) DEFAULT NULL COMMENT '性别',
  `photo_img` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

/*Data for the table `people` */

insert  into `people`(`id`,`name`,`position`,`mobile`,`relative_name`,`relative_mobile`,`class_id`,`department_id`,`company_id`,`equipment_id`,`sex`,`photo_img`) values (34,'张三','234','13111111111','张四','13222222222',NULL,1,NULL,NULL,'1','e98e6d48-1974-44c4-8550-9c384fe10309.jpeg'),(35,'张柳','运维','13311111111','张四','13222222222',NULL,0,NULL,NULL,'0','0443a742-4e42-492a-bf0b-7c8c6dabc9ab.jpg'),(40,'赵六','后勤','15245785478','找找找','18965985689',NULL,1,NULL,NULL,'1','b9503908-a6fa-442c-bb20-44fcb9c067d2.jpg'),(41,'张四','运维','18545454874','sd','15847474747',NULL,1,NULL,NULL,'1','f379044a-ad82-48d7-b6e6-284a6e0eb52b.jpg'),(42,'测试头像2','测试','18855555555','啊啊啊','15555888888',NULL,1,NULL,NULL,'1','15b3113a-0dce-4496-86c5-e298082fd328.jpg');

/*Table structure for table `people_group` */

DROP TABLE IF EXISTS `people_group`;

CREATE TABLE `people_group` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `people_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `people_group` */

/*Table structure for table `pos_new` */

DROP TABLE IF EXISTS `pos_new`;

CREATE TABLE `pos_new` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `SIMH` varchar(15) NOT NULL,
  `DWJD` double DEFAULT NULL,
  `DWWD` double DEFAULT NULL,
  `CLZT` varchar(8) DEFAULT NULL,
  `CLSD` float DEFAULT NULL,
  `CLFX` float DEFAULT NULL,
  `GPST` datetime DEFAULT NULL,
  `RQSJ` datetime DEFAULT NULL,
  `org_id` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  `car_id` varchar(32) CHARACTER SET latin1 DEFAULT '-1',
  `plate_number` varchar(16) DEFAULT NULL,
  `temperature` float DEFAULT NULL,
  `electric` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=202 DEFAULT CHARSET=utf8;

/*Data for the table `pos_new` */

insert  into `pos_new`(`id`,`SIMH`,`DWJD`,`DWWD`,`CLZT`,`CLSD`,`CLFX`,`GPST`,`RQSJ`,`org_id`,`car_id`,`plate_number`,`temperature`,`electric`) values (195,'353456789012345',114.082259,22.549677,NULL,0.0001,323.87,'2008-05-24 06:18:30','2017-10-25 13:41:00',NULL,NULL,NULL,NULL,NULL),(196,'20171025',107.116726,36.137962,NULL,0.0001,323.87,'2008-05-24 06:18:30','2017-10-25 10:12:44',NULL,NULL,NULL,NULL,NULL),(197,'201508071002001',113.857677,30.570194,NULL,0.0002,0,'2017-10-25 06:11:24','2017-10-25 14:11:32',NULL,NULL,NULL,NULL,NULL),(198,'353456789054321',115.082259,22.549677,NULL,0.0001,323.87,'2008-05-24 06:18:30','2017-10-25 08:37:59',NULL,NULL,NULL,NULL,'080'),(199,'868020170825210',116.40194,39.929437,NULL,0.0059,20.83,'2017-11-27 00:45:19','2017-10-25 17:45:17',NULL,NULL,NULL,NULL,'099'),(200,'868020170825314',116.402446,39.930125,NULL,0.0093,31.05,'2017-11-27 00:49:12','2017-10-25 17:49:11',NULL,NULL,NULL,NULL,'099'),(201,'868020170825289',116.459464,39.960454,NULL,0.0006,0,'2017-11-28 05:11:22','2017-10-26 10:11:10',NULL,NULL,NULL,NULL,'000');

/*Table structure for table `rectangle` */

DROP TABLE IF EXISTS `rectangle`;

CREATE TABLE `rectangle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trap_id` int(11) DEFAULT NULL COMMENT '电子围栏Id',
  `left_top_lng` double DEFAULT NULL COMMENT '左上角经度',
  `left_top_lat` double DEFAULT NULL COMMENT '左上角纬度',
  `right_top_lng` double DEFAULT NULL COMMENT '右上角经度',
  `right_top_lat` double DEFAULT NULL COMMENT '右上角纬度',
  `left_bottom_lng` double DEFAULT NULL COMMENT '左下角经度',
  `left_botton_lat` double DEFAULT NULL COMMENT '左下角纬度',
  `right_bottom_lng` double DEFAULT NULL COMMENT '右下角经度',
  `right_bottom_lat` double DEFAULT NULL COMMENT '右下角纬度',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `rectangle` */

/*Table structure for table `route` */

DROP TABLE IF EXISTS `route`;

CREATE TABLE `route` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL COMMENT '路线名称',
  `route_desc` varchar(256) DEFAULT NULL COMMENT '描述',
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '是否线路已经巡检完成：0未完成，1已完成',
  `group_id` int(11) DEFAULT NULL COMMENT '多个人同时巡查一条路线，所以路线和人员组Id想关联',
  `distence` float DEFAULT NULL COMMENT '路线长度',
  `voltage_level` varchar(10) DEFAULT NULL COMMENT '电压等级',
  `type` varchar(2) DEFAULT NULL COMMENT '带电状态，0：不带电。1：带电',
  `tower_count` int(9) DEFAULT NULL COMMENT '杆塔数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 KEY_BLOCK_SIZE=1;

/*Data for the table `route` */

insert  into `route`(`id`,`name`,`route_desc`,`start_time`,`end_time`,`status`,`group_id`,`distence`,`voltage_level`,`type`,`tower_count`) values (25,'线路五','12313','2017-10-02 00:00:00','2017-10-13 00:00:00',NULL,NULL,12,NULL,NULL,NULL),(26,'线路二','1231','2017-10-03 00:00:00','2017-10-06 00:00:00',NULL,NULL,123,NULL,NULL,NULL),(27,'线路三','撒扥啊','2017-10-02 00:00:00','2017-10-04 00:00:00',NULL,NULL,3,NULL,NULL,NULL),(33,'线路一1','gggg','2017-10-09 00:00:00','2017-10-19 00:00:00',NULL,NULL,444,NULL,NULL,NULL),(39,'线路无','撒扥撒扥','2017-10-01 00:00:00','2017-10-10 00:00:00',NULL,NULL,12,NULL,NULL,NULL),(41,'qwer','hhhh',NULL,NULL,NULL,NULL,1231,'0','0',NULL);

/*Table structure for table `route_detail` */

DROP TABLE IF EXISTS `route_detail`;

CREATE TABLE `route_detail` (
  `id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL COMMENT '巡检路线id',
  `tower_id` int(11) DEFAULT NULL COMMENT '杆塔id',
  `seq` int(11) DEFAULT NULL COMMENT '序号，暂时不用',
  `reach_time` datetime DEFAULT NULL COMMENT '人员到达杆塔时间',
  `reach_status` int(11) DEFAULT NULL COMMENT '人员到达杆塔状态：0未到达，1到达',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `route_detail` */

/*Table structure for table `route_detail_old` */

DROP TABLE IF EXISTS `route_detail_old`;

CREATE TABLE `route_detail_old` (
  `id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `tower_id` int(11) DEFAULT NULL,
  `seq` int(11) DEFAULT NULL,
  `reach_time` datetime DEFAULT NULL,
  `reach_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `route_detail_old` */

/*Table structure for table `route_old` */

DROP TABLE IF EXISTS `route_old`;

CREATE TABLE `route_old` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `describe` varchar(256) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `route_old` */

/*Table structure for table `route_relation` */

DROP TABLE IF EXISTS `route_relation`;

CREATE TABLE `route_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tower_id` int(11) DEFAULT NULL COMMENT '塔杆ID',
  `people_id` int(11) DEFAULT NULL COMMENT '人员ID',
  `route_id` int(11) DEFAULT NULL COMMENT '线路ID',
  `reach_date` datetime DEFAULT NULL COMMENT '抵达时间',
  `eq_num` varchar(45) DEFAULT NULL COMMENT '终端编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;

/*Data for the table `route_relation` */

insert  into `route_relation`(`id`,`tower_id`,`people_id`,`route_id`,`reach_date`,`eq_num`) values (35,3,17,39,NULL,NULL),(36,3,20,39,NULL,NULL),(58,5,34,26,NULL,NULL),(59,5,41,26,NULL,'QWEQWEQ'),(60,5,40,26,NULL,NULL),(61,3,34,26,NULL,NULL),(62,3,41,26,NULL,'QWEQWEQ'),(63,3,40,26,NULL,NULL),(64,3,NULL,33,NULL,NULL),(65,5,34,27,'2017-11-27 09:37:31',NULL),(66,3,34,27,'2017-11-21 09:37:24',NULL),(81,5,42,41,NULL,NULL),(82,5,41,41,NULL,'QWEQWEQ'),(83,6,NULL,41,NULL,NULL),(84,5,NULL,41,NULL,NULL),(85,3,NULL,41,NULL,NULL);

/*Table structure for table `task` */

DROP TABLE IF EXISTS `task`;

CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '作业表主键id',
  `name` varchar(100) DEFAULT NULL COMMENT '作业名称，唯一；默认是线路+时间+序号',
  `type` varchar(2) DEFAULT NULL COMMENT '作业类型',
  `begin_time` date DEFAULT NULL COMMENT '开始时间',
  `end_time` date DEFAULT NULL COMMENT '结束时间',
  `month_bgtime` varchar(10) DEFAULT NULL COMMENT '月度作业开始时间：yyyy年mm月',
  `month_edtime` varchar(10) DEFAULT NULL COMMENT '月度作业截止时间：yyyy年mm月',
  `route_id` int(10) DEFAULT NULL COMMENT '线路id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `task` */

insert  into `task`(`id`,`name`,`type`,`begin_time`,`end_time`,`month_bgtime`,`month_edtime`,`route_id`) values (8,'线路二 2017-12-15 7','1','2017-12-12','2017-12-21',NULL,NULL,26),(9,'线路三 2017-12-15 8','1','2017-12-21','2017-12-28',NULL,NULL,27);

/*Table structure for table `task_people` */

DROP TABLE IF EXISTS `task_people`;

CREATE TABLE `task_people` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '作业-人员 主键id',
  `task_id` int(11) DEFAULT NULL COMMENT '作业id',
  `people_id` int(11) DEFAULT NULL COMMENT '人员id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `task_people` */

insert  into `task_people`(`id`,`task_id`,`people_id`) values (3,9,42),(4,9,35),(5,9,34),(10,8,35);

/*Table structure for table `task_tower` */

DROP TABLE IF EXISTS `task_tower`;

CREATE TABLE `task_tower` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '作业-杆塔 主键id',
  `task_id` int(11) DEFAULT NULL COMMENT '作业id',
  `tower_id` int(11) DEFAULT NULL COMMENT '杆塔id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `task_tower` */

insert  into `task_tower`(`id`,`task_id`,`tower_id`) values (4,9,3),(5,9,5),(14,8,3),(15,8,5);

/*Table structure for table `tower` */

DROP TABLE IF EXISTS `tower`;

CREATE TABLE `tower` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '杆塔序号',
  `name` varchar(16) NOT NULL COMMENT '杆塔名称',
  `type` smallint(6) DEFAULT NULL COMMENT '杆塔类型',
  `tower_desc` varchar(512) NOT NULL COMMENT '描述',
  `lat` double DEFAULT NULL COMMENT '经度',
  `lan` double DEFAULT NULL COMMENT '纬度',
  `high` float DEFAULT NULL COMMENT '高度',
  `voltege_class` smallint(6) DEFAULT NULL COMMENT '电压级别',
  `people_id` int(11) DEFAULT NULL COMMENT '负责人id',
  `ratius` float DEFAULT NULL COMMENT '围栏半径',
  `company_id` int(11) DEFAULT NULL COMMENT '公司id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `tower` */

insert  into `tower`(`id`,`name`,`type`,`tower_desc`,`lat`,`lan`,`high`,`voltege_class`,`people_id`,`ratius`,`company_id`) values (3,'杆塔一1',NULL,'A到B',39.888,116.999,212,3,19,12,NULL),(5,'杆塔二',NULL,'掐EQwe',39.444,116.5553,123,1,20,23,NULL),(6,'12313',NULL,'12312',12313,123123,123,1,NULL,123,NULL);

/*Table structure for table `tower_people` */

DROP TABLE IF EXISTS `tower_people`;

CREATE TABLE `tower_people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tower_id` int(11) DEFAULT NULL COMMENT '塔杆Id',
  `people_id` int(11) DEFAULT NULL COMMENT '人员Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tower_people` */

/*Table structure for table `trap` */

DROP TABLE IF EXISTS `trap`;

CREATE TABLE `trap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `lat` double DEFAULT NULL,
  `lan` double DEFAULT NULL,
  `radis` float DEFAULT NULL COMMENT '半径',
  `terminal_id` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '围栏类型，0 内；1外',
  `perperty` int(11) DEFAULT NULL COMMENT '围栏类型：0 圆圈；1 长方形；2多边形',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `trap` */

insert  into `trap`(`id`,`name`,`lat`,`lan`,`radis`,`terminal_id`,`type`,`perperty`) values (1,'围栏一y',NULL,NULL,NULL,NULL,NULL,0),(5,'○二',NULL,NULL,NULL,NULL,NULL,0),(6,'111',NULL,NULL,NULL,NULL,NULL,0),(8,'禁止区域',NULL,NULL,NULL,NULL,NULL,0);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varbinary(16) NOT NULL,
  `realname` varchar(32) DEFAULT NULL,
  `nickname` varchar(16) DEFAULT NULL,
  `password` varchar(32) NOT NULL,
  `password_decode` varchar(32) DEFAULT NULL,
  `address` varchar(64) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `mail` varchar(64) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `org_id` int(11) DEFAULT NULL,
  `login_count` int(11) DEFAULT NULL,
  `last_login_time` datetime DEFAULT NULL,
  `last_login_ip` varchar(16) DEFAULT NULL,
  `partner` varchar(32) DEFAULT NULL,
  `join_date` date DEFAULT NULL,
  `create_userid` int(11) DEFAULT NULL,
  `create_user` varchar(16) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_userid` int(11) DEFAULT NULL,
  `update_user` varchar(16) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`name`,`realname`,`nickname`,`password`,`password_decode`,`address`,`phone`,`mail`,`type`,`org_id`,`login_count`,`last_login_time`,`last_login_ip`,`partner`,`join_date`,`create_userid`,`create_user`,`create_time`,`update_userid`,`update_user`,`update_time`) values (1,'admin','admin','admin','4QrcOUm6Wau+VuBX8g+IPg==','123456',NULL,NULL,NULL,1,1,NULL,NULL,NULL,'管理员',NULL,NULL,NULL,'2016-06-16 18:47:44',NULL,NULL,'2016-06-16 18:47:44'),(2,'yt','易通',NULL,'4QrcOUm6Wau+VuBX8g+IPg==',NULL,NULL,'','yt@yt.com',0,NULL,NULL,NULL,NULL,NULL,NULL,1,'admin','2017-10-26 15:06:04',NULL,NULL,NULL);

/*Table structure for table `warn` */

DROP TABLE IF EXISTS `warn`;

CREATE TABLE `warn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eq_num` varchar(45) DEFAULT NULL COMMENT '设备终端号',
  `status` varchar(2) DEFAULT '0' COMMENT '设备状态，0：未处理，1：已处理',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `lat` double DEFAULT NULL COMMENT '纬度',
  `lng` double DEFAULT NULL COMMENT '经度',
  `type` varchar(25) DEFAULT NULL COMMENT '类型',
  `num` varchar(25) DEFAULT NULL COMMENT '编号',
  `content` varchar(500) DEFAULT NULL COMMENT '报警内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=239 DEFAULT CHARSET=utf8;

/*Data for the table `warn` */

insert  into `warn`(`id`,`eq_num`,`status`,`time`,`lat`,`lng`,`type`,`num`,`content`) values (1,'353456789012345','1','2017-11-21 14:55:02',39.36585,105.25415,'1','1001','电量不足'),(2,'353456789012345','1','2017-11-23 18:09:04',0,0,'1','0','设备电量低'),(3,'868020170825210','1','2017-11-27 11:08:26',0,0,'1','0','电量不足'),(4,'868020170825210','1','2017-11-27 11:12:05',0,0,'1','0','电量不足'),(5,'868020170825210','1','2017-11-27 11:15:16',0,0,'1','0','电量不足'),(6,'868020170825210','0','2017-11-27 11:16:25',0,0,'1','0','电量不足'),(7,'868020170825210','1','2017-11-27 11:19:07',0,0,'1','0','电量不足'),(8,'868020170825289','0','2017-11-28 07:52:08',0,0,'1','0','电量不足'),(9,'868020170825289','0','2017-11-28 07:54:08',0,0,'1','0','电量不足'),(10,'868020170825289','0','2017-11-28 07:56:08',0,0,'1','0','电量不足'),(11,'868020170825289','0','2017-11-28 07:58:08',0,0,'1','0','电量不足'),(12,'868020170825289','0','2017-11-28 08:00:08',0,0,'1','0','电量不足'),(13,'868020170825289','0','2017-11-28 08:02:10',0,0,'1','0','电量不足'),(14,'868020170825289','0','2017-11-28 08:04:08',0,0,'1','0','电量不足'),(15,'868020170825289','0','2017-11-28 08:06:08',0,0,'1','0','电量不足'),(16,'868020170825289','0','2017-11-28 08:08:08',0,0,'1','0','电量不足'),(17,'868020170825289','0','2017-11-28 08:10:09',0,0,'1','0','电量不足'),(18,'868020170825289','0','2017-11-28 08:12:09',0,0,'1','0','电量不足'),(19,'868020170825289','0','2017-11-28 08:14:09',0,0,'1','0','电量不足'),(20,'868020170825289','0','2017-11-28 08:16:09',0,0,'1','0','电量不足'),(21,'868020170825289','0','2017-11-28 08:18:09',0,0,'1','0','电量不足'),(22,'868020170825289','0','2017-11-28 08:20:09',0,0,'1','0','电量不足'),(23,'868020170825289','0','2017-11-28 08:22:09',0,0,'1','0','电量不足'),(24,'868020170825289','0','2017-11-28 08:24:09',0,0,'1','0','电量不足'),(25,'868020170825289','0','2017-11-28 08:26:09',0,0,'1','0','电量不足'),(26,'868020170825289','0','2017-11-28 08:28:09',0,0,'1','0','电量不足'),(27,'868020170825289','0','2017-11-28 08:30:09',0,0,'1','0','电量不足'),(28,'868020170825289','0','2017-11-28 08:32:09',0,0,'1','0','电量不足'),(29,'868020170825289','0','2017-11-28 08:34:09',0,0,'1','0','电量不足'),(30,'868020170825289','0','2017-11-28 08:36:09',0,0,'1','0','电量不足'),(31,'868020170825289','0','2017-11-28 08:38:09',0,0,'1','0','电量不足'),(32,'868020170825289','0','2017-11-28 08:40:09',0,0,'1','0','电量不足'),(33,'868020170825289','0','2017-11-28 08:42:09',0,0,'1','0','电量不足'),(34,'868020170825289','0','2017-11-28 08:44:09',0,0,'1','0','电量不足'),(35,'868020170825289','0','2017-11-28 08:46:09',0,0,'1','0','电量不足'),(36,'868020170825289','0','2017-11-28 08:48:09',0,0,'1','0','电量不足'),(37,'868020170825289','0','2017-11-28 08:50:09',0,0,'1','0','电量不足'),(38,'868020170825289','0','2017-11-28 08:52:09',0,0,'1','0','电量不足'),(39,'868020170825289','0','2017-11-28 08:54:09',0,0,'1','0','电量不足'),(40,'868020170825289','0','2017-11-28 08:56:10',0,0,'1','0','电量不足'),(41,'868020170825289','0','2017-11-28 08:58:10',0,0,'1','0','电量不足'),(42,'868020170825289','0','2017-11-28 09:00:10',0,0,'1','0','电量不足'),(43,'868020170825289','0','2017-11-28 09:02:10',0,0,'1','0','电量不足'),(44,'868020170825289','0','2017-11-28 09:04:10',0,0,'1','0','电量不足'),(45,'868020170825289','0','2017-11-28 09:06:10',0,0,'1','0','电量不足'),(46,'868020170825289','0','2017-11-28 09:08:10',0,0,'1','0','电量不足'),(47,'868020170825289','0','2017-11-28 09:10:10',0,0,'1','0','电量不足'),(48,'868020170825289','0','2017-11-28 09:12:10',0,0,'1','0','电量不足'),(49,'868020170825289','0','2017-11-28 09:14:10',0,0,'1','0','电量不足'),(50,'868020170825289','0','2017-11-28 09:16:10',0,0,'1','0','电量不足'),(51,'868020170825289','0','2017-11-28 09:18:10',0,0,'1','0','电量不足'),(52,'868020170825289','0','2017-11-28 09:20:10',0,0,'1','0','电量不足'),(53,'868020170825289','0','2017-11-28 09:22:10',0,0,'1','0','电量不足'),(54,'868020170825289','0','2017-11-28 09:24:10',0,0,'1','0','电量不足'),(55,'868020170825289','0','2017-11-28 09:26:10',0,0,'1','0','电量不足'),(56,'868020170825289','0','2017-11-28 09:28:10',0,0,'1','0','电量不足'),(57,'868020170825289','0','2017-11-28 09:30:10',0,0,'1','0','电量不足'),(58,'868020170825289','0','2017-11-28 09:32:10',0,0,'1','0','电量不足'),(59,'868020170825289','0','2017-11-28 09:34:10',0,0,'1','0','电量不足'),(60,'868020170825289','0','2017-11-28 09:36:10',0,0,'1','0','电量不足'),(61,'868020170825289','0','2017-11-28 09:38:11',0,0,'1','0','电量不足'),(62,'868020170825289','0','2017-11-28 09:40:11',0,0,'1','0','电量不足'),(63,'868020170825289','0','2017-11-28 09:42:11',0,0,'1','0','电量不足'),(64,'868020170825289','0','2017-11-28 09:44:11',0,0,'1','0','电量不足'),(65,'868020170825289','0','2017-11-28 09:46:11',0,0,'1','0','电量不足'),(66,'868020170825289','0','2017-11-28 09:48:11',0,0,'1','0','电量不足'),(67,'868020170825289','0','2017-11-28 09:50:11',0,0,'1','0','电量不足'),(68,'868020170825289','0','2017-11-28 09:52:11',0,0,'1','0','电量不足'),(69,'868020170825289','0','2017-11-28 09:54:11',0,0,'1','0','电量不足'),(70,'868020170825289','0','2017-11-28 09:56:11',0,0,'1','0','电量不足'),(71,'868020170825289','0','2017-11-28 09:58:11',0,0,'1','0','电量不足'),(72,'868020170825289','0','2017-11-28 10:00:11',0,0,'1','0','电量不足'),(73,'868020170825289','0','2017-11-28 10:02:11',0,0,'1','0','电量不足'),(74,'868020170825289','0','2017-11-28 10:04:11',0,0,'1','0','电量不足'),(75,'868020170825289','0','2017-11-28 10:06:11',0,0,'1','0','电量不足'),(76,'868020170825289','0','2017-11-28 10:08:11',0,0,'1','0','电量不足'),(77,'868020170825289','0','2017-11-28 10:10:11',0,0,'1','0','电量不足'),(78,'868020170825289','0','2017-11-28 10:12:11',0,0,'1','0','电量不足'),(79,'868020170825289','0','2017-11-28 10:14:11',0,0,'1','0','电量不足'),(80,'868020170825289','0','2017-11-28 10:16:11',0,0,'1','0','电量不足'),(81,'868020170825289','0','2017-11-28 10:18:11',0,0,'1','0','电量不足'),(82,'868020170825289','0','2017-11-28 10:20:12',0,0,'1','0','电量不足'),(83,'868020170825289','0','2017-11-28 10:22:12',0,0,'1','0','电量不足'),(84,'868020170825289','0','2017-11-28 10:24:12',0,0,'1','0','电量不足'),(85,'868020170825289','0','2017-11-28 10:26:12',0,0,'1','0','电量不足'),(86,'868020170825289','0','2017-11-28 10:28:12',0,0,'1','0','电量不足'),(87,'868020170825289','0','2017-11-28 10:30:12',0,0,'1','0','电量不足'),(88,'868020170825289','0','2017-11-28 10:32:12',0,0,'1','0','电量不足'),(89,'868020170825289','0','2017-11-28 10:34:12',0,0,'1','0','电量不足'),(90,'868020170825289','0','2017-11-28 10:36:12',0,0,'1','0','电量不足'),(91,'868020170825289','0','2017-11-28 10:38:12',0,0,'1','0','电量不足'),(92,'868020170825289','0','2017-11-28 10:40:14',0,0,'1','0','电量不足'),(93,'868020170825289','0','2017-11-28 10:42:12',0,0,'1','0','电量不足'),(94,'868020170825289','0','2017-11-28 10:44:12',0,0,'1','0','电量不足'),(95,'868020170825289','0','2017-11-28 10:46:12',0,0,'1','0','电量不足'),(96,'868020170825289','0','2017-11-28 10:48:12',0,0,'1','0','电量不足'),(97,'868020170825289','0','2017-11-28 10:50:12',0,0,'1','0','电量不足'),(98,'868020170825289','0','2017-11-28 10:52:12',0,0,'1','0','电量不足'),(99,'868020170825289','0','2017-11-28 10:54:12',0,0,'1','0','电量不足'),(100,'868020170825289','0','2017-11-28 10:56:12',0,0,'1','0','电量不足'),(101,'868020170825289','0','2017-11-28 10:58:12',0,0,'1','0','电量不足'),(102,'868020170825289','0','2017-11-28 11:00:12',0,0,'1','0','电量不足'),(103,'868020170825289','0','2017-11-28 11:02:12',0,0,'1','0','电量不足'),(104,'868020170825289','0','2017-11-28 11:04:13',0,0,'1','0','电量不足'),(105,'868020170825289','0','2017-11-28 11:06:14',0,0,'1','0','电量不足'),(106,'868020170825289','0','2017-11-28 11:08:13',0,0,'1','0','电量不足'),(107,'868020170825289','0','2017-11-28 11:10:13',0,0,'1','0','电量不足'),(108,'868020170825289','0','2017-11-28 11:12:14',0,0,'1','0','电量不足'),(109,'868020170825289','0','2017-11-28 11:14:13',0,0,'1','0','电量不足'),(110,'868020170825289','0','2017-11-28 11:16:13',0,0,'1','0','电量不足'),(111,'868020170825289','0','2017-11-28 11:18:13',0,0,'1','0','电量不足'),(112,'868020170825289','0','2017-11-28 11:20:13',0,0,'1','0','电量不足'),(113,'868020170825289','0','2017-11-28 11:22:13',0,0,'1','0','电量不足'),(114,'868020170825289','0','2017-11-28 11:24:13',0,0,'1','0','电量不足'),(115,'868020170825289','0','2017-11-28 11:26:15',0,0,'1','0','电量不足'),(116,'868020170825289','0','2017-11-28 11:28:13',0,0,'1','0','电量不足'),(117,'868020170825289','0','2017-11-28 11:30:13',0,0,'1','0','电量不足'),(118,'868020170825289','0','2017-11-28 11:32:13',0,0,'1','0','电量不足'),(119,'868020170825289','0','2017-11-28 11:34:13',0,0,'1','0','电量不足'),(120,'868020170825289','0','2017-11-28 11:36:13',0,0,'1','0','电量不足'),(121,'868020170825289','0','2017-11-28 11:38:13',0,0,'1','0','电量不足'),(122,'868020170825289','0','2017-11-28 11:40:13',0,0,'1','0','电量不足'),(123,'868020170825289','0','2017-11-28 11:42:13',0,0,'1','0','电量不足'),(124,'868020170825289','0','2017-11-28 11:44:13',0,0,'1','0','电量不足'),(125,'868020170825289','0','2017-11-28 11:46:13',0,0,'1','0','电量不足'),(126,'868020170825289','0','2017-11-28 11:48:14',0,0,'1','0','电量不足'),(127,'868020170825289','0','2017-11-28 11:50:14',0,0,'1','0','电量不足'),(128,'868020170825289','0','2017-11-28 11:52:15',0,0,'1','0','电量不足'),(129,'868020170825289','0','2017-11-28 11:54:14',0,0,'1','0','电量不足'),(130,'868020170825289','0','2017-11-28 11:56:14',0,0,'1','0','电量不足'),(131,'868020170825289','0','2017-11-28 11:58:14',0,0,'1','0','电量不足'),(132,'868020170825289','0','2017-11-28 12:00:14',0,0,'1','0','电量不足'),(133,'868020170825289','0','2017-11-28 12:02:14',0,0,'1','0','电量不足'),(134,'868020170825289','0','2017-11-28 12:04:14',0,0,'1','0','电量不足'),(135,'868020170825289','0','2017-11-28 12:06:14',0,0,'1','0','电量不足'),(136,'868020170825289','0','2017-11-28 12:08:14',0,0,'1','0','电量不足'),(137,'868020170825289','0','2017-11-28 12:10:14',0,0,'1','0','电量不足'),(138,'868020170825289','0','2017-11-28 12:12:14',0,0,'1','0','电量不足'),(139,'868020170825289','0','2017-11-28 12:14:14',0,0,'1','0','电量不足'),(140,'868020170825289','0','2017-11-28 12:16:14',0,0,'1','0','电量不足'),(141,'868020170825289','0','2017-11-28 12:18:14',0,0,'1','0','电量不足'),(142,'868020170825289','0','2017-11-28 12:20:14',0,0,'1','0','电量不足'),(143,'868020170825289','0','2017-11-28 12:22:14',0,0,'1','0','电量不足'),(144,'868020170825289','0','2017-11-28 12:24:14',0,0,'1','0','电量不足'),(145,'868020170825289','0','2017-11-28 12:26:14',0,0,'1','0','电量不足'),(146,'868020170825289','0','2017-11-28 12:28:14',0,0,'1','0','电量不足'),(147,'868020170825289','0','2017-11-28 12:30:14',0,0,'1','0','电量不足'),(148,'868020170825289','0','2017-11-28 12:32:15',0,0,'1','0','电量不足'),(149,'868020170825289','0','2017-11-28 12:34:15',0,0,'1','0','电量不足'),(150,'868020170825289','0','2017-11-28 12:36:15',0,0,'1','0','电量不足'),(151,'868020170825289','0','2017-11-28 12:38:15',0,0,'1','0','电量不足'),(152,'868020170825289','0','2017-11-28 12:40:15',0,0,'1','0','电量不足'),(153,'868020170825289','0','2017-11-28 12:42:15',0,0,'1','0','电量不足'),(154,'868020170825289','0','2017-11-28 12:44:15',0,0,'1','0','电量不足'),(155,'868020170825289','0','2017-11-28 12:46:15',0,0,'1','0','电量不足'),(156,'868020170825289','0','2017-11-28 12:48:15',0,0,'1','0','电量不足'),(157,'868020170825289','0','2017-11-28 12:50:15',0,0,'1','0','电量不足'),(158,'868020170825289','0','2017-11-28 12:52:15',0,0,'1','0','电量不足'),(159,'868020170825289','0','2017-11-28 12:54:15',0,0,'1','0','电量不足'),(160,'868020170825289','0','2017-11-28 12:56:15',0,0,'1','0','电量不足'),(161,'868020170825289','0','2017-11-28 12:58:15',0,0,'1','0','电量不足'),(162,'868020170825289','0','2017-11-28 13:00:15',0,0,'1','0','电量不足'),(163,'868020170825289','0','2017-11-28 13:02:15',0,0,'1','0','电量不足'),(164,'868020170825289','0','2017-11-28 13:04:15',0,0,'1','0','电量不足'),(165,'868020170825289','0','2017-11-28 13:06:15',0,0,'1','0','电量不足'),(166,'868020170825289','0','2017-11-28 13:08:15',0,0,'1','0','电量不足'),(167,'868020170825289','0','2017-11-28 13:10:16',0,0,'1','0','电量不足'),(168,'868020170825289','0','2017-11-28 13:12:16',0,0,'1','0','电量不足'),(169,'868020170825289','0','2017-11-28 13:14:16',0,0,'1','0','电量不足'),(170,'868020170825289','0','2017-11-28 13:16:16',0,0,'1','0','电量不足'),(171,'868020170825289','0','2017-11-28 13:18:16',0,0,'1','0','电量不足'),(172,'868020170825289','0','2017-11-28 13:20:16',0,0,'1','0','电量不足'),(173,'868020170825289','0','2017-11-28 13:22:16',0,0,'1','0','电量不足'),(174,'868020170825289','0','2017-11-28 13:24:16',0,0,'1','0','电量不足'),(175,'868020170825289','0','2017-11-28 13:26:16',0,0,'1','0','电量不足'),(176,'868020170825289','0','2017-11-28 13:28:16',0,0,'1','0','电量不足'),(177,'868020170825289','0','2017-11-28 13:30:16',0,0,'1','0','电量不足'),(178,'868020170825289','0','2017-11-28 13:32:16',0,0,'1','0','电量不足'),(179,'868020170825289','0','2017-11-28 13:34:16',0,0,'1','0','电量不足'),(180,'868020170825289','0','2017-11-28 13:36:16',0,0,'1','0','电量不足'),(181,'868020170825289','0','2017-11-28 13:38:16',0,0,'1','0','电量不足'),(182,'868020170825289','0','2017-11-28 13:40:16',0,0,'1','0','电量不足'),(183,'868020170825289','0','2017-11-28 13:42:16',0,0,'1','0','电量不足'),(184,'868020170825289','0','2017-11-28 13:44:16',0,0,'1','0','电量不足'),(185,'868020170825289','0','2017-11-28 13:46:16',0,0,'1','0','电量不足'),(186,'868020170825289','0','2017-11-28 13:48:16',0,0,'1','0','电量不足'),(187,'868020170825289','0','2017-11-28 13:50:16',0,0,'1','0','电量不足'),(188,'868020170825289','0','2017-11-28 13:52:16',0,0,'1','0','电量不足'),(189,'868020170825289','0','2017-11-28 13:54:16',0,0,'1','0','电量不足'),(190,'868020170825289','0','2017-11-28 13:56:17',0,0,'1','0','电量不足'),(191,'868020170825289','0','2017-11-28 13:58:17',0,0,'1','0','电量不足'),(192,'868020170825289','0','2017-11-28 14:00:17',0,0,'1','0','电量不足'),(193,'868020170825289','0','2017-11-28 14:02:17',0,0,'1','0','电量不足'),(194,'868020170825289','0','2017-11-28 14:04:17',0,0,'1','0','电量不足'),(195,'868020170825289','0','2017-11-28 14:06:17',0,0,'1','0','电量不足'),(196,'868020170825289','0','2017-11-28 14:08:17',0,0,'1','0','电量不足'),(197,'868020170825289','0','2017-11-28 14:10:17',0,0,'1','0','电量不足'),(198,'868020170825289','0','2017-11-28 14:12:17',0,0,'1','0','电量不足'),(199,'868020170825289','0','2017-11-28 14:14:17',0,0,'1','0','电量不足'),(200,'868020170825289','0','2017-11-28 14:16:17',0,0,'1','0','电量不足'),(201,'868020170825289','0','2017-11-28 14:18:17',0,0,'1','0','电量不足'),(202,'868020170825289','0','2017-11-28 14:20:17',0,0,'1','0','电量不足'),(203,'868020170825289','0','2017-11-28 14:22:17',0,0,'1','0','电量不足'),(204,'868020170825289','0','2017-11-28 14:24:17',0,0,'1','0','电量不足'),(205,'868020170825289','0','2017-11-28 14:26:17',0,0,'1','0','电量不足'),(206,'868020170825289','0','2017-11-28 14:28:17',0,0,'1','0','电量不足'),(207,'868020170825289','0','2017-11-28 14:30:17',0,0,'1','0','电量不足'),(208,'868020170825289','0','2017-11-28 14:32:17',0,0,'1','0','电量不足'),(209,'868020170825289','0','2017-11-28 14:34:17',0,0,'1','0','电量不足'),(210,'868020170825289','0','2017-11-28 14:36:18',0,0,'1','0','电量不足'),(211,'868020170825289','0','2017-11-28 14:38:19',0,0,'1','0','电量不足'),(212,'868020170825289','0','2017-11-28 14:40:18',0,0,'1','0','电量不足'),(213,'868020170825289','0','2017-11-28 14:42:18',0,0,'1','0','电量不足'),(214,'868020170825289','0','2017-11-28 14:44:18',0,0,'1','0','电量不足'),(215,'868020170825289','0','2017-11-28 14:46:18',0,0,'1','0','电量不足'),(216,'868020170825289','0','2017-11-28 14:48:18',0,0,'1','0','电量不足'),(217,'868020170825289','0','2017-11-28 14:50:18',0,0,'1','0','电量不足'),(218,'868020170825289','0','2017-11-28 14:52:18',0,0,'1','0','电量不足'),(219,'868020170825289','0','2017-11-28 14:54:18',0,0,'1','0','电量不足'),(220,'868020170825289','0','2017-11-28 14:56:18',0,0,'1','0','电量不足'),(221,'868020170825289','0','2017-11-28 14:58:18',0,0,'1','0','电量不足'),(222,'868020170825289','0','2017-11-28 15:00:18',0,0,'1','0','电量不足'),(223,'868020170825289','0','2017-11-28 15:02:18',0,0,'1','0','电量不足'),(224,'868020170825289','0','2017-11-28 15:04:18',0,0,'1','0','电量不足'),(225,'868020170825289','0','2017-11-28 15:06:18',0,0,'1','0','电量不足'),(226,'868020170825289','0','2017-11-28 15:08:18',0,0,'1','0','电量不足'),(227,'868020170825289','0','2017-11-28 15:10:18',0,0,'1','0','电量不足'),(228,'868020170825289','0','2017-11-28 15:12:18',0,0,'1','0','电量不足'),(229,'868020170825289','0','2017-11-28 15:14:18',0,0,'1','0','电量不足'),(230,'868020170825289','0','2017-11-28 15:16:18',0,0,'1','0','电量不足'),(231,'868020170825289','0','2017-11-28 15:18:18',0,0,'1','0','电量不足'),(232,'868020170825289','0','2017-11-28 15:20:18',0,0,'1','0','电量不足'),(233,'868020170825289','0','2017-11-28 15:22:19',0,0,'1','0','电量不足'),(234,'868020170825289','0','2017-11-28 15:24:21',0,0,'1','0','电量不足'),(235,'868020170825289','0','2017-11-28 15:26:19',0,0,'1','0','电量不足'),(236,'868020170825289','0','2017-11-28 15:28:19',0,0,'1','0','电量不足'),(237,'868020170825289','0','2017-11-28 15:30:19',0,0,'1','0','电量不足'),(238,'868020170825289','1','2017-11-28 15:32:19',0,0,'1','0','电量不足');

/*!50106 set global event_scheduler = 1*/;

/* Event structure for event `daysumevent` */

/*!50106 DROP EVENT IF EXISTS `daysumevent`*/;

DELIMITER $$

/*!50106 CREATE DEFINER=`root`@`localhost` EVENT `daysumevent` ON SCHEDULE EVERY 1 DAY STARTS '2017-10-20 00:01:00' ON COMPLETION PRESERVE ENABLE DO BEGIN

call daysum();

END */$$
DELIMITER ;

/* Procedure structure for procedure `daysum` */

/*!50003 DROP PROCEDURE IF EXISTS  `daysum` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `daysum`()
BEGIN

  DECLARE  simh_v varchar(255) default NULL;

  DECLARE  org_id_v varchar(255) default NULL;

  DECLARE  run_time int default 0;

  DECLARE  pad_run_time int default 0;

  DECLARE  no_more_products int default 0;

  DECLARE  nowdate date DEFAULT NULL; 

  DECLARE  all_terminal CURSOR FOR  SELECT distinct id,org_id FROM equipment where id like 'E515%' and business_status is not null and business_status !=0 ;

 DECLARE  CONTINUE HANDLER FOR NOT FOUND  SET  no_more_products = 1;

 



  select DATE_SUB(curdate(),INTERVAL 1 DAY) into nowdate;



 

 OPEN  all_terminal;

 FETCH  all_terminal INTO simh_v,org_id_v;



 DELETE from gjhf_day_sum where DATE_FORMAT(GPST,'%Y-%m-%d')=DATE_FORMAT(nowdate,'%Y-%m-%d');



 REPEAT



 SELECT  count(*)*5 INTO run_time

 FROM  gjhf

 WHERE  simh = simh_v and DATE_FORMAT(GPST,'%Y-%m-%d')=DATE_FORMAT(nowdate,'%Y-%m-%d'); 



select count(*)*5 into pad_run_time  

from deduct_fee_log  

where eq_id=simh_v and DATE_FORMAT(create_time,'%Y-%m-%d')=DATE_FORMAT(nowdate,'%Y-%m-%d');







 INSERT  INTO gjhf_day_sum(SIMH,GPST,RUN_TIME,PAD_RUN_TIME,org_id)

 VALUES  (simh_v,nowdate,run_time,pad_run_time,org_id_v);



 FETCH  all_terminal INTO simh_v,org_id_v;



 UNTIL  no_more_products = 1

 END REPEAT;





END */$$
DELIMITER ;

/* Procedure structure for procedure `daysumhistory` */

/*!50003 DROP PROCEDURE IF EXISTS  `daysumhistory` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `daysumhistory`()
BEGIN

  DECLARE  simh_v varchar(255) default NULL;

  DECLARE  org_id_v varchar(255) default NULL;

  DECLARE  run_time int default 0;

  DECLARE  pad_run_time int default 0;

  

  DECLARE b int;

  DECLARE nowdate date DEFAULT NULL;  

  DECLARE endtmp date DEFAULT NULL;  





  DECLARE  all_terminal CURSOR FOR  SELECT distinct id,org_id FROM equipment where id like 'E515%' and business_status is not null and business_status !=0;

  DECLARE  CONTINUE HANDLER FOR NOT FOUND SET  b = 1;

  

  set b=0;

 

 OPEN  all_terminal;

 



 REPEAT

     FETCH  all_terminal INTO simh_v,org_id_v;

    

    set nowdate = STR_TO_DATE('2017-09-01',"%Y-%m-%d"); 

    set endtmp = STR_TO_DATE('2017-09-23',"%Y-%m-%d");  



		WHILE nowdate<=endtmp   

		DO  

       

         DELETE from gjhf_day_sum where DATE_FORMAT(GPST,'%Y-%m-%d')=nowdate AND SIMH=simh_v;

				 SELECT  count(*)*5 INTO run_time

				 FROM  gjhf

				 WHERE  simh = simh_v and DATE_FORMAT(GPST,'%Y-%m-%d')=nowdate;



					select count(*)*5 into pad_run_time  

					from deduct_fee_log  

					where eq_id=simh_v and DATE_FORMAT(create_time,'%Y-%m-%d')=nowdate;





				 INSERT  INTO gjhf_day_sum(SIMH,GPST,RUN_TIME,PAD_RUN_TIME,org_id)

				 VALUES  (simh_v,nowdate,run_time,pad_run_time,org_id_v);

         

		set nowdate = DATE_ADD(nowdate,INTERVAL 1 DAY);  

		END WHILE;  

    

 UNTIL  b = 1

 END REPEAT;

CLOSE all_terminal;



END */$$
DELIMITER ;

/*Table structure for table `terminalandtower` */

DROP TABLE IF EXISTS `terminalandtower`;

/*!50001 DROP VIEW IF EXISTS `terminalandtower` */;
/*!50001 DROP TABLE IF EXISTS `terminalandtower` */;

/*!50001 CREATE TABLE  `terminalandtower`(
 `eq_id` int(11) ,
 `sn` varchar(45) ,
 `term_id` int(11) ,
 `people_id` int(11) ,
 `reach_date` datetime ,
 `tower_id` int(11) ,
 `name` varchar(16) ,
 `ratius` float ,
 `lat` double ,
 `lan` double ,
 `route_id` int(11) ,
 `route_relation_id` int(11) ,
 `route_name` varchar(128) ,
 `start_time` datetime ,
 `end_time` datetime ,
 `status` tinyint(4) 
)*/;

/*View structure for view terminalandtower */

/*!50001 DROP TABLE IF EXISTS `terminalandtower` */;
/*!50001 DROP VIEW IF EXISTS `terminalandtower` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`192.168.1.152` SQL SECURITY DEFINER VIEW `terminalandtower` AS select `equipment`.`id` AS `eq_id`,`equipment`.`sn` AS `sn`,`equipment_assign`.`terminal_id` AS `term_id`,`route_relation`.`people_id` AS `people_id`,`route_relation`.`reach_date` AS `reach_date`,`tower`.`id` AS `tower_id`,`tower`.`name` AS `name`,`tower`.`ratius` AS `ratius`,`tower`.`lat` AS `lat`,`tower`.`lan` AS `lan`,`route_relation`.`route_id` AS `route_id`,`route_relation`.`id` AS `route_relation_id`,`route`.`name` AS `route_name`,`route`.`start_time` AS `start_time`,`route`.`end_time` AS `end_time`,`route`.`status` AS `status` from ((((`route_relation` join `equipment`) join `equipment_assign`) join `tower`) join `route`) where ((`equipment_assign`.`terminal_id` = `equipment`.`id`) and (`route_relation`.`people_id` = `equipment_assign`.`people_id`) and (`route_relation`.`tower_id` = `tower`.`id`) and (`equipment_assign`.`terminal_id` = `equipment`.`id`) and (`equipment_assign`.`people_id` = `route_relation`.`people_id`) and (`route`.`id` = `route_relation`.`route_id`)) order by `route_relation`.`route_id`,`tower`.`id` */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
