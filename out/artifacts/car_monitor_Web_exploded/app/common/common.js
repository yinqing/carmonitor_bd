var basePath = location.protocol+"//"+location.hostname+":"+location.port;
var finalwidth = 260 //最后的宽度
var wd = 260; //初始宽度
var dragable = false;//默认不可拖拽
var oldX = '';//记录第一次的鼠标位置
var startDrag = function(event){
    dragable = true;
    var e=event?event:window.event;
    oldX = e.pageX; //记录第一次的鼠标位置
};
var unDrop = function(){
    dragable = false;
    window.event? window.event.cancelBubble = true : e.stopPropagation();
};
var endDrop = function(){
    if(dragable){
        finalwidth = wd;
        dragable = false;            
    };
};
document.onmouseup=function(){
    endDrop();
};
document.onmousemove=function(event){
    if(dragable){
        var e=event?event:window.event;
        box = document.getElementById('moveBarBox');
        wd =  e.pageX - oldX  + parseInt(finalwidth);
        //鼠标的位移 + div的最后宽度 = div的新宽度
        //向左拉  wd =  oldX - e.pageX  + parseInt(finalwidth); 
        //向右拉  wd =  e.pageX - oldX  + parseInt(finalwidth);
        if(dragable){
            if(wd<245 || wd==245){//div最小宽度
                box.style.width = '245px';
                wd = '245px';
                 return;
            }
            if(wd>400 || wd==400){//div最大宽度
                box.style.width = '400px';
                wd = '400px';return;
            }
            box.style.width = wd + 'px'; 
        };
    };
};
//弹窗事件
function layer_show(w,h,title,url){
	if (w == null || w == '') {
		w=800;
	};
	if (h == null || h == '') {
		h=($(window).height() - 50);
	};
	if (title == null || title == '') {
		title=false;
	};
	if (url == null || url == '') {
		url="404.html";
	};
	layer.open({
    	type: 2,
    	shadeClose: false,
    	title: title,
		maxmin:false,
		//shadeClose: true,
//    	closeBtn: [0, true],
    	shade: [0.8, '#000'],
    	border: [0],
    	offset: ['20px',''],
    	area: [w+'px', h +'px'],
    	content:url
	});
}

//阻止冒泡事件
function stopPropagation(e){
	e = window.event||e;
	if(document.all){
		e.cancelBubble = true;
	}else{
		e.stopPropagation();
	}
}
//iframe中关闭当前窗口，并给出弹框提示
function refreshTable(msg){
	var index = parent.layer.getFrameIndex(window.name);  
	parent.layer.close(index); 
	if(msg){
	    parent.layer.alert(msg);
	}
	parent.$("table").bootstrapTable("refresh");
}
//关闭下拉框
$("body").click(function(e){
    if($(e.target).closest('.autoSelect-input').length){          //判断点击的地方是否是在弹出框里面
            //判断点击对象是否在#box内
    }else{
            $(".autoSelect-ul").hide();
    } 
})
//格式化日期
function dateFormatterFn(time,type){
	var t = new Date(time),
		year = t.getFullYear(),
		month = t.getMonth()+1,
		day = t.getDate(),
		h = t.getHours(),
		m = t.getMinutes(),
		s = t.getSeconds();
		month = month<10?"0"+month:month;
		day = day<10?"0"+day:day;
		h = h<10?"0"+h:h;
		m = m<10?"0"+m:m;
		s = s<10?"0"+s:s;
	var newDate = "";
	if(type && type=="full"){
		newDate = year+"-"+month+"-"+day+" "+h+":"+m+":"+s;
	}else{
		newDate = year+"-"+month+"-"+day;
	}
	return newDate;
}
function initFormDatas(form,key,value){
	var formField = $(form).find("[name="+key+"]");
	if(formField.length===0){
		return true;
	}
	var fieldTagName = formField[0].tagName.toLowerCase();
	if(fieldTagName == "input"){
		if(formField.attr("type") == "radio"){
			$("input:radio[name='"+key+"'][value='"+value+"']").attr("checked","checked");
		}else if(formField.attr("type") == "checkbox"){
			$("input:checkbox[name='"+key+"'][value='"+value+"']").attr("checked","checked");
		}else {
			formField.val(value);
		}
	} else if(fieldTagName == "select"){
		$("select[name="+key+"]").find("option[value="+value+"]").attr("selected",true);
	} else if(fieldTagName == "textarea"){
		formField.val(value);
	} else {
		formField.val(value);
	}
}

//Object trim化
function objTrim(obj){
	if(!isEmptyObject(obj)){
		var i;
		for(i in obj){
			obj[i] = $.trim(obj[i])
		}
		
		return obj;
	}
}

function isEmptyObject(e) {
	var t;
	for (t in e)
		return !1;
	return !0
}

//cookie
function setCookie(c_name,value,expiredate){
	var exdate = new Date();//获取当前日期
	exdate.setDate(exdate.getDate()+expiredate);//获取到期日期
	document.cookie = c_name+"="+escape(value)+
			((expiredate==null) ? "" : ";expires="+exdate.toGMTString())
}
function getCookie(c_name){
	if (document.cookie.length>0){
		var c_start=document.cookie.indexOf(c_name + "=")
		if (c_start!=-1)
		{
			c_start=c_start + c_name.length+1
			var c_end=document.cookie.indexOf(";",c_start)
			if (c_end==-1) {c_end=document.cookie.length}
			return unescape(document.cookie.substring(c_start,c_end))
		}
	}
	return ""
}

//检查登录状态
function checkLogin(resp){
	if(!resp.success && resp.message=="unlogin"){
		layer.alert("您已经长时间未操作或已经退出登录，请重新登录！",function(){
			top.location.href = basePath +"/app/login/login.jsp";
		});
		return;
	}
}
/**
 * 上传图片预览
 * @params  imgFile  img 图片的信息，imgPreview: 要显示图片的div 的ID
 *
 * **/
function PreviewImage(imgFile,imgPreview)
{
	//判断是否为空
	if(imgFile.value==""){
		return;
	}
	var filextension=imgFile.value.substring(imgFile.value.lastIndexOf("."),imgFile.value.length);
	filextension=filextension.toLowerCase();
	if ((filextension!='.jpg')&&(filextension!='.gif')&&(filextension!='.jpeg')&&(filextension!='.png')&&(filextension!='.bmp'))
	{
		layer.alert("对不起，系统仅支持标准格式的照片，请您调整格式后重新上传，谢谢 !");
		imgFile.focus();
	}
	else
	{
		var path;

		if(document.all)//IE
		{
			imgFile.select();
			path = document.selection.createRange().text;

			document.getElementById(imgPreview).innerHTML="";
			document.getElementById(imgPreview).style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled='true',sizingMethod='scale',src=\"" + path + "\")";//使用滤镜效果
		}
		else//FF
		{
			path = window.URL.createObjectURL(imgFile.files[0]);
//                path = imgFile.files[0].getAsDataURL();//getAsDataURL()这个方法现在主流浏览器已经不能用了
			document.getElementById(imgPreview).innerHTML = "<img id='img1' width='120px' height='100px' src='"+path+"'/>";
			// document.getElementById("img1").src = path;
		}
	}
}