//获取导航菜单
var loadMenuUrl = "/app/common/menu.json";
if(L_TYPE!=1){
	//普通管理员
	loadMenuUrl = "/app/common/menu2.json";
}
$.getJSON(loadMenuUrl,function(resp){
	var data = resp.data;
	var navArr = [];
	for(var i=0;i<data.length;i++){
		var	name = data[i].name,
			index = data[i].index,
			icon = data[i].icon,
			children = data[i].children;
		/**加载顶部导航**/
		var navLi = $("<li data-index="+index+"></li>");
		$(navLi).append("<div class='nav-menu-icon' style='background:url(/app/img/menu/"+icon+")'><span>"+name+"</span></div>");
		$(".home-nav").append(navLi);
		/**加载左侧菜单**/
		//加载一级菜单
		var menuLi = $("<li class='first-level' data-index="+index+"></li>");
		var subMenu = "<span class='menu-title'>"
						+"<a href='javascript:;'>"+name+"</a>"
						+"<i class='menu-pr-icon'><img src='/app/img/menu/plus.png'/></i>"
						+"</span>";
		$(menuLi).append(subMenu);
		//加载二级菜单
		var _subUl = $("<ul class='sub-menu'></ul>");
		for(var j=0;j<children.length;j++){
			var _subItems = "<li class='menu-subItem' data-url="+children[j].url+">"
							+"<i><img src='/app/img/menu/menu-no.png' class='menu-icon'/></i>"
							+"<span>"+children[j].name+"</span>"
							+"</li>";
			$(_subUl).append(_subItems);
		}
		$(menuLi).append(_subUl);
		$(".home-menu-item").append(menuLi)
	}
},function(e){
	alert("加载失败")
})

//一级标题点击事件
$(".home-menu-item").on("click",".menu-title",function(){
	//判断是否有二级标题
	var hasChild = $(this).next("ul").length;
	if(hasChild>0){
		var that = this;
		$(this).siblings().slideToggle(100,function(){
			var isHidden = $(that).siblings().css("display");
			if(isHidden == "none"){
				$(that).find("i img").prop("src","/app/img/menu/plus.png");
			}else{
				$(that).find("i img").prop("src","/app/img/menu/reduce.png");
			}
		});
		$(this).parent().siblings().find("ul").slideUp(100);
		$(this).parent().siblings().find(".menu-pr-icon img").prop("src","/app/img/menu/plus.png");
	}
})
//二级标题点击事件
$(".home-menu-item").on("click",".menu-subItem",function(){
	var that = this;
	var url = $(that).data("url");
	$(".menu-subItem").removeClass("sub-checked");
	$(".menu-subItem").find("i img").prop("src","/app/img/menu/menu-no.png");
	$(".menu-title").removeClass("title-checked");
	$(that).addClass("sub-checked");
	$(that).parent().siblings(".menu-title").addClass("title-checked");
	$(that).find("i img").prop("src","/app/img/menu/menu-yes.png");
	//点击二级菜单，其对应的顶部导航栏样式随之改变
	var index = $(that).parents(".first-level").data("index");
	$(".home-nav").find("div").removeClass("nav-menu-checked");
	var topNav = $(".home-nav").find("li");
	for(var i=0;i<topNav.length;i++){
		var _topIndex = $(topNav[i]).data("index");
		if(index == _topIndex) {
			$(topNav[i]).find("div").addClass("nav-menu-checked");
		}
	}
	if(url){
		$.ajax({
			type:"GET",
			url:"/app/pages/"+url+".html",
			success:function(resp){
				$(".show-grid").html(resp);
			},
			error:function(e){
				alert("数据加载有误。。。");
			}
		});
	}
})
//导航栏点击事件
$(".home-nav").on("click","li",function(){
	$(".home-nav").find(".nav-menu-icon").removeClass("nav-menu-checked");
	$(this).find(".nav-menu-icon").addClass("nav-menu-checked");
	var index = $(this).data("index");
	if(index == 0){
		$(".home-location").show();
		$(".show-waring-icon").show();
		$(".home-map").show();

		$(".home-other").hide();
		$(".home-menu-item").find(".sub-menu").slideUp(100);
	}else{
		$(".map-tips").height(0).removeClass("my-fadeInUp");
		$(".home-location").hide();
		$(".home-map").hide();
		$(".home-other").show();
		//隐藏警示框大红点
		//$(".show-waring-icon").hide();
		//遍历左侧菜单的data-index
		var li = $(".home-menu-item").find(".first-level");
		for(var i=0;i<li.length;i++){
			var _index = $(li[i]).data("index");
			if(index===_index){
				$(li[i]).siblings().find(".sub-menu").slideUp(100,function(){
					$(this).siblings().find("i img").prop("src","/app/img/menu/plus.png");
				});
				$(li[i]).find(".sub-menu").slideDown(100,function(){
					$(this).siblings().find("i img").prop("src","/app/img/menu/reduce.png");
				});
				$(li[i]).find(".menu-subItem:first").trigger("click");
			}
		}
	}
})