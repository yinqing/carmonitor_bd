;!function(){
	layer = layui.layer
	,laydate = layui.laydate
	,form = layui.form;
}();
/**
 * 实时位置点击事件
 * **/
$(function(){
	initLocationGrid();
	loadRouteInfo();
	$(".location-item").click(function(){
		//改变按钮样式
		$(".location-params").find(".location-item").removeClass("location-checked");
		$(this).addClass("location-checked");
		//获取点击的按钮序号，1为实时位置，2为历史查询
		var index = $(this).data("index");
		getHeight();
		switch (index){
			case 1:
				$(".history-box").hide();
				$(".tower-box").hide();
				$(".location-box").show();
				initLocationGrid();
				break;
			case 2:
				$(".location-box").hide();
				$(".tower-box").hide();
				$(".history-box").show();
				initHistoryLine();
				break;
			default:
				$(".location-box").hide();
				$(".history-box").hide();
				$(".tower-box").show();
				initTowerInfo();
		}
		
	});
})
//加载实时位置信息
function initLocationGrid(){
	//加载列表
	$("#main-location-table").bootstrapTable({
		height: getHeight(),
		method:"post",
		url:basePath+"/selectCurrPosition.do",
		dataType:"json",
		pagination: true, //分页
		sidePagination: "client",//服务器端分页server  如果是客户端分页 是 client
		pageSize: 10,
		pageList: [10, 25, 50, 100],
		responseHandler: function(resp) {
			checkLogin(resp);
			var data =[]
			if(resp.success){
				data = resp.data;
			}
			return data;
		},
		onLoadSuccess: function(data) { //加载成功时执行
			_contextMenu_(data);
		},
		onLoadError: function() { //加载失败时执行
			layer.msg("系统繁忙，请稍后再试", { time: 1500, icon: 2 });
		},
		queryParams: function(params) { //查询的参数
			//return {
			//	pageNumber: params.offset + 1,
			//	pageSize: params.limit,
			//};
		},
		columns: [
			{
				field:"id",
				visible:false
			},
			{
				field: 'peopleName',
				title: '姓名',
				sortable:true,
				align: 'center'
			}, {
				field: 'mobile',
				title: '手机号',
				align: 'center'
			}, {
				field: 'routeName',
				title: '线路名称',
				align: 'center'
			}
		]
	})
}
function getHeight(){
	return $(window).height()-307;
}
$(window).resize(function () {
    $('.main-self-table').bootstrapTable('resetView', {
        height: getHeight()
    });
});
//加载历史信息
function initHistoryLine(){
	//加载列表
	$("#main-history-table").bootstrapTable({
		height: getHeight(),
		pagination: true, //分页
		sidePagination: "client",//服务器端分页server  如果是客户端分页 是 client
		pageSize: 3,
		pageList: [10, 25, 50, 100],
		responseHandler: function(resp) {
			var data = resp.result;
			return data;
		},
		onLoadSuccess: function(data) { //加载成功时执行
			_contextMenu_(data);
	//							alert("加载成功" + data);
	//							$("th.bs-checkbox .th-inner").append("<div class='my-grid-checkbox'></div>");
		},
		onLoadError: function() { //加载失败时执行
	//							layer.msg("加载数据失败", { time: 1500, icon: 2 });
		},
		queryParams: function(params) { //查询的参数
			//var subcompany = $('#subcompany option:selected').val();
			var name = "";
			return {
				pageNumber: params.offset + 1,
				pageSize: params.limit,
				//companyId:subcompany,
				name: name
			};
		},
		columns: [
			{
				field:"id",
				visible:false
			},
			{
				field: 'name',
				title: '员工姓名',
				sortable:true,
				align: 'center'
			}, {
				field: 'phone',
				title: '员工手机号',
				align: 'center'
			}
		],
		url: "/app/pages/data.json"
	})
}
//加载杆塔信息
function initTowerInfo(){
	//加载列表
	$("#main-tower-table").bootstrapTable({
		height: getHeight(),
		pagination: true, //分页
		sidePagination: "client",//服务器端分页server  如果是客户端分页 是 client
		pageSize: 3,
		pageList: [10, 25, 50, 100],
		responseHandler: function(resp) {
			var data = resp.result;
			return data;
		},
		onLoadSuccess: function(data) { //加载成功时执行
			_contextMenu_(data);
	//							alert("加载成功" + data);
	//							$("th.bs-checkbox .th-inner").append("<div class='my-grid-checkbox'></div>");
		},
		onLoadError: function() { //加载失败时执行
	//							layer.msg("加载数据失败", { time: 1500, icon: 2 });
		},
		queryParams: function(params) { //查询的参数
			//var subcompany = $('#subcompany option:selected').val();
			var name = "";
			return {
				pageNumber: params.offset + 1,
				pageSize: params.limit,
				//companyId:subcompany,
				name: name
			};
		},
		columns: [
			{
				field:"id",
				visible:false
			},
			{
				field: 'name',
				title: '责任人',
				sortable:true,
				align: 'center'
			}, {
				field: 'phone',
				title: '塔杆编号',
				align: 'center'
			}, {
				field: 'id',
				title: 'ID',
				visible:false,
				align: 'center'
			}
		],
		url: "/app/pages/data.json"
	})
}
//加载线路信息
function loadRouteInfo(){
	$.ajax({
		type:"post",
		url:basePath +"/selectAllRoute.do",
		dataType:"json",
		success:function(resp){
			checkLogin(resp);
			if(resp.success){
				//获取所有线路信息
				var data = resp.data;
				if(data.length>0){
					for(var i=0;i<data.length;i++){
						$(".curr-location-route").append("<option value='"+data[i].id+"'>"+data[i].name+"</option>");
					}
				}
			}
		},
		error:function(e){
			layer.msg("系统繁忙，请稍后再试！");
			return false;
		}
	});
}
//右键菜单
function _contextMenu_(data){
	var menu = new BootstrapMenu('.main-self-table tr', {
		fetchElementData: function(rowElem) {
			var rowId = rowElem.data('index');
	 		return data[rowId];
		},
		actions: [{
		    name: '查看位置',
		    onClick: function(row) {
		    	if(selfMap.markerArr.length>0){
		    		var la = selfMap.markerArr[0].getLabel().content;
		    		selfMap.map.removeOverlay(selfMap.markerArr[0]);
		    		selfMap.markerArr.length = 0;
		    	}
			     var lgt = row.dWJD,lat = row.dWWD;
					var obj = {
						name:row.peopleName,
						id:row.id,
						peopleId:row.peopleId,
						routeName:row.routeName
					};
					var pt = new BMap.Point(lgt, lat);
					var myIcon = new BMap.Icon("/app/img/map/photo.png", new BMap.Size(48,48),{
		//				anchor:new BMap.Size(32,64)
					});
					var marker = new BMap.Marker(pt,{icon:myIcon});  // 创建标注
					selfMap.map.addOverlay(marker);
					selfMap.map.setCenter(pt);
					var label = new BMap.Label(row.id);
					label.setStyle({
						display:"none"
					});
					marker.setLabel(label);
					//将员工的marker 添加到数组
					selfMap.markerArr.push(marker);
					//给标注添加click事件
					selfMap.addMarkerClick(obj,marker,pt,"person");
		    }
		}
		//	,
		//	{
		//	name:"查看线路",
		//	onClick: function(row){
		//		var polyline = new BMap.Polyline([
		//			new BMap.Point(116.417,39.909),//起始点的经纬度
		//			new BMap.Point(116.017,38.909),//起始点的经纬度
		//			new BMap.Point(116.017,37.909),//起始点的经纬度
		//			new BMap.Point(113.917,36.909),//起始点的经纬度
		//			new BMap.Point(113.817,35.909),//起始点的经纬度
		//			new BMap.Point(111.717,34.909),//起始点的经纬度
		//			new BMap.Point(111.617,33.909),//起始点的经纬度
		//			new BMap.Point(109.417,32.909),//起始点的经纬度
		//			new BMap.Point(108.917,31.909),//起始点的经纬度
		//			new BMap.Point(107.717,30.909),//起始点的经纬度
		//			new BMap.Point(105.417,27.909)//终止点的经纬度
		//		], {strokeColor:"red",//设置颜色
		//			strokeWeight:3, //宽度
		//			strokeOpacity:1});//透明度
        //
		//	 selfMap.map.addOverlay(polyline);
		//	}
		//}
			,{
			name:"查看轨迹",
			onClick: function(row){
				clearTimeout(markerMove);
				selfMap.map.removeOverlay(polyline);
				selfMap.map.removeOverlay(polyMarker);
				var pts1 = [
					new BMap.Point(116.417,39.909),//起始点的经纬度
					new BMap.Point(116.017,38.909),//起始点的经纬度
					new BMap.Point(116.017,37.909),//起始点的经纬度
					new BMap.Point(113.917,36.909),//起始点的经纬度
					new BMap.Point(113.817,35.909),//起始点的经纬度
					new BMap.Point(111.717,34.909),//起始点的经纬度
					new BMap.Point(111.617,33.909),//起始点的经纬度
					new BMap.Point(109.417,32.909),//起始点的经纬度
					new BMap.Point(108.917,31.909),//起始点的经纬度
					new BMap.Point(107.717,30.909),//起始点的经纬度
					new BMap.Point(105.417,27.909)//终止点的经纬度
				];
				var pts2 = [
					new BMap.Point(116.417,39.909),//起始点的经纬度
					new BMap.Point(113.017,37.909),//起始点的经纬度
					new BMap.Point(110.017,36.909),//起始点的经纬度
					new BMap.Point(108.917,35.909),//起始点的经纬度
					new BMap.Point(106.817,34.909),//起始点的经纬度
					new BMap.Point(104.717,33.909),//起始点的经纬度
					new BMap.Point(103.617,32.909),//起始点的经纬度
					new BMap.Point(101.417,31.909),//起始点的经纬度
					new BMap.Point(99.917,30.909),//起始点的经纬度
					new BMap.Point(99.717,29.909),//起始点的经纬度
					new BMap.Point(97.417,35.909)//终止点的经纬度
				];
				var pts3 = [
					new BMap.Point(116.417,39.909),//起始点的经纬度
					new BMap.Point(113.017,37.909),//起始点的经纬度
					new BMap.Point(110.017,36.909),//起始点的经纬度
					new BMap.Point(108.917,35.909),//起始点的经纬度
					new BMap.Point(106.817,34.909),//起始点的经纬度
					new BMap.Point(104.717,33.909),//起始点的经纬度
					new BMap.Point(105.617,32.909),//起始点的经纬度
					new BMap.Point(107.417,31.909),//起始点的经纬度
					new BMap.Point(108.917,30.909),//起始点的经纬度
					new BMap.Point(110.717,27.909),//起始点的经纬度
					new BMap.Point(111.417,25.909)//终止点的经纬度
				];
				var pts  = row.id=="195"?pts1:
						row.id =="196"?pts2:pts3
				polyline = new BMap.Polyline(pts,
						{
							strokeColor:"red",//设置颜色
							strokeWeight:3, //宽度
							strokeOpacity:1
						});//透明度

				selfMap.map.addOverlay(polyline);
				var myIcon = new BMap.Icon("/app/img/map/photo.png", new BMap.Size(48,48),{
					anchor:new BMap.Size(12,48)
				});
				polyMarker = new BMap.Marker(pts[0],{icon:myIcon});
				selfMap.map.addOverlay(polyMarker);

				selfMap.addMarkerClick(row,polyMarker,pts[0],"person");

				i=0;
				var paths = pts.length;    //获得有几个点
				function resetMkPoint(i){
					polyMarker.setPosition(pts[i]);
					if(i < paths){
						markerMove = setTimeout(function(){
							i++;
							resetMkPoint(i);
						},1000);
					}
				}
				resetMkPoint(0);
			}
		}
		]
	});
}

/**
 * 加载警告信息
 **/
loadWarningList();
function loadWarningList(){
	$(".warnint-table").bootstrapTable({
		height: 170,
		responseHandler: function(resp) {
			var data = resp.result;
			return data;
		},
		onLoadSuccess: function(data) { //加载成功时执行
		},
		onLoadError: function() { //加载失败时执行
		},
		queryParams: function(params) { //查询的参数
			var name = "";
			return {
				pageNumber: params.offset + 1,
				pageSize: params.limit,
				//companyId:subcompany,
				name: name
			};
		},
		columns: [
			{
				field: 'name',
				title: '设备名',
				sortable:true,
				align: 'center'
			}, {
				field: 'id',
				title: 'IMEI号/ID号',
				align: 'center'
			}, {
				field: 'waringType',
				title: '报警类型',
				align: 'center'
			}, {
				field: 'waringTime',
				title: '报警时间',
				align: 'center'
			}, {
				field: 'sysTime',
				title: '定位时间',
				align: 'center'
			}, {
				field: 'type',
				title: '型号',
				align: 'center'
			},{
				title: '操作', 
				align: 'center',
				formatter:function(value, row, index){
					return "<a class='handle-case'>处理</a>"
				},
				events:{
					"click .handle-case" : function(e, value, row, index){
						layer.msg("您点击了处理事件");
					}
				}
			}
		],
		data:[
			{
				id:"201515151515151",
				name:"DHCG737733",
				waringType:"离线报警",
				waringTime:"2017-07-07 16:15:15",
				sysTime:"2017-05-01 14:22:22",
				type:"DTrack"
			}
		]
//		url: "./pages/data.json"
	});
}

//监听是否开启报警声音
$(".open-warning").change(function(){
	var isChecked = $(this).prop("checked");//true 选中了，false 未选中
	if(isChecked){
		layer.msg("开启了报警声音");
	}else{
		layer.msg("关闭了报警声音");
	}
});
//一键处理
$(".fast-handle").click(function(){
	layer.alert("您点击了一键处理事件");
});
//关闭警示框
$(".close-map-tips").click(function(){
	$(".map-tips").animate({
		height:0
	},100,"linear",function(){
		$(".map-tips").removeClass("my-fadeInUp");
		$(".show-waring-icon").fadeIn(200);
	});
});
//显示报警警示框
$(".show-waring-icon").click(function(){
	$(".map-tips").addClass("my-fadeInUp").height("200px");
	$(this).fadeOut(200);
});
//实时位置重置
$(".location-reset").click(function(){
	$(".search-location").find("input").val("");
	$(".search-location").find("select").val("");
});
//实时位置查询
$(".location-search").click(function(){
	var name = $(".curr-search-name").val().trim(),
		mobile = $(".curr-search-mobile").val().trim(),
		routeId = $(".curr-location-route").val();
	$.ajax({
		type:"post",
		url:basePath+"/selectCurrPosition.do",
		data:{
			peopleName:name,
			mobile:mobile,
			routeId:routeId
		},
		dataType:"json",
		success:function(resp){
			checkLogin(resp);
			if(resp){
				var data = resp.data;
				$("#main-location-table").bootstrapTable("load",data);
			}else{
				layer.msg("系统繁忙，请稍后再试！");
				return false;
			}
		},error:function(e){
			layer.msg("系统繁忙，请稍后再试！");
			return false;
		}
	});
});

//退出系统
$(".login-out-cls").click(function(){
	layer.confirm("是否确认退出系统？",
		{btn:["是","否"]},
		function(){
			$.ajax({
				type:"post",
				url:basePath+"/userLogout.do",
				success:function(resp){
					if(resp){
						top.location.href = basePath +"/app/login/login.jsp";
					}
				},
				error:function(e){
					layer.msg("系统繁忙，请稍后再试！");
					return false;
				}
			});
		}
	);

});