<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String baseUrl = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>电力作业安全预警指挥系统</title>
	<!--<link rel="Shortcut Icon" href="./jsp/eto/images/index/favicon.ico"/>
    <link rel="bookmark" href="./jsp/eto/images/index/favicon.ico"/>-->
	<!--common style-->
	<link rel="stylesheet" href="<%=baseUrl%>app/css/login.css">
	<script>
		var baseUrl = "<%=baseUrl%>";
	</script>
</head>
<body>
<div class="login">
	<p class="login-info">电力作业安全预警指挥系统</p>
	<div class="loginform">
		<p class="login-info">
			<input type="text" placeholder="请输入用户名" class="login-text username">
		</p>
		<p class="login-info">
			<input type="password" placeholder="请输入密码" class="login-text password">
		</p>
		<span class="rememberpwd"><i class=""></i>记住密码</span>
		<span class="forgetpwd"><a href="javascript:void(0)">忘记密码？</a></span>
		<button class="login-btn">登录</button>
	</div>
</div>
<p class="error-tip"></p>
</body>
</html>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<%--<script src="<%=basePath%>/app/js/login.js"></script>--%>
<script src="<%=baseUrl%>/app/common/common.js"></script>
<script>
	$(function(){
		var co = getCookie("loginInfo");
		if(co){
			var loginInfo = co.split("@");
			if(loginInfo[2]==="yes"){//记住密码了
				var u_name = loginInfo[0],
					u_pwd = loginInfo[1];
				$(".username").val(u_name);
				$(".password").val(u_pwd);
				$(".rememberpwd").find("i").addClass("isremember");
			}
		}
		//点击记住密码
		$(".rememberpwd").click(function(){
			$(this).find("i").toggleClass("isremember")
		});
		//登录按钮点击事件
		$(".login-btn").click(function(){
			var username = $(".username").val().trim(),
					pwd = $(".password").val().trim();

			if(!username){
				showErrorTip("用户名不能为空！");
				$(".username").addClass("login-text-error");
				return;
			}
			if(!pwd){
				showErrorTip("密码不能为空！");
				$(".password").addClass("login-text-error");
				return;
			}
			$(".login-btn").text("登录中。。。");
				//调用登录接口
				$.ajax({
					type:"POST",
					url:baseUrl+"login.do",
					data: {userName:username,password:pwd},
					dataType: "json",
					success:function(resp){
						if(resp && resp.success!=="false"){
							//检查是否记住密码
							var isRememberPwd = $(".rememberpwd").find(".isremember");
							var _cookie = username+"@"+pwd;
							_cookie += isRememberPwd.length>0?"@yes":"@no";
							setCookie("loginInfo",_cookie,1);
							window.location.href = baseUrl+"/index.do";
//							window.location.href = basePath+"/app/main.html";
						}else if(resp && resp.success==="false"){
							showErrorTip(resp.message);
						}else{
							showErrorTip("用户名或密码不正确！");
						}
					},
					error:function(){
						showErrorTip("系统繁忙，请稍后再试！");
					}
				})

		});
		//绑定Enter按键
		$("body").on("keyup",function(e){
			e = e || window.event;
			//Enter键
			if(e.keyCode===13){
				$(".login-btn").trigger("click");
			}
		})
		function showErrorTip(msg){
			$(".error-tip").text(msg);
			$(".error-tip").show();
			setTimeout(function(){
				$(".error-tip").fadeOut().text("");
			},2500);
			$(".login-btn").text("登录");
		}
		$(".login-text").focus(function(){
			$(this).removeClass("login-text-error");
		});
	})
</script>