<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.alibaba.fastjson.JSON" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	Object sess = request.getSession().getAttribute("loginUser");
	String loginSession = JSON.toJSONString(sess);
%>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<link rel="stylesheet" type="text/css" href="/app/lib/grid/bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="/app/lib/grid/bootstrap-table.min.css"/>
		<link rel="stylesheet" type="text/css" href="/app/lib/font/css/font-awesome.css" />
		<link rel="stylesheet" type="text/css" href="/app/lib/layui/css/layui.css"/>
		<link rel="stylesheet" type="text/css" href="/app/lib/grid/jquery.contextMenu.css"/>
		<link rel="stylesheet" href="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.css" />
		<link rel="stylesheet" type="text/css" href="/app/lib/css/animate.min.css"/>
		<title>电力作业安全预警指挥系统</title>
		<style type="text/css">
			html,body,div,p,a,ul,li,span,i,dl,dd,dt {
				margin: 0;
				padding: 0;
			}
			body {
				width: 100%;
				height: 100%;	
				background-color: #f2fafd;
				font-family:"微软雅黑";
			}
			ul li  {
				list-style: none;
			}
			.nav {
				width: 100%;
				height: 74px;
				background: url(/app/img/logo/alpha-background-head.png) #23b7fb right center no-repeat;
			}
			.nav:after {
				clear: both;
			}
			.main-logo {
				float: left;
				width: 269px;
				height: 100%;
				background: url(img/logo/logo.png) no-repeat;
				background-size: 100% 100%;
			}
			.home-nav {
				float: left;
			}
			.home-nav li {
				float: left;
			}
			.home-nav:after {
				clear: both;
			}
			.nav-menu-icon {
				position: relative;
			    width: 85px;
			    height: 74px;
			    color: #FFFFFF;
			    cursor: pointer;
			}
			.table {
				background-color: #FFFFFF;
			}
			.location-nav {
				background: url(/app/img/menu/location.png) no-repeat;
			}
			.nav-menu-icon:hover,.nav-menu-checked {
				background-color: #4fc5fb !important;
			}
			.nav-menu-icon span {
			    display: inline-block;
			    position: absolute;
			    bottom: 5px;
			    width: 100%;
			    text-align: center;
			}
			.home-body {
				display: flex;
				display: -webkit-flex;
			}
			.home-menu-outer {
				width: 260px;
			}
			#moveBox {
				width: 9px;
				background-color: #a4d8f0;
			}
			#moveBox:hover {
				cursor: col-resize;
			}
			.home-menu,.home-menu-grid {
				width: 100%;
			}
			.home-menu-grid{
				
			}
			.home-other {
				overflow: hidden;
				width: 100%;
    			height: 100%;
				display: none;
			}
			.home-content {
				flex-grow: 1;
				overflow: hidden;
			}
			.home-map {
				width: 100%;
				height: 100%;
			}
			#allmap {
				width: 100%;
				height: 100%;
				overflow: hidden;
			}
			.map-tips {
				position: absolute;
				right: 0;
				bottom: 0;
				width: 820px;
				height: 0;
				background-color: #FFFFFF;
				overflow: auto;
			}
			.map-tips-title {
			    height: 30px;
    			padding: 4px;
				width: 100%;
			    background-color: #eaeaea;
			}
			.fast-handle {
				display: inline-block;
				margin-left: 50px;
			    border: 1px solid #ababab;
			    border-radius: 5px;
			    padding: 1px 5px;
			    cursor: pointer;
			}
			.fast-handle:hover {
				background-color: #f4f4f4;
			}
			.handle-case {
				color: deepskyblue;
			}
			.handle-case:hover {
				text-decoration: none;
				cursor: pointer;
				color: #23b7fb;
			}
			.map-tips-info {
			    display: inline-block;
				margin: 0 5px;
				font-size: 16px;
				font-weight: bold;
			}
			.my-fadeInUp {
				animation: fadeUpCss 0.5s;
				-webkit-animation: fadeUpCss 0.5s;
			}
			@keyframes fadeUpCss
			{
				from {height:0;}
				to {height:200px;}
			}
			.show-grid {
				display: none;
			}
			.menu-title {
				display: inline-block;
				width: 100%;
				height: 35px;
				font-size: 16px;
			    padding-left: 25px;
			    padding-top: 6px;
    			background-color: #d9e7f8;
			    border-bottom: 1px solid#99bbe8;
		        cursor: pointer;
			}
			.menu-title a {
				color: #000000;
				text-decoration: none;
			}
			.title-checked {
				font-weight: bold;
			}
			.sub-menu {
				display: none;
			}
			.first-submenu {
				display: block;
			}
			.menu-subItem {
				display: inline-block;
				height:40px;
			    width: 100%;
			    border-bottom: 1px solid #D9E0E6;
			    padding-top: 10px;
			    font-size: 15px;
			    padding-left: 40px;
			    cursor: pointer;
			    background-color: #FEFEFE;
			}
			.sub-checked {
				color: #FF7900;
			}
			.menu-icon {
				vertical-align: middle;
				vertical-align: -webkit-baseline-middle;
				vertical-align: -moz-middle-with-baseline;
				width: 22px;
    			margin-top: -15px;
    			margin-right: 8px;
			}
			.menu-pr-icon img{
				vertical-align: middle;
			    float: right;
			    margin-right: 30px;
			    margin-top: 5px;
			}
			/**实时位置 历史查询**/
			.location-params {
				width: 100%;
				height: 30px;
				line-height: 30px;
				background-color: #d5e1f2;
			}
			.location-item {
				display: inline-block;
				border: 1px solid#8db2e3;
				height: 22px;
				font-size: 13px;
    			line-height: 22px;
    			padding-left: 10px;
			    padding-right: 10px;
			    border-radius: 18px;
		     	margin-bottom: -1px;
		     	color:#416aa3;
			    cursor: pointer;
			}
			.location-item:hover {
				background-color: #f3f3f3;
			}
			.location-checked {
			    background-color: #f3f3f3;
			    color: #15428b;
			    font-weight: bold;
			}
			.search-location,.history-line,.tower-info {
			    position: relative;
			    height: 138px;
			    border-bottom: 1px solid#8db2e3;
			}
			.history-box,.tower-box {
				display: none;
			}
			.main-self-item {
				margin: 5px;
			}
			.main-self-item label {
			    font-size: 15px;
			    width: 80px;
			    font-weight: normal;
			}
			.main-self-input {
			    width: 66%;
			    height: 30px;
			    border: 1px solid #c0c0c0;
			    border-radius: 5px;
			    padding-left: 5px;
			}
			.main-btn-item {
				position: absolute;
			    right: 0;
			    margin-right: 27px;
			}
			.main-self-btn {
			    width: 56px;
			    height: 25px;
			    border: 1px solid#8db2e3;
			    border-radius: 5px;
			    background-color: #fdfffa;
			}
			.main-self-btn:hover {
				background-color: #eefbfd;
			}
			.table {
				/*background-color: #FFFFFF;*/
			}
			.home-menu-grid .pagination a {
			    width: 29px !important;
			    text-align: center !important;
			    padding: 6px 0px !important;
			}
			.home-menu-grid .pagination-detail {
				margin-top: 0 !important;
				margin-bottom: 0 !important;
			}
			.home-menu-grid .pagination-info {
				line-height: 0;
				margin-right: 0;
			}
			/*地图弹出信息框*/
			.show-marker-box {
				border: 1px solid #209ae7;
                width: 358px;
                min-height: 100px;
                background-color: #fff;
                border-radius: 3px;
				margin-left: -70px;
            }
			.show-marker-box-tower {
				border: 1px solid #209ae7;
				width: 275px;
				min-height: 100px;
				background-color: #fff;
				border-radius: 3px;
				margin-left: -22px;
			}
            .box-info {
				width: 260px;
				border-top: 1px dashed #209ae7;
                min-height: 26px;
                line-height: 26px;
				color: #209ae7;
                margin-left: 5px;
                margin-right: 5px;
			}
            .show-marker-box .box-info span {
				color: #526069;
			}
			.show-marker-box .title {
                min-height: 26px;
                line-height: 26px;
				text-align: center;
				background-color: #209ae7;
				color: #FFFFFF;
				border-radius: 3px 3px 0 0 ;
			}
			.show-marker-box .title .box-close {
				display: inline-block;
				float: right;
				color: #000000;
		        margin-right: 5px;
    			margin-top: -2px;
			    cursor: pointer;
			}
			.show-marker-box .title .box-close:hover {
				color: #FFFFFF;
			}
			
			.BMap_contextMenu {
				text-align: center;
			}
			.my-label-css {
				width: 95px;
			}
			.action-btn {
				margin-left: 5px;
			}
			#main-location-table tr{
				cursor: pointer !important;
			}
			.home-select-cls {
				width: 165px;
				height: 29px;
				border: 1px solid #c0c0c0;
				border-radius: 5px;
			}
			.my-btn {
				color: #fff !important;
				background-color: #337ab7;
				border-color: #2e6da4;
			}
			.my-btn:hover {
				color: #fff !important;
				background-color: #286090;
				border-color: #204d74;
			}
			.login-info {
				float: right;
				height: 74px;
				line-height: 74px;
				margin-right: 20px;
				color: #FFFFFF;
				font-size: 15px;
			}
			.top-login-name {
				display: inline-block;
				margin-right: 15px;
			}
			.login-out-cls {
				color: #fff;
				cursor: pointer;
			}

			.close-map-tips {
				display: inline-block;
				float: right;
				margin-right: 22px;
				cursor: pointer;
			}
			.close-map-tips:hover {
				color:red;
			}
			.show-waring-icon {
				position: absolute;
				width: 50px;
				height: 50px;
				background-color: red;
				border-radius: 50%;
				z-index: 999;
				right: 70px;
				bottom: 0;
				cursor: pointer;
			}

			.person-map-info {
				display: flex;
				display: -webkit-flex;
			}
			.person-map-photo {
				width: 86px;
				height: 107px;
				line-height: 107px;
				text-align: center;
				color: #c0c0c0;
				overflow: hidden;
			}
			.person-map-photo img {
				width: 86px;
				height: 107px;
			}
			.person-map-tip {
				flex-grow: 1;
			}
			.person-map-other .box-info {
				width: 347px;
			}
			.map-show-timeSwther {
				position: absolute;
				z-index: 99999;
				background: rgb(252, 251, 250);
				height: 48px;
				line-height: 48px;
				border-radius: 5px;
				padding: 0 14px;
			}
			.map-local-time {
				font-family: "Microsoft YaHei";
				font-size: 20px;
			}
			.swther-img {
				display: inline-block;
				width: 85px;
				height: 74px;
				background: url(./img/map/view2.png) no-repeat;
				background-position: -152px -33px;
				background-size: 872px 574px;
				vertical-align: middle;
			}
			.tower-tab {
				height: 50px;
				background-color: #f2fafd;
			}
			.tab-items {
				display: inline-block;
				height: 50px;
				line-height: 50px;
				padding: 0 15px;
				cursor: pointer;
				font-size: 16px;
			}
			.tab-items.active {
				font-size: 18px;
				background-color: #fff;
				font-weight: bold;
			}
		</style>
	</head>
	<body>
		<div id="app">
			<!--顶部导航栏-->
			<div class="nav">
				<div class="main-logo"></div>
				<ul class="home-nav">
					<li data-index="0">
						<div class="nav-menu-icon nav-menu-checked location-nav">
							<span>实时监控</span>
						</div>
					</li>
				</ul>
				<div class="login-info">
					<span class="map-local-swther"></span>
					<span class="top-login-name"></span>
					<a href="javascript:;" class="login-out-cls">退出</a>
				</div>
			</div>
			<div class="home-body">
				<div id="moveBarBox" class="home-menu-outer">
	    			<!--左侧菜单栏-->
					<div class="home-menu-grid home-location">
						<div class="location-params">
							<span class="location-item location-tip location-checked" data-index="1">实时位置</span>
							<span class="location-item history-tip" data-index="2">历史查询</span>
							<!--<span class="location-item tower-tip" data-index="3">塔杆信息</span>-->
						</div>
						<!--实时位置查询 start -->
						<div class="location-box">
							<div class="search-location">
								<p class="main-self-item">
									<label>员工姓名：</label>
									<input type="text" class="main-self-input curr-search-name" name="name"  value="" placeholder="请输入姓名"/>
								</p>
								<p class="main-self-item">
									<label>手机号码：</label>
									<input type="text" class="main-self-input curr-search-mobile" name="number" value="" placeholder="请输入手机号码"/>
								</p>
								<p class="main-self-item">
									<label>线路名称：</label>
									<select name="routeId" class="curr-location-route home-select-cls">
										<option value="">请选择线路</option>
									</select>
								</p>
								<p class="main-btn-item">
									<button class="main-self-btn location-search">查询</button>
									<button class="main-self-btn location-reset">重置</button>
								</p>
							</div>
							<table id="main-location-table" class="table table-striped table-no-bordered main-self-table" border="0" cellspacing="" cellpadding="0">
							</table>
						</div>
						<!--实时位置查询  end-->
						<!--历史查询 start -->
						<div class="history-box">
							<div class="history-line">
								<p class="main-self-item">
									<label >员工姓名：</label>
									<input type="text" class="main-self-input" name="name"value="" placeholder="请输入姓名"/>
								</p>
								<p class="main-self-item">
									<label >手机号码：</label>
									<input type="text" class="main-self-input" name="number" value="" placeholder="请输入手机号码"/>
								</p>
								<p class="main-self-item">
									<label>线路名称：</label>
									<select name="hisRouteId" class="his-location-route home-select-cls">
										<option value="">请选择线路</option>
									</select>
								</p>
								<p class="main-btn-item">
									<button class="main-self-btn history-search">查询</button>
									<button class="main-self-btn history-reset">重置</button>
								</p>
							</div>
							<table id="main-history-table" class="table table-striped table-no-bordered main-self-table" border="0" cellspacing="" cellpadding="0">
							</table>
						</div>
						<!--历史查询  end-->
						<!--塔杆信息 start -->
						<div class="tower-box">
							<div class="tower-info">
								<p class="main-self-item">
									<label >塔杆编号：</label>
									<input type="text" class="main-self-input" name="name" value="" placeholder="请输入塔杆编号"/>
								</p>
								<p class="main-self-item">
									<label >责任人：</label>
									<input type="text" class="main-self-input" name="number"  value="" placeholder="请输入责任人"/>
								</p>
								<p class="main-btn-item">
									<button class="main-self-btn tower-search">查询</button>
									<button class="main-self-btn tower-reset">重置</button>
								</p>
							</div>
							<table id="main-tower-table" class="table table-striped table-no-bordered main-self-table" border="0" cellspacing="" cellpadding="0">
							</table>
						</div>
						<!--塔杆信息  end-->
					</div>
					<div class="home-menu home-other">
						<ul class="home-menu-item"></ul>
					</div>
				</div>
				<div id="moveBox"  onmousedown="startDrag()"></div>
				<!--正文列表-->
				<div class="home-content">
					<div class="home-map">
						<div class="map-show-timeSwther">
							<span class="map-local-time"></span>
							<%--<span class="map-local-swther"></span>--%>
						</div>
						<div id="allmap" class="home-location"></div>
						<div class="map-tips">
							<p class="map-tips-title">
								<span class="map-tips-info">系统警报信息提醒</span>
								<span><input type="checkbox" class="open-warning"/>开启报警声音</span>
								<span class="fast-handle">一键处理</span>
								<span class="close-map-tips" title="收起警示框">X</span>
							</p>
							<table class="table warnint-table">
							</table>
						</div>
						<div class="show-waring-icon" title="显示报警信息"></div>
					</div>
					<div class="show-grid home-other"></div>
				</div>
			</div>
		</div>
		
		<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
		<!--BMap LIB start-->
		<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=Om5N5uYSAWfedXNL4ItqdxVqz0vHHqvk"></script>
		<script type="text/javascript" src="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.js"></script>
		<script type="text/javascript" src="http://api.map.baidu.com/library/GeoUtils/1.2/src/GeoUtils_min.js"></script>
		<!--BMap LIB end-->
		<!--TABLE LIB start-->
		<script src="/app/lib/grid/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="/app/lib/grid/bootstrap-table.min.js" type="text/javascript" charset="utf-8"></script>
		<!--<script src="lib/grid/bootstrap-table.js" type="text/javascript" charset="utf-8"></script>-->
		<script src="/app/lib/grid/locale/bootstrap-table-zh-CN.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="/app/lib/layui/layui.all.js" type="text/javascript" charset="utf-8"></script>
		<script src="/app/lib/convertPinyin.js" type="text/javascript" charset="utf-8"></script>
		<script src="/app/lib/grid/BootstrapMenu.min.js" type="text/javascript" charset="utf-8"></script>
		
		<script src="/app/lib/grid/jquery.contextMenu.js" type="text/javascript" charset="utf-8"></script>
		<script src="/app/lib/infoBox/InfoBox_min.js" type="text/javascript" charset="utf-8"></script>
			
		<!--TABLE LIB end-->
		<script>
			LOGININFO = JSON.parse('<%=loginSession%>');
			L_RNAME = LOGININFO.realName;
			L_NAME = LOGININFO.userName;
			L_TYPE = LOGININFO.userType;
			L_ID = LOGININFO.userId;
			$(".top-login-name").text(L_RNAME?"欢迎您："+L_RNAME:"欢迎您："+L_NAME);
		</script>
		<script src="/app/common/common.js" type="text/javascript" charset="utf-8"></script>
		<script src="/app/common/menu.js" type="text/javascript" charset="utf-8"></script>
		<script src="/app/js/map.js" type="text/javascript" charset="utf-8"></script>
		<script src="/app/js/main.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">
			var bodyWidth = document.documentElement.clientWidth,
				bodyHeight = document.documentElement.clientHeight;
				$(".home-menu").height(bodyHeight-74);
				$(".home-menu-grid").height(bodyHeight-74);
				$(".home-content").height(bodyHeight-74);
				$(".home-content").css("max-height","bodyHeight-74");
			window.onresize = function(){
				var bodyWidth = document.documentElement.clientWidth,
				bodyHeight = document.documentElement.clientHeight;
				$(".home-menu").height(bodyHeight-74);
				$(".home-menu-grid").height(bodyHeight-74);
				$(".home-content").height(bodyHeight-74);
				$(".home-content").css("max-height","bodyHeight-74");
			}
		</script>
	</body>
</html>
